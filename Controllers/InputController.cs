using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InputController : MonoBehaviour 
{	
	private bool leftMouseDown;
	private bool rightMouseDown;
	private RaycastHit[] hits;
	private Ray ray;
	private IDictionary<string, RaycastHit> hitList;
		
	// Use this for initialization
	void Start ()
	{
		hitList = new Dictionary<string, RaycastHit>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		//GetMouseOver ();
		GetMouseInput();
		GetCommandKeys();
	}

	#region MouseInputs
	public static Vector3 GetMousePositionOnFloor()
	{
		Ray ray = GameManager.MainCamera.GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);		
		RaycastHit[] hits = Physics.RaycastAll(ray, 300f);

		Debug.DrawLine (GameManager.MainCamera.transform.position, ray.GetPoint (300f));

		foreach(RaycastHit hit in hits)
		{
			if(hit.transform.tag == "Floor")
				return hit.point;				
		}
		
		return Vector3.zero;
	}

	public static bool IsMouseOverTag(string targetTag)
	{
		Ray ray = GameManager.MainCamera.GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);		
		RaycastHit[] hits = Physics.RaycastAll(ray, 300f);
		
		Debug.DrawLine (GameManager.MainCamera.transform.position, ray.GetPoint (300f));
		
		foreach(RaycastHit hit in hits)
		{
			//Debug.Log("Over : " + hit.collider.tag + " - " + hit.transform.tag);
			if(hit.collider.tag == targetTag)
			{	
				//Debug.Break();
				return true;				
			}
		}
		
		return false;
	}
	#endregion


	/*
	public static bool IsMouseOverTag(string targetTag)
	{
		Ray ray = GameManager.MainCamera.GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);		
		RaycastHit[] hits = Physics.RaycastAll(ray, 300f);

		Debug.DrawLine (GameManager.MainCamera.transform.position, ray.GetPoint (300f));

		foreach(RaycastHit hit in hits)
		{
			//Debug.Log("Over : " + hit.collider.tag + " - " + hit.transform.tag);
			if(hit.collider.tag == targetTag)
			{	
				//Debug.Break();
				return true;				
			}
		}
		
		return false;
	}
	*/

	#region MouseInputs
	private void GetMouseInput()
	{
		if(Input.GetKeyDown(KeyCode.Mouse0))
			UpdateMouseInput(true);

		if(Input.GetKeyDown(KeyCode.Mouse1))
			UpdateMouseInput(false);
	}

	private void UpdateMouseInput(bool mouse1)
	{
		bool hasFloor = false;
		bool hasPlayer = false;
		string hited = "";

		ray = GameManager.MainCamera.GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);		
		hits = Physics.RaycastAll(ray, 300f);
		string lastHit = null;

		Debug.DrawLine (GameManager.MainCamera.transform.position, ray.GetPoint (300f), Color.red);

		//Debug.Break ();

		foreach(RaycastHit hit in hits)
		{
			if (!hitList.ContainsKey(hit.transform.tag))
			{
				hitList.Add(hit.transform.tag, hit);

				if(hit.transform.tag.Equals("Floor"))
					hasFloor = true;
				
				if(hit.transform.tag.Equals("Character-Selector"))
					hasPlayer = true;	
			}
		}

		if(hasPlayer)
		{
			hited = "Character-Selector";
		}
		else
		{
			if(hasFloor)
			{
				hited = "Floor";
			}
		}

		if(hitList.ContainsKey(hited))
		{
			if(mouse1)
			{
				GameManager.Mouse1Click(hitList[hited]);
			}
			else
			{
				GameManager.Mouse2Click(hitList[hited]);
			}
		}
		hitList.Clear();
	}

	/*
	private void GetMouseOver()
	{
		ray = GameManager.MainCamera.GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);		
		hits = Physics.RaycastAll(ray, 300f);

		foreach(RaycastHit hit in hits)
		{
			MouseOverFunc(hit);				
		}
	}



	private void MouseOverFunc(RaycastHit hit)
	{
		if(hit.transform.tag.Equals("UI-Item"))
		{
			hit.collider.gameObject.SendMessage ("MouseOverObject");
		}
	}*/
	#endregion


	#region KeyInputs
	// Controlador de Estados das Setas do Teclado, cada tecla pode estar Down ou Up.
	private void GetCommandKeys()
	{
		if (Input.anyKeyDown)
		{
			Vector3 mousePos = GetMousePositionOnFloor();

			if(Input.GetKeyDown(KeyCode.Q)) {GameManager.AddCommand(KeyCode.Q, mousePos);}
			if(Input.GetKeyDown(KeyCode.W)) {GameManager.AddCommand(KeyCode.W, mousePos);}
			if(Input.GetKeyDown(KeyCode.E)) {GameManager.AddCommand(KeyCode.E, mousePos);}
			if(Input.GetKeyDown(KeyCode.R)) {GameManager.AddCommand(KeyCode.R, mousePos);}
			if(Input.GetKeyDown(KeyCode.T)) {GameManager.AddCommand(KeyCode.T, mousePos);}
			if(Input.GetKeyDown(KeyCode.Y)) {GameManager.AddCommand(KeyCode.Y, mousePos);}
			if(Input.GetKeyDown(KeyCode.U)) {GameManager.AddCommand(KeyCode.U, mousePos);}
			if(Input.GetKeyDown(KeyCode.I)) {GameManager.AddCommand(KeyCode.I, mousePos);}
			if(Input.GetKeyDown(KeyCode.O)) {GameManager.AddCommand(KeyCode.O, mousePos);}
			if(Input.GetKeyDown(KeyCode.P)) {GameManager.AddCommand(KeyCode.P, mousePos);}
                                             
			if(Input.GetKeyDown(KeyCode.A)) {GameManager.AddCommand(KeyCode.A, mousePos);}
			if(Input.GetKeyDown(KeyCode.S)) {GameManager.AddCommand(KeyCode.S, mousePos);}
			if(Input.GetKeyDown(KeyCode.D)) {GameManager.AddCommand(KeyCode.D, mousePos);}
			if(Input.GetKeyDown(KeyCode.F)) {GameManager.AddCommand(KeyCode.F, mousePos);}
			if(Input.GetKeyDown(KeyCode.G)) {GameManager.AddCommand(KeyCode.G, mousePos);}
			if(Input.GetKeyDown(KeyCode.H)) {GameManager.AddCommand(KeyCode.H, mousePos);}
			if(Input.GetKeyDown(KeyCode.J)) {GameManager.AddCommand(KeyCode.J, mousePos);}
			if(Input.GetKeyDown(KeyCode.K)) {GameManager.AddCommand(KeyCode.K, mousePos);}
			if(Input.GetKeyDown(KeyCode.L)) {GameManager.AddCommand(KeyCode.L, mousePos);}
                                             
			if(Input.GetKeyDown(KeyCode.Z)) {GameManager.AddCommand(KeyCode.Z, mousePos);}
			if(Input.GetKeyDown(KeyCode.X)) {GameManager.AddCommand(KeyCode.X, mousePos);}
			if(Input.GetKeyDown(KeyCode.C)) {GameManager.AddCommand(KeyCode.C, mousePos);}
			if(Input.GetKeyDown(KeyCode.V)) {GameManager.AddCommand(KeyCode.V, mousePos);}
			if(Input.GetKeyDown(KeyCode.B)) {GameManager.AddCommand(KeyCode.B, mousePos);}
			if(Input.GetKeyDown(KeyCode.N)) {GameManager.AddCommand(KeyCode.N, mousePos);}
			if(Input.GetKeyDown(KeyCode.M)) {GameManager.AddCommand(KeyCode.M, mousePos);}
		}
	}
	#endregion
}
