﻿using UnityEngine;
using System.Collections;

public class MinionController : MonoBehaviour 
{
	private CharacterController minionController;

	void Start ()
	{
		minionController = CharacterManager.GetController (this.gameObject);
	}

	// Update is called once per frame
	void FixedUpdate ()
	{
		if(minionController.IsIdle)
		{
			Vector3 minionDirection;

			if(minionController.TeamColor == TeamColor.RED_TEAM)
				minionDirection = new Vector3(-100, 1, 0);
			else
				minionDirection = new Vector3(100, 1, 0);

			minionController.UseAction(ActionType.PURSUIT_AND_ATTACK, Vector3.zero);
			minionController.UseAction(ActionType.CLICK_TARGET, minionDirection);
		}
	}
}
