﻿using UnityEngine;
using System.Collections;

public class CharacterController : MonoBehaviour 
{
	#region atributos
	/**
	 * Atributos do Character
	 */
	private GameObject characterGO;	
	private CharacterModel characterModel;
	private bool isMainPlayer;
	private TeamColor teamColor;
	private int index;
		
	private bool isAlive;
	private bool isMoving;
	private bool isLocked;	
	private bool isIdle;
	private int lockTimer;
	private int lockCurrentTimer;

	private Vector3 originalSpawn;
	private Vector3 direction;
	private Vector3 destination;
	private float movimentSpeed;
	
	private Action currentAction;
	private GameObject currentActionGO;
	private Vector3 clickedTarget;
	private CharacterController selectedTarget;

	private int respawnTime;
	#endregion
	
	#region Properties
	/**
	 * Propriedades Publicas do Character
	 */
	public GameObject CharacterGO
	{
		get 
		{
			if (characterGO == null)				
				characterGO = transform.gameObject;

			return characterGO;
		}
		set 
		{
			characterGO = value;
		}
	}

	public CharacterModel CharacterModel
	{
		get {return characterModel;}
		set {characterModel = value;}
	}
	
	public bool IsMainPlayer
	{
		get {return isMainPlayer;}
		set 
		{	
			//  ================ REPENSAR A FORMA DE ATRELAR A CAMERA ===================== //
			//GameObject ui = CharacterGO.transform.FindChild("UI").gameObject;
			//GameObject camera = ui.transform.FindChild("Main Camera").gameObject;

			if (value)
			{
				GameManager.MainCamera.transform.position = new Vector3(CharacterPosition.x, GameManager.MainCamera.transform.position.y, CharacterPosition.z + 50);
				GameManager.MainCamera.transform.LookAt(CharacterPosition);

				GameManager.MainCamera.transform.parent = CharacterGO.transform;
			}

			isMainPlayer = value;
		}
	}
	
	public int Index { get { return index;}}
	public bool IsAlive { get { return isAlive;} set{isAlive = value;}}
	public bool IsLocked { get { return isLocked;}}
	public bool IsIdle { get { return isIdle;} set{isIdle = value;}}
	public TeamColor TeamColor {get {return teamColor;} set {teamColor = value;}}
	public Vector3 CharacterPosition {get {return new Vector3(transform.position.x, 0, transform.position.z);}}
	public GameObject CurrentActionGO {get	{return currentActionGO;} set{currentActionGO = value;}}
	public string WeaponPrefab {get	{return characterModel.WeaponPrefab;}}
	public WeaponType Weapon {get {return characterModel.Weapon;}}
	public float AttackRange { get { return characterModel.AttackRange; } set { characterModel.AttackRange = value; }}
	public int CurrentCoolDown { get { return characterModel.CurrentCoolDown; } set { characterModel.CurrentCoolDown = value; }}
	public int AttackCoolDown {get {return characterModel.AttackCoolDown;}}
	public int AttackDuration {get {return characterModel.AttackDuration;}}

	public int AttackDamage {get {return characterModel.AttackDamage;}}
	public int MagicDamage {get {return characterModel.MagicDamage;}}	
	public int Armor { get { return characterModel.Armor; }}
	public int MagicResist { get { return characterModel.MagicResist; }}
	public float AttackSpeed { get { return characterModel.AttackSpeed; }}
	public int CoolDownReduction { get { return characterModel.CoolDownReduction; }}
	public float MovimentSpeed { get { return characterModel.MovimentSpeed; }}

	public float CriticalChance { get { return characterModel.CriticalChance;}}
	public float CriticalDamage { get { return characterModel.CriticalDamage;}}	
	public Vector3 Destination { get { return destination;}}
	#endregion	

	// Use this for initialization
	void Start () 
	{
		characterGO = transform.gameObject;

		isLocked = false;
		IsAlive = true;
		respawnTime = 1000;
		originalSpawn = transform.position;
		//clickedTarget = Vector3.zero;
	}

	void FixedUpdate()
	{
		if(characterModel != null)
			characterModel.UpdateCoolDown();
		
		UpdateMoviment();
		UpdateLock();

		if (!IsAlive)
			UpdateRespawn ();
	}
	
	private void UpdateMoviment()
	{
		if (isMoving && !isLocked)
		{
			direction = (destination - CharacterPosition).normalized;

			VerifyPath();

			if ((destination - CharacterPosition).magnitude < 0.3f) 
			{
				StopMoving();
			}	
			else
			{
				direction = direction.normalized * (CharacterModel.MovimentSpeed/5000);
				characterGO.transform.Translate(new Vector3(direction.x, 0, direction.z));
			}
		}
	}
	
	private void UpdateLock()
	{
		if(isLocked)
		{
			if (lockTimer > 0)
			{
				Debug.Log("Locked Until " + lockCurrentTimer + " / " + lockTimer);

				lockCurrentTimer++;

				if (lockCurrentTimer >= lockTimer)
				{
					lockTimer = 0;
					lockCurrentTimer = 0;

					UnlockCharacter();
				}
			}
		}
	}

	private void UpdateRespawn()
	{
		respawnTime--;

		if (IsMainPlayer)
			Debug.Log ("Decay " + respawnTime);

		if(respawnTime < 0)
		{
			ReSpawn();
		}
	}
	
	#region ReceivedCommands
	public void UseAction(ActionType actionType, Vector3 target)
	{
		if (IsAlive)
			ActionManager.UseAction(actionType, target, this);
	}
	
	public void UseAction(ActionType actionType, CharacterController target)
	{
		if (IsAlive)
			ActionManager.UseAction(actionType, target, this);
	}	
	
	public void SetTarget(Vector3 target)
	{
		if (clickedTarget == Vector3.zero)
		{
			clickedTarget = target;
		}
	}

	public Vector3 GetTarget()
	{
		return clickedTarget;
	}	

	public void SetTarget(CharacterController target)
	{
		if (selectedTarget == null)
		{
			selectedTarget = target;
			clickedTarget = InputController.GetMousePositionOnFloor();
		}
		else
		{
			selectedTarget = null;
			clickedTarget = Vector3.zero;
		}
	}

	public CharacterController GetTargetController()
	{
		return selectedTarget;
	}

	public void ReleaseTarget()
	{
		if(IsMainPlayer){GameManager.ClearTargets ();}

		selectedTarget = null;
		clickedTarget = Vector3.zero;
	}
	
	public void ClearActions()
	{	
		if(!isLocked)
		{
			//if (currentAction)
			//	currentAction.Dismiss();

			if (currentActionGO)
				DestroyObject(currentActionGO);

			ReleaseTarget ();
		}
	}	

	public void SetCharacterMaterial(Material classMaterial)
	{
		GameObject body = CharacterGO.transform.FindChild ("Body").gameObject;

		foreach (MeshRenderer render in body.GetComponentsInChildren<MeshRenderer>())
		{
			render.material = classMaterial;
		}
	}
	#endregion

	#region Effects	
	/**
	 * Trata o effeito recebido, atualizando a vida, a barra de vida e exibindo o dano em numeros vermelhos.
	 */	 
	public void ApplyEffect(ActionEffectTypes effect, CharacterController caster, int amout, int duration)
	{
		DoApplyEffect(effect, caster, Index, amout, duration);
	}
	
	public void DoApplyEffect(ActionEffectTypes effect, CharacterController caster, int targetIndex, int amount, int duration)
	{
		if(characterModel.HitPoints > 0)
		{			
			int ReceivedDamage = CharacterModel.CalculateDamage(effect, amount);
			int ReceivedEffectDuration = CharacterModel.CalculateEffectDuration(effect, duration);

			VerifyEffects(effect, amount, duration);

			if(effect != ActionEffectTypes.HEALING_LIFE)
			{
				amount = -amount;
				GameManager.ShowDamage (this, amount, Color.red);
			}
			else
			{
				GameManager.ShowHealing (this, amount, Color.green);
			}

			characterModel.HitPoints += amount;
			
			if(characterModel.HitPoints <= 0)
			{
				Death(caster, amount);
			}

			if(effect == ActionEffectTypes.STUN_EFFECT)
			{
				LockCharacterUntil(duration);
			}

			if(isAlive)
				GameManager.UpdateLifeBar (this, amount);
		}
	}	
	
	public void AddEffectGO(GameObject effectGO)
	{
		//currentEffectGO = effectGO;
	}
	#endregion
		
	#region Moviments
	public void MoveTo(Vector3 destination, bool drawTarget)
	{
		this.destination = new Vector3 (destination.x, 0, destination.z);

		if(drawTarget && !isLocked && IsMainPlayer)
		{
			GameManager.DrawMovimentTarget(this.destination);

			//if(GameManager.IsOnline)
			//	MatchPhotonClient.Instance.SendCommand(ActionType.MOVE_TO, destination);

			//MultiPlayerController.Instance.SendCommand(ActionType.MOVE_TO, destination);
		}

		//TODO Path Find !!!!	
		SetDirection (this.destination);	
	}
	
	public void StopMoving()
	{
		isMoving = false;
		direction = Vector3.zero;
	}
	
	public void LockCharacter()
	{
		isLocked = true;
		isMoving = false;
	}

	public void LockCharacterUntil(int lockTimer)
	{
		this.lockTimer = lockTimer;
		lockCurrentTimer = 0;
		LockCharacter ();
	}

	public void UnlockCharacter()
	{
		//if (currentEffectGO)
		//	DestroyObject (currentEffectGO);

		isLocked = false;
	}

	public void PushBack(float speed, float distance)
	{
	}

	public void AddEffectGO2(GameObject effectGO)
	{
		//currentEffectGO = effectGO;
	}
	#endregion

	private void VerifyEffects(ActionEffectTypes effect, int amout, int duration)
	{
		switch(effect)
		{			
			case ActionEffectTypes.STUN_EFFECT:
			{
				LockCharacterUntil(duration);
				break;
			}
		}
	}

	private void SetDirection(Vector3 destination)
	{		
		isMoving = true;
		this.destination = destination;

		direction = (destination - CharacterPosition).normalized;
	}
	
	private void VerifyPath()
	{
		bool pathIsClear = true;
		int deltaAngle = 0;
		int maxTries = 5;
		
		float distance = (destination - CharacterPosition).magnitude;
		direction = (destination - CharacterPosition).normalized;
		distance = distance / 20;
	
		Ray ray = new Ray(CharacterPosition, direction.normalized);	
		RaycastHit[] hits = Physics.RaycastAll(ray, distance);

		Debug.DrawLine (CharacterPosition, ray.GetPoint (distance), Color.green);

		foreach(RaycastHit hit in hits)
		{
			if(!hit.collider.gameObject.tag.Equals("Character-Selector") && !hit.collider.gameObject.tag.Equals("Untagged"))
			{
				if(CharacterManager.GetController(hit.collider.gameObject) != GetTargetController())
				{
					pathIsClear = false;
					deltaAngle += 20;
				}
			}
		}

		while(!pathIsClear)
		{

			pathIsClear = true;
			maxTries--;

			Vector3 normalizedDirection = (destination - CharacterPosition).normalized;
			Vector3 targetPosition = destination; 
		
			Vector3 leftNormal = new Vector3 (normalizedDirection.z, 0, -normalizedDirection.x);
			Vector3 rightNormal = new Vector3 (-normalizedDirection.z, 0, normalizedDirection.x);
			
			Vector3 leftDirection = targetPosition;
			Vector3 rightDirection = targetPosition;			
			
			leftDirection += leftNormal*deltaAngle;
			rightDirection += rightNormal*deltaAngle;

			leftDirection = (leftDirection - CharacterPosition).normalized;
			rightDirection = (rightDirection - CharacterPosition).normalized;
		
			Ray leftRay = new Ray(CharacterPosition, leftDirection);
			Ray rightRay = new Ray(CharacterPosition, rightDirection);
			
			Debug.DrawLine (CharacterPosition, leftRay.GetPoint (distance), Color.yellow);			
			Debug.DrawLine (CharacterPosition, rightRay.GetPoint (distance), Color.red);			
			
			hits = Physics.RaycastAll(leftRay, distance);
			foreach(RaycastHit hit in hits)
			{
				pathIsClear = false;
				deltaAngle += 20;
			}
			
			if(pathIsClear)
			{
				direction = leftDirection;				
				Debug.DrawLine (CharacterPosition, leftRay.GetPoint (distance), Color.green);
				return;
			}

			hits = Physics.RaycastAll(rightRay, distance);
			foreach(RaycastHit hit in hits)
			{
				pathIsClear = false;
				deltaAngle += 20;
			}
			
			if(pathIsClear)
			{
				direction = rightDirection;				
				Debug.DrawLine (CharacterPosition, rightRay.GetPoint (distance), Color.green);
				return;
			}

			if(maxTries <= 0)
				return;
		}	
	}	
	
	private void Death(CharacterController caster, int amount)
	{
		// Caso seja uma torre, troca o objeto 3d para a torre explodindo
		if(CharacterModel.CharacterClass == ClassType.TOWER)
		{			
			CharacterManager.CreateBrokenTowerAt(TeamColor, CharacterPosition);
			CharacterGO.SetActive(false);
			IsAlive = false;
		}
		else
		{
			// Caso seja o player 1, solta a camera e apaga os targets.
			if(IsMainPlayer)
			{
				ReleaseCamera();
			}

			// Interrompe as movimentaçoes
			isLocked = true;

			// Desliga a barra de vida e os coliders
			GameManager.HideLifeBar (this);
			
			transform.FindChild("Selection").gameObject.SetActive(false);
			
			foreach(BoxCollider collider in transform.FindChild("Body").GetComponentsInChildren<BoxCollider>())
			{
				collider.isTrigger = false;
			}
			
			transform.FindChild("Selection").gameObject.SetActive(false);
			
			characterGO.AddComponent<Rigidbody>(); 		
			characterGO.GetComponent<Rigidbody>().mass = 0.2f;
			characterGO.GetComponent<Rigidbody>().isKinematic = false;
			characterGO.GetComponent<Rigidbody>().useGravity = true;
			
			Vector3 casterDirection = caster.CharacterPosition - CharacterPosition;
			Vector3 deathForce = Random.onUnitSphere;
			deathForce -= casterDirection.normalized * 20f;

			// Joga o corpo em uma direçao randomica
			characterGO.GetComponent<Rigidbody> ().velocity = new Vector3 (deathForce.x, 2f, deathForce.z);
			
			//characterGO.GetComponent<Rigidbody>().AddExplosionForce (400, new Vector3(amount, 0.1f, amount) , 10);
			
			isAlive = false;

			if(CharacterModel.CharacterClass == ClassType.MERCENARY ||
			   CharacterModel.CharacterClass == ClassType.DISCIPLE ||
			   CharacterModel.CharacterClass == ClassType.HUNTER)
			{
				respawnTime = 2000;
			}
		}
	}

	private void RefreshAll()
	{
		/*
		isLocked = false;
		clickedTarget = Vector3.zero;
		characterDisplay.SetActive (true);	

		if(IsMainPlayer)
		{
			GameManager.PlayerCAM.SetActive (true);

			if(teamColor == TeamColor.BLUE_TEAM)
				MatchPhotonClient.Instance.currentMatchRoom.BlueCamera.SetActive(false);
			else
				MatchPhotonClient.Instance.currentMatchRoom.RedCamera.SetActive(false);
		}

		characterGO.transform.rotation = Quaternion.Euler(Vector3.zero);

		currentCoolDown = 1000;
		firstActionCoolDown = 1000;
		secondActionCoolDown = 1000;
		thirdActionCoolDown = 1000;
		specialActionCoolDown = 1000;

		characterModel.HitPoints = characterModel.MaxHitPoints;
		
		lifeBar.transform.localScale = new Vector3(10, 1.5f, 0.1f);
		lifeBar.transform.localPosition = fullLifePosition; 
		*/
	}
	
	public void ReleaseCamera()
	{
		GameObject cenario = GameObject.Find("Cenario");
		GameManager.MainCamera.transform.parent = cenario.transform;
		GameManager.ClearTargets ();
	}

	public void ReSpawn()
	{
		if(CharacterModel.CharacterClass == ClassType.WARRIOR ||
		   CharacterModel.CharacterClass == ClassType.ARCHER ||
		   CharacterModel.CharacterClass == ClassType.MAGE)
		{
			CharacterController respawnedChar = CharacterManager.CreateCharacterAt(CharacterModel.CharacterName, 
			                                                                       CharacterModel.CharacterClass,
			                                                                       TeamColor,
			                                                                       IsMainPlayer,
			                                                                       originalSpawn);
		}
		DestroyObject(CharacterGO);
	}
	
}

public enum ClassType
{	
	NONE,
	MAGE,
	ARCHER,
	WARRIOR,

	DISCIPLE,
	HUNTER,
	MERCENARY,

	TOWER,
	CASTLE
}

public enum WeaponType
{
	BOW,
	SWORD,
	SLING,
	CANNON
}