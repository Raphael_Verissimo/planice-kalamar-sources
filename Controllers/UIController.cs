using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/**
 * Author: Kubaman Kalamar
 * 
 * Classe Principal de Controle das Interfaces, Por ela s�o executadas os Comandos de Interface.
 */
public class UIController : MonoBehaviour
{
	#region Internal Attributes
	/**
	 * Objetos de Target
	 */
	private GameObject SelectedTarget;	
	private GameObject SkillShotTarget;
	private GameObject AttackTarget;
	private GameObject AttackTarget2;
	private GameObject AttackTarget3;
	private GameObject AttackTarget4;
	private GameObject AreaTarget;
	private GameObject RangedAreaTarget;
	private GameObject DefenseTarget;	
		
	private Vector3 lastFloorPosition;
	
	private IDictionary<int, GameObject> MultipleSkillShotTarget = new Dictionary<int, GameObject>();
	#endregion

	#region Public API
	/**
	 * Exibe um Numero acima do GameObject, com anima��o de dano, com o valor passado na cor escolhida (a cor do dano segue a propriedade do dano).
	 */
	public void ShowDamage(CharacterController character, int amount, Color numberColor) 
	{
		GameObject _display = ObjectManager.InstantiateObject ("Interfaces/NumberDisplay", character.CharacterPosition);
		NumberDisplay numberDisplay = _display.GetComponent<NumberDisplay> ();

		numberDisplay.ShowDamage (character, amount, numberColor);
	}

	/**
	 * Exibe um Numero acima do GameObject, com anima��o de cura, com o valor passado na cor escolhida (curas de mana s�o azuis).
	 */
	public void ShowHealing(CharacterController character, int amount, Color numberColor) 
	{
		GameObject _display = ObjectManager.InstantiateObject ("Interfaces/NumberDisplay", character.CharacterPosition);
		NumberDisplay numberDisplay = _display.GetComponent<NumberDisplay> ();
		
		numberDisplay.ShowHealing (character, amount, numberColor);
	}

	/**
	 * Atualiza a barra de vida para contemplar a nova Vida, usado em Level Up's ou Curses ... 
	 * Lembre-se de ajustar a vida, pois a barra se enche apos a atualiza��o.
	 */
	public void SetUpLifeBar(CharacterController character)
	{
		GameObject lifeBarGO = character.CharacterGO.transform.FindChild("LifeBar").gameObject;	
		LifeBar lifeBar = lifeBarGO.GetComponent<LifeBar> ();

		lifeBar.SetUpLifeBar(character);
	}
	
	/**
	 * Atualiza a anima��o da barra de vida, caso amount seja negativo, ser� considerado dano, positivo ser� healing.
	 */
	public void UpdateLifeBar(CharacterController character, int amount)
	{
		GameObject lifeBarGO = character.transform.FindChild("LifeBar").gameObject;	
		LifeBar lifeBar = lifeBarGO.GetComponent<LifeBar> ();

		lifeBar.UpdateBar(amount);
	}

	/**
	 * Esconde a barra de vido do personagem alvo
	 */
	public void HideLifeBar(CharacterController character)
	{
		GameObject lifeBarGO = character.transform.FindChild("LifeBar").gameObject;	
		LifeBar lifeBar = lifeBarGO.GetComponent<LifeBar> ();
		
		lifeBar.gameObject.SetActive(false);
	}

	/**
	 * Exibe a barra de vido do personagem alvo
	 */
	public void ShowLifeBar(CharacterController character)
	{
		GameObject lifeBarGO = character.transform.FindChild("LifeBar").gameObject;	
		LifeBar lifeBar = lifeBarGO.GetComponent<LifeBar> ();
		
		lifeBar.gameObject.SetActive(true);
	}
	
	/**
	 * Limpa o desenho de Interfaces de target
	 */	
	public void ClearTargets()
	{
		if(AreaTarget)GameObject.DestroyObject(AreaTarget);
		if(AttackTarget)GameObject.DestroyObject(AttackTarget);
		if(DefenseTarget)GameObject.DestroyObject(DefenseTarget);
		if(AttackTarget2)GameObject.DestroyObject(AttackTarget2);
		if(AttackTarget3)GameObject.DestroyObject(AttackTarget3);
		if(AttackTarget4)GameObject.DestroyObject(AttackTarget4);
		if(SelectedTarget)GameObject.DestroyObject(SelectedTarget);
		if(SkillShotTarget)GameObject.DestroyObject(SkillShotTarget);
		if(RangedAreaTarget)GameObject.DestroyObject(RangedAreaTarget);
		if(MultipleSkillShotTarget.Count > 0)
		{
			foreach(GameObject obj in MultipleSkillShotTarget.Values)
			{
				GameObject.DestroyObject(obj);
			}

			MultipleSkillShotTarget.Clear();
		}
	}

	/**
	 * Limpa o desenho de Interfaces de target
	 */	
	public void ClearAttackTargets()
	{
		if(AttackTarget)GameObject.DestroyObject(AttackTarget);
		if(AttackTarget2)GameObject.DestroyObject(AttackTarget2);
		if(AttackTarget3)GameObject.DestroyObject(AttackTarget3);
		if(AttackTarget4)GameObject.DestroyObject(AttackTarget4);
	}
	
	/**
	 * Exibe a interface de target de movimenta��o.
	 */	
	public void DrawMovimentTarget(Vector3 target)
	{
		if (GameObject.FindGameObjectsWithTag("UI-Target").Length < 5)
		{
			Vector3 position = new Vector3(target.x, target.y + 0.1f, target.z);
			ObjectManager.InstantiateObject("UI/Targets/Target", position);
		}
	}
	
	/**
	 * Exibe a interface de target por skillshot (disparo reto sem alvo predeterminado, onde pegar ativa).
	 */	
	public void DrawSkillShotTarget(Vector3 casterPos, float range)
	{
		Vector3 target = InputController.GetMousePositionOnFloor ();

		DrawSkillShotTarget (casterPos, range, target);
	}
	
	/**
	 * Exibe a interface de target por skillshot, com controle de Range Maximo.
	 */	
	public void DrawSkillShotTarget(Vector3 casterPos, float range, Vector3 target)
	{
		range = range / 10;

		if(target != Vector3.zero)
		{
			target = new Vector3(target.x, 0.1f, target.z);		
			Vector3 position = new Vector3(casterPos.x, 0.1f, casterPos.z);
			
			if (!SkillShotTarget)
			{ 
				SkillShotTarget = ObjectManager.InstantiateObject("UI/Targets/SkillShotTarget", position);
				SkillShotTarget.transform.localScale = new Vector3(1f, 1f, range);
			}
			
			SkillShotTarget.transform.position = position;
			SkillShotTarget.transform.LookAt(target);
		}
	}
	
	/**
	 * Exibe a interface de target por skillshot multiplos, utilizado para o arrow shower!
	 * Futuramente generalizar para numero de shots configuravel.
	 */	
	public void DrawMultipleSkillShotTarget(int index, Vector3 casterPos, float range, Vector3 target)
	{
		if(target != Vector3.zero)
		{
			target = new Vector3(target.x, 0.1f, target.z);		
			Vector3 position = new Vector3(casterPos.x, 0.1f, casterPos.z);
			
			if (!MultipleSkillShotTarget.ContainsKey(index))
			{ 
				MultipleSkillShotTarget.Add(index, ObjectManager.InstantiateObject("UI/Targets/SkillShotTarget", position));
				MultipleSkillShotTarget[index].transform.localScale = new Vector3(1f, 1f, range);
			}
			
			MultipleSkillShotTarget[index].transform.position = position;
			MultipleSkillShotTarget[index].transform.LookAt(target);
		}
	}
	
	/**
	 * Exibe a interface de atack a players se movimentando.
	 */	
	public void DrawAttackTarget()
	{
		Vector3 target = InputController.GetMousePositionOnFloor ();
		Vector3 position = new Vector3(target.x, target.y + 0.05f, target.z);
		
		if (!AttackTarget)
		{
			AttackTarget = ObjectManager.InstantiateObject("UI/Targets/RedAttackTarget", position);
			AttackTarget.transform.localScale *= 0.7f;	
		}

		//Debug.Log ("Target " + position);
		AttackTarget.transform.position = position;
	}
	
	/**
	 * Exibe a interface de atack a players se movimentando, recebendo target.
	 */	
	public void DrawAttackTarget(Vector3 target)
	{
		//Vector3 target = InputController.GetMousePositionOnFloor ();
		Vector3 position = new Vector3(target.x, target.y + 0.05f, target.z);
		
		if (!AttackTarget2)
		{
			AttackTarget2 = ObjectManager.InstantiateObject("UI/Targets/RedAttackTarget", position);
			AttackTarget2.transform.localScale *= 0.4f;
		}
		
		AttackTarget2.transform.position = position;
	}
	
	/**
	 * Exibe a interface de atack a players no alvo.
	 */	
	public void DrawAttackPlayer()
	{		
		Vector3 target = InputController.GetMousePositionOnFloor ();

		if((lastFloorPosition - target).magnitude > 0.1f)
		{
			lastFloorPosition = target;

			bool isOverTarget = InputController.IsMouseOverTag ("Player");			
			
			Vector3 effectOffSet = (GameManager.MainCamera.transform.position - target).normalized;
			Vector3 effectPosition = target + (effectOffSet * 10);
			
			if (isOverTarget)
			{
				if (!AttackTarget3)
				{	
					if(AttackTarget4)
						GameObject.DestroyObject(AttackTarget4);
					
					AttackTarget3 = ObjectManager.InstantiateObject("UI/Targets/RedAttackTargetGO", effectPosition);
				}
			}
			else
			{
				if (!AttackTarget4)
				{	
					if(AttackTarget3)
						GameObject.DestroyObject(AttackTarget3);
					
					AttackTarget4 = ObjectManager.InstantiateObject("UI/Targets/GreyAttackTargetGO", effectPosition);
				}
			}			
			
			if(AttackTarget3)
			{
				AttackTarget3.transform.position = effectPosition;
				AttackTarget3.transform.LookAt (GameManager.MainCamera.transform);
			}
			
			if(AttackTarget4)
			{
				AttackTarget4.transform.position = effectPosition;
				AttackTarget4.transform.LookAt (GameManager.MainCamera.transform);
			}
		}
	}
	
	/**
	 * Exibe a interface de atack ao player selecionado, recebendo o target.
	 */	
	public void DrawSelectedAttackTarget(Vector3 target)
	{
		Vector3 position = new Vector3(target.x, 0.1f, target.z);
		
		if (!AttackTarget)
		{
			AttackTarget = ObjectManager.InstantiateObject("UI/Targets/RedAttackTarget", position);
		}
		
		AttackTarget.transform.position = position;
	}
	
	/**
	 * Exibe a interface de atack ao player selecionado, recebendo o target gameObject.
	 */	
	public void DrawSelectedAttackTarget(GameObject targetGO)
	{
		Vector3 target = targetGO.transform.position;
		Vector3 position = new Vector3(target.x, 0.05f, target.z);
		
		if (!AttackTarget)
		{
			AttackTarget = ObjectManager.InstantiateObject("UI/Targets/RedAttackTarget", position);
		}
		
		AttackTarget.transform.position = position;
	}	
	
	/**
	 * Exibe a interface de target em area.
	 */	
	public void DrawAreaTarget(float area)
	{
		Vector3 target = InputController.GetMousePositionOnFloor ();
		Vector3 position = new Vector3(target.x, target.y + 0.1f, target.z);

		if (!AreaTarget)
		{
			AreaTarget = ObjectManager.InstantiateObject("UI/Targets/BlueTargetArea", position);
			AreaTarget.transform.localScale *= area/10;
		}

		AreaTarget.transform.position = position;
	}
	
	/**
	 * Exibe a interface de target em area, com limite de Range.
	 */	
	public void DrawAreaTargetInRange(Vector3 casterPosition, float area, float range)
	{
		Vector3 target = InputController.GetMousePositionOnFloor ();

		if (target != Vector3.zero)
		{
			Vector3 diferenceDirections = (casterPosition - target).normalized;
			float distance = (casterPosition - target).magnitude;
			float diference = distance - range;
			
			if (distance > range)
				target += diferenceDirections * diference;
			
			Vector3 position = new Vector3(target.x, target.y + 0.1f, target.z);
			
			if (!AreaTarget)
			{
				AreaTarget = ObjectManager.InstantiateObject("UI/Targets/BlueTargetArea", position);
				AreaTarget.transform.localScale *= area/10;
			}

			AreaTarget.transform.position = position;
		}
	}
	
	/**
	 * Exibe a interface de target em area, recebendo a posicao do player.
	 */	
	public void DrawRangedAreaTarget(Vector3 playerPos, float range)
	{
		if (!RangedAreaTarget)
		{
			RangedAreaTarget = ObjectManager.InstantiateObject("UI/Targets/RangedTargetArea", playerPos);
			RangedAreaTarget.transform.localScale *= range/20;
			RangedAreaTarget.transform.position += Vector3.up * 0.2f;
		}
		RangedAreaTarget.transform.position = playerPos;
		RangedAreaTarget.transform.position += Vector3.up * 0.2f;
	}
	
	/**
	 * Exibe a interface de target em area selecionavel.
	 */	
	public void DrawSelectedAreaTarget(float area)
	{
		Vector3 target = InputController.GetMousePositionOnFloor ();
		Vector3 position = new Vector3(target.x, target.y + 0.1f, target.z);

		GameObject.DestroyObject (AreaTarget);

		if (!SelectedTarget)
		{
			SelectedTarget = ObjectManager.InstantiateObject("UI/Targets/RedTargetArea", position);
			SelectedTarget.transform.localScale *= area/10;
		}

		SelectedTarget.transform.position = position;
	}
	
	/**
	 * Exibe a interface de target em area selecionavel, com range.
	 */	
	public void DrawSelectedAreaTargetInRange(Vector3 casterPosition, float range)
	{
		Vector3 target = InputController.GetMousePositionOnFloor ();

		if (target != Vector3.zero)
		{ 
		
			Vector3 diferenceDirections = (casterPosition - target).normalized;
			float distance = (casterPosition - target).magnitude;
			float diference = distance - range;
			
			if (distance > range)
				target += diferenceDirections * diference;

			Vector3 position = new Vector3(target.x, target.y + 0.1f, target.z);
			
			GameObject.DestroyObject (AreaTarget);
			
			if (!SelectedTarget)
				SelectedTarget = ObjectManager.InstantiateObject("UI/Targets/RedTargetArea", position);
			
			SelectedTarget.transform.position = position;
		}
	}
	
	/**
	 * Exibe a interface de target para Action Defesa.
	 */	
	public void DrawDefenseTarget(Vector3 casterPos)
	{
		Vector3 target = InputController.GetMousePositionOnFloor ();
		Vector3 position = new Vector3(casterPos.x, 0.2f, casterPos.z);
		
		if (!DefenseTarget)
		{ 
			DefenseTarget = ObjectManager.InstantiateObject("UI/Targets/DefenseTarget", position);
		}

		Vector3 direction = (target - position).normalized;
		target = casterPos + (direction * 100);

		DefenseTarget.transform.position = position;
		DefenseTarget.transform.LookAt(target);
	}
	#endregion
}
