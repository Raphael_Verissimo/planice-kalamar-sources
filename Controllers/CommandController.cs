﻿
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CommandController 
{
	//private CharacterController currentCharacter;
	private ActionType currentCommand;
	private IDictionary	<KeyCode, ActionType> commandAlias = new Dictionary<KeyCode, ActionType>();

	public CommandController()
	{
		commandAlias.Add(KeyCode.Mouse0, ActionType.CLICK_TARGET);
		commandAlias.Add(KeyCode.Mouse1, ActionType.MOVE_AND_CLEAR);

		commandAlias.Add(KeyCode.A, ActionType.PURSUIT_AND_ATTACK);

		commandAlias.Add(KeyCode.Q, ActionType.NONE);
		commandAlias.Add(KeyCode.W, ActionType.NONE);
		commandAlias.Add(KeyCode.R, ActionType.NONE);
	}

	public void SetMageCommands(CharacterController character)
	{
		UpdateCommandAllias(KeyCode.Q, ActionType.PARALYSE_ACTION);
		UpdateCommandAllias(KeyCode.W, ActionType.DRAIN_LIFE_ACTION);
		UpdateCommandAllias(KeyCode.R, ActionType.FIREBALL_ACTION);
	}

	public void SetArcherCommands(CharacterController character)
	{
		UpdateCommandAllias(KeyCode.Q, ActionType.POWER_SHOT_ACTION);
		UpdateCommandAllias(KeyCode.W, ActionType.ARROW_SHOWER_ACTION);
		UpdateCommandAllias(KeyCode.R, ActionType.BULLET_TIME_ACTION);
	}

	public void SetWarriorCommands(CharacterController character)
	{
		UpdateCommandAllias(KeyCode.Q, ActionType.DEFENSE_ACTION);
		UpdateCommandAllias(KeyCode.W, ActionType.BRAVER_ACTION);
		UpdateCommandAllias(KeyCode.R, ActionType.THROW_AXE_ACTION);
	}
	
	public void SetClassCommands(CharacterController character, ClassType characterClass)
	{
		switch (characterClass)
		{
			case ClassType.MAGE:
				SetMageCommands (character);
			break;

			case ClassType.ARCHER:
				SetArcherCommands (character);
			break;

			case ClassType.WARRIOR:
				SetWarriorCommands (character);
			break;
		}
	}

	private void UpdateCommandAllias(KeyCode keyCode, ActionType actionType)
	{
		if(commandAlias.ContainsKey(keyCode))
			commandAlias.Remove(keyCode);

		commandAlias.Add(keyCode, actionType);
	}

	public void Mouse1Click(RaycastHit hit)
	{
		Debug.Log ("Clicked on " + hit.transform.tag);

		if(hit.transform.tag.Equals ("Floor"))
		{	
			AddCommand(KeyCode.Mouse0, hit.point);
		}

		if(hit.transform.tag.Equals ("Character-Selector"))
		{	
			GameManager.UseAction (ActionType.CLICK_TARGET_GO, CharacterManager.GetController(hit.collider.transform.parent.gameObject));
		}

		if(hit.transform.tag.Equals ("Player"))
		{
			GameManager.UseAction (ActionType.CLICK_TARGET_GO, CharacterManager.GetController(hit.collider.transform.parent.gameObject));
		}
	}
	
	public void Mouse2Click(RaycastHit hit)
	{
		if(hit.transform.tag.Equals ("Floor"))
		{	
			AddCommand(KeyCode.Mouse1, hit.point);
		}

		if(hit.transform.tag.Equals ("Character-Selector"))
		{	
			CharacterController target = CharacterManager.GetController(hit.collider.transform.parent.gameObject);

			if(CharacterManager.MainCharacter.TeamColor != target.TeamColor)
				GameManager.UseAction (ActionType.PURSUIT_AND_ATTACK, target);
			else
				AddCommand(KeyCode.Mouse1, hit.point);			
		}
		
		if(hit.transform.tag.Equals ("Player"))
		{
			CharacterController target = CharacterManager.GetController(hit.collider.transform.parent.gameObject);
			
			if(CharacterManager.MainCharacter.TeamColor != target.TeamColor)
				GameManager.UseAction (ActionType.PURSUIT_AND_ATTACK, target);
			else
				AddCommand(KeyCode.Mouse1, hit.point);	
		}
	}

	public void AddCommand(KeyCode command, Vector3 target)
	{
		if(commandAlias.ContainsKey(command))
		{
			if (!commandAlias[command].Equals (ActionType.NONE))
			{
				GameManager.UseAction (commandAlias[command], target);
			}
		}
		else
		{
			Debug.Log("Atalho " + command + " nao Cadastrado !");
		}
	}
}
