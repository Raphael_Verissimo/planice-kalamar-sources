using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/**
 * Author: Kubaman Kalamar
 * 
 * Classe Principal de Controle de Inputs do Usu�rio, Por ela s�o executadas os Comandos de Teclado do Jogador.
 */
public class CameraController : MonoBehaviour
{
	private GameObject mainCamera;
	
	private Vector3 cameraDirection;
	private float cameraSpeed;
	
	public GameObject MainCamera
	{
		get
		{
			if (mainCamera == null)
				mainCamera = GameObject.Find("Main Camera");

			return mainCamera;
		}

		set
		{
			mainCamera = value;
		}
	}


	public Vector3 CameraDirection
	{
		get
		{
			if (cameraDirection == Vector3.zero)
				cameraDirection = (CharacterManager.MainCharacter.CharacterPosition - mainCamera.transform.position).normalized;

			return cameraDirection;
		}

		set
		{
			cameraDirection = value;
		}
	}
	
	// Use this for initialization
	void Start ()
	{
		CameraDirection = Vector3.zero;
		cameraSpeed = 2;
	}
	
	// Update is called once per frame
	void Update () 
	{
		UpdateCameraZoom ();
	}
	
	public void UpdateCameraZoom()
	{
		//Debug.Log ("Scroll = " + Input.mouseScrollDelta);
		if (Input.mouseScrollDelta.y != 0) 
		{
			Vector3 pos = Vector3.zero;

			if(Input.mouseScrollDelta.y > 0)
			{
				if(mainCamera.transform.position.y > 10)
				{
					mainCamera.transform.LookAt(CharacterManager.MainCharacter.CharacterPosition);

					mainCamera.transform.Translate(Vector3.forward*cameraSpeed);
					mainCamera.transform.Translate(-Vector3.up*(cameraSpeed/4));
				}
			}
			else
			{				
				if(mainCamera.transform.position.y < 70)
				{
					mainCamera.transform.LookAt(CharacterManager.MainCharacter.CharacterPosition);

					mainCamera.transform.Translate(-Vector3.forward*cameraSpeed);
					mainCamera.transform.Translate(Vector3.up*(cameraSpeed/4));
				}
			}				
		}
	}
}
