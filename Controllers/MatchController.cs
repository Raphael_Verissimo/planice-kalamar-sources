﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MatchController : MonoBehaviour 
{
	private IList<PlayerModel> players = new List<PlayerModel> ();

	private IList<CharacterController> redTeamCharacters = new List<CharacterController>();
	private IList<CharacterController> blueTeamCharacters = new List<CharacterController>();
	
	private GameObject redTeamSpawnPoint;
	private GameObject blueTeamSpawnPoint;

	// Use this for initialization
	void Start ()
	{
		redTeamSpawnPoint = GameObject.Find ("RedTeamSpawnPoint");
		blueTeamSpawnPoint = GameObject.Find ("BlueTeamSpawnPoint");

		CreateFakePlayers ();

		SpawnPlayers ();
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}

	private void CreateFakePlayers()
	{
		players.Add(new PlayerModel("player1", true, ClassType.MAGE, TeamColor.RED_TEAM));
		players.Add(new PlayerModel("player2", false, ClassType.ARCHER, TeamColor.BLUE_TEAM));
		
		//players.Add(new PlayerModel("player3", false, ClassType.ARCHER, TeamColor.RED_TEAM));		
		//players.Add(new PlayerModel("player4", false, ClassType.ARCHER, TeamColor.RED_TEAM));
	}

	private void SpawnPlayers()
	{
		int redCount = 0;
		int blueCount = 0;

		foreach(PlayerModel player in players) 
		{
			if(player.CurrentTeamColor == TeamColor.RED_TEAM)
			{
				Vector3 spawnPosition = redTeamSpawnPoint.transform.position - (Vector3.right * redCount);

				redTeamCharacters.Add(GameManager.CreateCharacterAt(player.Login, 
				                                                    player.CurrentCharacterClass,
				                                                    player.CurrentTeamColor, 
				                                                    player.IsMainPlayer, 
				                                                    spawnPosition));
				redCount += 3;
			}
			else
			{
				Vector3 spawnPosition = blueTeamSpawnPoint.transform.position + (Vector3.right * blueCount);

				CharacterController enemy = GameManager.CreateCharacterAt(player.Login, 
				                                                 player.CurrentCharacterClass,
				                                                 player.CurrentTeamColor, 
				                                                 player.IsMainPlayer, 
				                                                 blueTeamSpawnPoint.transform.position);

				enemy.transform.gameObject.AddComponent<NpcAIController>();

				blueTeamCharacters.Add(enemy);
				blueCount += 3;
			}
		}
	}
}
