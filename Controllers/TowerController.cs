﻿using UnityEngine;
using System.Collections;

public class TowerController : MonoBehaviour 
{
	private int attackCounter; 
	private CharacterController tower;

	void Start ()
	{
		attackCounter = 0;
		tower = CharacterManager.GetController (this.gameObject);
	}

	// Update is called once per frame
	void FixedUpdate ()
	{
		if(tower != null)
		{
			if(tower.IsAlive)
			{
				attackCounter++;

				if (attackCounter >= 10)
				{
					attackCounter = 0;
					tower.UseAction (ActionType.PURSUIT_AND_ATTACK, this.transform.position);
					tower.SetTarget(new Vector3(this.transform.position.x, 1, this.transform.position.z));
				}
			}
		}
	}
}
