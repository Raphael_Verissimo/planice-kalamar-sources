﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NpcAIController : MonoBehaviour 
{
	private CharacterController characterController;

	private IList<CharacterController> enemies;
	private IList<CharacterController> enemyCreeps;
	private IList<CharacterController> enemyStructures;
		
	private IList<CharacterController> allies;
	private IList<CharacterController> allyCreeps;
	private IList<CharacterController> allyStructures;

	private float CriticalDistance;
	private float DesirableDistance;
	private float SafeDistance;

	private IList<CharacterController> EnemiesInCriticalDistance;
	private IList<CharacterController> EnemiesInDesirableDistance;
	private IList<CharacterController> EnemiesInSafeDistance;	
	
	private IList<CharacterController> EnemyCreepsInCriticalDistance;
	private IList<CharacterController> EnemyCreepsInDesirableDistance;
	private IList<CharacterController> EnemyCreepsInSafeDistance;
	
	private IList<CharacterController> EnemyStructuresInCriticalDistance;
	private IList<CharacterController> EnemyStructuresInDesirableDistance;
	private IList<CharacterController> EnemyStructuresInSafeDistance;
	
	private float SupportDistance;
	private float UnsupportedDistance;
	private float BackUpDistance;
	
	private IList<CharacterController> AlliesInSupportDistance;
	private IList<CharacterController> AlliesInUnsupportedDistance;
	private IList<CharacterController> AlliesInBackUpDistance;		
	
	private IList<CharacterController> AllyCreepsInSupportDistance;
	private IList<CharacterController> AllyCreepsInUnsupportedDistance;
	private IList<CharacterController> AllyCreepsInBackUpDistance;

	private float attackChance;
	private float zoningChance;
	private float runAwayChance;
	private float attackCreepsChance;
	private float attackStructureChance;

	private int actionCoolDown;

	// Jogar para o MatchManager
	private GameObject redTeamSpawnPoint;
	private GameObject blueTeamSpawnPoint;

	private float farDistance;
	private float closeDistance;

	private Vector3 baseTowerFarDistance;
	private Vector3 baseTowerCloseDistance;

	private Vector3 basePlayerFarDistance;
	private Vector3 basePlayerCloseDistance;

	private Vector3 baseCreepsFarDistance;	
	private Vector3 baseCreepsCloseDistance;

	private Vector3 zoningStart;
	private Vector3 zoningEnd;

	// Use this for initialization
	void Start () 
	{
		characterController = GetComponent<CharacterController>();

		ResetPriorities ();

		actionCoolDown = 100;

		CriticalDistance = 10; 
		DesirableDistance = 20; 
		SafeDistance = 30; 
		
		SupportDistance = 10; 
		BackUpDistance = 20; 
		UnsupportedDistance = 30; 

		farDistance = 40;
		closeDistance = 10;
		
		allies = new List<CharacterController>();
		allyCreeps = new List<CharacterController>();
		allyStructures = new List<CharacterController>();

		enemies = new List<CharacterController>();
		enemyCreeps = new List<CharacterController>();
		enemyStructures = new List<CharacterController>();

		AlliesInSupportDistance = new List<CharacterController>();
		AlliesInUnsupportedDistance = new List<CharacterController>();
		AlliesInBackUpDistance = new List<CharacterController>();
		
		AllyCreepsInSupportDistance = new List<CharacterController>();
		AllyCreepsInUnsupportedDistance = new List<CharacterController>();
		AllyCreepsInBackUpDistance = new List<CharacterController>();

		EnemiesInCriticalDistance = new List<CharacterController>();
		EnemiesInDesirableDistance = new List<CharacterController>();
		EnemiesInSafeDistance = new List<CharacterController>();
		
		EnemyCreepsInCriticalDistance = new List<CharacterController>();
		EnemyCreepsInDesirableDistance = new List<CharacterController>();
		EnemyCreepsInSafeDistance = new List<CharacterController>();
		
		EnemyStructuresInCriticalDistance = new List<CharacterController>();
		EnemyStructuresInDesirableDistance = new List<CharacterController>();
		EnemyStructuresInSafeDistance = new List<CharacterController>();

		if (characterController.TeamColor == TeamColor.BLUE_TEAM)
		{
			enemies = CharacterManager.RedCharacters;
			enemyCreeps = CharacterManager.RedCreeps;
			enemyStructures = CharacterManager.RedStructures;
		}
		else
		{
			allies = CharacterManager.BlueCharacters;
			allyCreeps = CharacterManager.BlueCreeps;
			allyStructures = CharacterManager.BlueStructures;
		}

		redTeamSpawnPoint = GameObject.Find ("RedTeamSpawnPoint");
		blueTeamSpawnPoint = GameObject.Find ("BlueTeamSpawnPoint");

		baseTowerFarDistance = Vector3.zero;
		baseTowerCloseDistance = Vector3.zero;
		
		basePlayerFarDistance = Vector3.zero;
		basePlayerCloseDistance = Vector3.zero;
		
		baseCreepsFarDistance = Vector3.zero;
		baseCreepsCloseDistance = Vector3.zero;

		zoningStart = Vector3.zero;
		zoningEnd = Vector3.zero;

		characterController.UseAction (ActionType.MOVE_TO, new Vector3(1f, 0, 0));
	}
	
	// Update is called once per frame
	void Update () 
	{
		CalculateDistances ();
		DrawDistanceLines ();
		EvaluateZoning (ZoningType.DesirableCreeps, ZoningType.SafeTower);

		//if (GameManager.GameClock % 1 == 0)
		//{
			MoveToZoningArea();
		//}


		// Procura pelos Inimigos, Aliados e Estruturas proximas, avaliando suas posiçoes e estados atuais.
		//LookForEnemies ();
		//LookForCreeps ();
		//LookForStructures ();
		//LookForFriends ();

		// Faz uma analise com base nos inimigos e define suas prioridades
		//EvaluatePriorities ();

		// Encontra a melhor posiçao para esta situaçao
		//CalculateZoning ();

		// Executa as açoes deste personagem
		//PerformeActions ();
	}

	private void LookForEnemies()
	{	
		foreach (CharacterController enemy in enemies)
		{
			float distance = (characterController.CharacterPosition - enemy.CharacterPosition).magnitude;

			if(distance >= SafeDistance)
			{
				if(!EnemiesInSafeDistance.Contains(enemy)) 
					EnemiesInSafeDistance.Add(enemy);
			}
			else
			{
				if(distance <= CriticalDistance)
				{
					if(!EnemiesInCriticalDistance.Contains(enemy)) 
						EnemiesInCriticalDistance.Add(enemy);
				}
				else
				{
					if(!EnemiesInDesirableDistance.Contains(enemy)) 
						EnemiesInDesirableDistance.Add(enemy);
				}
			}
		}
	}

	private void CalculateDistances()
	{
		CharacterController structureController = GetClosestEnemy (enemyStructures);

		if (structureController != null) 
		{			
			Vector3 structureDistance = structureController.CharacterPosition;

			if (characterController.TeamColor == TeamColor.RED_TEAM) 
			{
				baseTowerFarDistance = structureDistance + new Vector3 (farDistance, 0, 0);
				baseTowerCloseDistance = structureDistance + new Vector3 (closeDistance, 0, 0);
			} 
			else 
			{
				baseTowerFarDistance = structureDistance - new Vector3 (farDistance, 0, 0);
				baseTowerCloseDistance = structureDistance - new Vector3 (closeDistance, 0, 0);
			}
		}

		CharacterController creepController = GetClosestEnemy (enemyCreeps);
		
		if (creepController != null) 
		{
			Vector3 creepDistance = creepController.CharacterPosition;
			
			if (characterController.TeamColor == TeamColor.RED_TEAM)
			{
				baseCreepsFarDistance = creepDistance + new Vector3(farDistance, 0, 0);
				baseCreepsCloseDistance = creepDistance + new Vector3(closeDistance, 0, 0);
			}
			else
			{
				baseCreepsFarDistance = creepDistance - new Vector3(farDistance, 0, 0);
				baseCreepsCloseDistance = creepDistance - new Vector3(closeDistance, 0, 0);
			}
		}

		CharacterController playerController = GetClosestEnemy (enemies);
		
		if (playerController != null) 
		{
			Vector3 playerDistance = playerController.CharacterPosition;
		
			if (characterController.TeamColor == TeamColor.RED_TEAM)
			{
				basePlayerFarDistance = playerDistance + new Vector3(farDistance, 0, 0);
				basePlayerCloseDistance = playerDistance + new Vector3(closeDistance, 0, 0);
			}
			else
			{
				basePlayerFarDistance = playerDistance - new Vector3(farDistance, 0, 0);
				basePlayerCloseDistance = playerDistance - new Vector3(closeDistance, 0, 0);
			}
		}
	}

	private void DrawDistanceLines()
	{		
		if(enemyStructures.Count > 0)
		{
			DrawLine (baseTowerFarDistance, Color.green);
			DrawLine (baseTowerCloseDistance, Color.red);
		}

		if(enemyCreeps.Count > 0)
		{
			DrawLine (baseCreepsFarDistance, Color.green);
			DrawLine (baseCreepsCloseDistance, Color.red);
		}

		if(enemies.Count > 0)
		{
			//DrawLine (basePlayerFarDistance, Color.green);
			//DrawLine (basePlayerFarDistance, Color.red);
		}
	}

	private void EvaluateZoning(ZoningType priority, ZoningType concern)
	{
		Vector3 baseStart = Vector3.zero;

		Vector3 S1 = Vector3.zero;
		Vector3 S2 = Vector3.zero;
		Vector3 E1 = Vector3.zero;
		Vector3 E2 = Vector3.zero;

		if (characterController.TeamColor == TeamColor.RED_TEAM)
		{
			baseStart = redTeamSpawnPoint.transform.position;
		}
		else
		{
			baseStart = blueTeamSpawnPoint.transform.position;
		}

		switch(priority)
		{
			case ZoningType.SafeTower:
				S1 = baseStart;
				E1 = baseTowerFarDistance;
			break;

			case ZoningType.SafePlayer:
				S1 = baseStart;
				E1 = basePlayerFarDistance;
			break;

			case ZoningType.SafeCreeps:
				S1 = baseStart;
				E1 = baseCreepsFarDistance;
			break;

			case ZoningType.DesirableTower:
				S1 = baseTowerFarDistance;
				E1 = baseTowerCloseDistance;
			break;

			case ZoningType.DesirablePlayer:
				S1 = basePlayerFarDistance;
				E1 = basePlayerCloseDistance;
			break;

			case ZoningType.DesirableCreeps:
				S1 = baseCreepsFarDistance;
				E1 = baseCreepsCloseDistance;
			break;

			case ZoningType.AttackTower:
				S1 = baseTowerCloseDistance;
				if(enemyStructures.Count > 0)
					E1 = GetClosestEnemy(enemyStructures).CharacterPosition;
			break;
			
			case ZoningType.AttackPlayer:
				S1 = basePlayerCloseDistance;
				if(enemies.Count > 0)
					E1 = GetClosestEnemy(enemies).CharacterPosition;
			break;
			
			case ZoningType.AttackCreep:
				S1 = baseCreepsCloseDistance;
				if(enemyCreeps.Count > 0)
					E1 = GetClosestEnemy(enemyCreeps).CharacterPosition;
			break;
		}

		switch (concern) 
		{
			case ZoningType.SafeTower:
				S2 = baseStart;
				E2 = baseTowerFarDistance;
			break;
			
			case ZoningType.SafePlayer:
				S2 = baseStart;
				E2 = basePlayerFarDistance;
			break;
			
			case ZoningType.SafeCreeps:
				S2 = baseStart;
				E2 = baseCreepsFarDistance;
			break;
		}

		if(characterController.TeamColor == TeamColor.BLUE_TEAM)
		{
			if (S1.x > E2.x)
				zoningStart = S2;
			else
				zoningStart = S1;

			if (E1.x > E2.x)
				zoningEnd = E2;
			else
				zoningEnd = E1;
		}
		else
		{
			if (S1.x < E2.x)
				zoningStart = S2;
			else
				zoningStart = S1;
			
			if (E1.x < E2.x)
				zoningEnd = E2;
			else
				zoningEnd = E1;
		}
	}

	private void MoveToZoningArea()
	{
		characterController.UseAction (ActionType.MOVE_TO, zoningEnd);
	}

	private void LookForCreeps()
	{
		foreach (CharacterController creep in enemyCreeps)
		{
			float distance = (characterController.CharacterPosition - creep.CharacterPosition).magnitude;
			
			if(distance >= SafeDistance)
			{
				if(!EnemyCreepsInSafeDistance.Contains(creep)) 
					EnemyCreepsInSafeDistance.Add(creep);
			}
			else
			{
				if(distance <= CriticalDistance)
				{
					if(!EnemyCreepsInCriticalDistance.Contains(creep)) 
						EnemyCreepsInCriticalDistance.Add(creep);
				}
				else
				{
					if(!EnemyCreepsInDesirableDistance.Contains(creep)) 
						EnemyCreepsInDesirableDistance.Add(creep);
				}
			}
		}
	}

	private void LookForStructures()
	{
		foreach (CharacterController creep in enemyCreeps)
		{
			float distance = (characterController.CharacterPosition - creep.CharacterPosition).magnitude;
			
			if(distance >= SafeDistance)
			{
				if(!EnemyStructuresInSafeDistance.Contains(creep)) 
					EnemyStructuresInSafeDistance.Add(creep);
			}
			else
			{
				if(distance <= CriticalDistance)
				{
					if(!EnemyStructuresInCriticalDistance.Contains(creep)) 
						EnemyStructuresInCriticalDistance.Add(creep);
				}
				else
				{
					if(!EnemyStructuresInDesirableDistance.Contains(creep)) 
						EnemyStructuresInDesirableDistance.Add(creep);
				}
			}
		}
	}

	private void LookForFriends()
	{
		foreach (CharacterController ally in allies)
		{
			float distance = (characterController.CharacterPosition - ally.CharacterPosition).magnitude;
			
			if(distance >= UnsupportedDistance)
			{
				if(!AlliesInUnsupportedDistance.Contains(ally)) 
					AlliesInUnsupportedDistance.Add(ally);
			}
			else
			{
				if(distance <= SupportDistance)
				{
					if(!AlliesInSupportDistance.Contains(ally)) 
						AlliesInSupportDistance.Add(ally);
				}
				else
				{
					if(!AlliesInBackUpDistance.Contains(ally)) 
						AlliesInBackUpDistance.Add(ally);
				}
			}
		}

		foreach (CharacterController ally in allyCreeps)
		{
			float distance = (characterController.CharacterPosition - ally.CharacterPosition).magnitude;
			
			if(distance >= UnsupportedDistance)
			{
				if(!AllyCreepsInUnsupportedDistance.Contains(ally)) 
					AllyCreepsInUnsupportedDistance.Add(ally);
			}
			else
			{
				if(distance <= SupportDistance)
				{
					if(!AllyCreepsInSupportDistance.Contains(ally)) 
						AllyCreepsInSupportDistance.Add(ally);
				}
				else
				{
					if(!AllyCreepsInBackUpDistance.Contains(ally)) 
						AllyCreepsInBackUpDistance.Add(ally);
				}
			}
		}
	}

	private void EvaluatePriorities()
	{
		ResetPriorities ();

		// Caso existam inimigos por perto e a minha vida esteja baixa, almenta as chances de fugir.		
		EvaluateFleeChance (EnemiesInSafeDistance, 0.1f, 1);
		EvaluateFleeChance (EnemiesInDesirableDistance, 0.2f, 3);
		EvaluateFleeChance (EnemiesInCriticalDistance, 0.3f, 5);

		// Caso existam inimigos por perto e a vida deles esteja baixa, aumenta as chances de atacar.
		EvaluateAttackChance(EnemiesInSafeDistance, 0.3f, 1);
		EvaluateAttackChance(EnemiesInDesirableDistance, 0.5f, 3);
		EvaluateAttackChance(EnemiesInCriticalDistance, 0.5f, 5);

		// Caso existam creeps inimigas por perto e a vida delas esteja baixa, aumenta as chances de atacar.
		EvaluateAttackCreepsChance(EnemyCreepsInSafeDistance, 0.5f, 1);
		EvaluateAttackCreepsChance(EnemyCreepsInDesirableDistance, 0.4f, 2);
		EvaluateAttackCreepsChance(EnemyCreepsInCriticalDistance, 0.3f, 3);

		// Caso existam creeps inimigas por perto e a vida delas esteja baixa, aumenta as chances de atacar.
		EvaluateStruturesAttackChance(EnemyStructuresInDesirableDistance, 0.3f, 1);
		EvaluateStruturesAttackChance(EnemyStructuresInCriticalDistance, 0.2f, 3);

	}

	private void EvaluateFleeChance(IList<CharacterController> enemies, float minLife, float priority)
	{
		if(enemies.Count > 0)
		{
			float lifePercent = characterController.CharacterModel.LifePercent;
			
			if(lifePercent < minLife)
			{
				runAwayChance = (enemies.Count * 0.1f) + (priority - lifePercent);
			}
		}
	}

	private void EvaluateAttackChance(IList<CharacterController> enemies, float minLife, float priority)
	{
		if(enemies.Count > 0)
		{
			foreach (CharacterController enemy in enemies)
			{
				float lifePercent = enemy.CharacterModel.LifePercent;
				
				if(lifePercent < minLife)
				{
					attackChance += 0.1f + (priority - lifePercent);
				}
			}
		}
	}

	private void EvaluateAttackCreepsChance(IList<CharacterController> creeps, float minLife, float priority)
	{
		if(creeps.Count > 0)
		{
			foreach (CharacterController creep in creeps)
			{
				float lifePercent = creep.CharacterModel.LifePercent;
				
				if(lifePercent < minLife)
				{
					attackCreepsChance += 0.1f + (priority - lifePercent);
				}
			}
		}
	}

	private void EvaluateStruturesAttackChance(IList<CharacterController> structures, float minLife, float priority)
	{
		if(structures.Count > 0)
		{
			foreach (CharacterController structure in structures)
			{
				float lifePercent = structure.CharacterModel.LifePercent;
				
				if(lifePercent < minLife)
				{
					float safetyCounter = 0;
					safetyCounter = AlliesInSupportDistance.Count * 0.2f;
					safetyCounter = AlliesInBackUpDistance.Count * 0.1f;
					safetyCounter = AllyCreepsInSupportDistance.Count * 0.05f;

					float dangerCounter = 0;
					dangerCounter = EnemyCreepsInCriticalDistance.Count * 0.1f;
					dangerCounter = EnemyCreepsInDesirableDistance.Count * 0.05f;
					dangerCounter = EnemiesInCriticalDistance.Count * 0.2f;
					dangerCounter = EnemiesInDesirableDistance.Count * 0.1f;

					attackStructureChance += 0.1f + (priority - lifePercent) - dangerCounter + safetyCounter;
				}
			}
		}
	}

	private void ResetPriorities()
	{
		attackChance = 0;
		zoningChance = 0;
		runAwayChance = 0;
		attackCreepsChance = 0;
		attackStructureChance = 0;
	}

	private void CalculateZoning()
	{
		if(EnemyStructuresInCriticalDistance.Count < 1 && EnemyStructuresInDesirableDistance.Count < 1)
		{
			zoningChance += 0.1f;
		}

		if (EnemyCreepsInCriticalDistance.Count < 1 && EnemyCreepsInDesirableDistance.Count < 1)
		{
			zoningChance += 0.2f;
		}

		if (EnemiesInCriticalDistance.Count < 1 && EnemiesInDesirableDistance.Count < 1)
		{
			zoningChance += 0.3f;
		}
	}

	private void PerformeActions()
	{
		if(actionCoolDown <= 0)
		{
			// Caso nao exista nenhum inimigo por perto, avança e ataca o primeiro alvo a frente
			if(zoningChance > attackChance && zoningChance > attackCreepsChance && zoningChance > attackStructureChance)
			{
				Vector3 zoningLocation = Vector3.zero;

				if(characterController.TeamColor == TeamColor.BLUE_TEAM)
					zoningLocation = redTeamSpawnPoint.transform.position;
				else
					zoningLocation = blueTeamSpawnPoint.transform.position;

				characterController.UseAction(ActionType.PURSUIT_AND_ATTACK, characterController.CharacterPosition);
				characterController.UseAction(ActionType.CLICK_TARGET, zoningLocation);
			}

			// Caso esteja em perigo, foge em direçao a base
			if (runAwayChance > attackChance || runAwayChance > attackCreepsChance || runAwayChance > zoningChance)
			{
				Vector3 runAwayLocation = Vector3.zero;

				if(characterController.TeamColor == TeamColor.BLUE_TEAM)
					runAwayLocation = blueTeamSpawnPoint.transform.position;
				else
					runAwayLocation = redTeamSpawnPoint.transform.position;

				characterController.UseAction(ActionType.MOVE_AND_CLEAR, runAwayLocation);
			}

			// Caso exista algum inimigo em condiçoes desfavoraveis, ataca 
			if(attackChance > runAwayChance && attackChance > attackCreepsChance && attackChance > attackStructureChance)
			{
				characterController.UseAction(ActionType.PURSUIT_AND_ATTACK, GetClosestEnemy(EnemiesInCriticalDistance));
			}

			// Caso exista alguma creep morrendo proximo, ataca 
			if(attackCreepsChance > runAwayChance && attackCreepsChance > attackChance && attackCreepsChance > attackStructureChance)
			{
				characterController.UseAction(ActionType.PURSUIT_AND_ATTACK, GetClosestDyingEnemy(enemyCreeps));
			}

			// Caso alguma estrutura esteja desprotegida, ataca 
			if(attackStructureChance > runAwayChance && attackStructureChance > attackChance && attackStructureChance > attackCreepsChance)
			{
				characterController.UseAction(ActionType.PURSUIT_AND_ATTACK, GetClosestEnemy(enemyStructures));
			}

			actionCoolDown = 100;
		}

		actionCoolDown--;
	}

	private CharacterController GetClosestEnemy(IList<CharacterController> enemies)
	{
		CharacterController closestEnemy = null;
		float closestDistance = float.MaxValue;

		foreach (CharacterController enemy in enemies) 
		{
			float distance = (enemy.CharacterPosition - characterController.CharacterPosition).magnitude;

			if(distance <= closestDistance)
			{
				closestDistance = distance;
				closestEnemy = enemy;
			}
		}

		return closestEnemy;
	}

	private CharacterController GetClosestDyingEnemy(IList<CharacterController> enemies)
	{
		CharacterController closestDyingEnemy = null;
		float closestDistance = float.MaxValue;
		float lowerLifePercent = 1;
		
		foreach (CharacterController enemy in enemies) 
		{
			float distance = (enemy.CharacterPosition - characterController.CharacterPosition).magnitude;
			float lifePercent = enemy.CharacterModel.LifePercent;

			if(lifePercent <= lowerLifePercent)
			{	
				if(distance <= closestDistance)
				{
					closestDyingEnemy = enemy;
				}
			}
		}
		
		return closestDyingEnemy;
	}

	private void DrawLine(Vector3 basePoint, Color color)
	{
		Vector3 point1 = new Vector3 (basePoint.x, 0.5f, basePoint.z - 50);
		Vector3 point2 = new Vector3 (basePoint.x, 0.5f, basePoint.z + 50);;

		Debug.DrawLine (point1, point2, color);
	}

	private enum ZoningType
	{
		None,
		SafeTower,
		SafePlayer,
		SafeCreeps,
		DesirableTower,
		DesirablePlayer,
		DesirableCreeps,
		AttackTower,
		AttackPlayer,
		AttackCreep
	}
}
