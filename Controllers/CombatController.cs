﻿using UnityEngine;
using System.Collections;

public class CombatController 
{
	public CombatController()
	{
	
	}
	
	public GameObject PursuitAndAttack(CharacterController player)
	{
		GameObject pursuitAndAttackGO = ObjectManager.InstantiateObject("Actions/Combat/PursuitAndAttackAction", player.transform.position);
		PursuitAndAttackAction pursuitAndAttack = pursuitAndAttackGO.GetComponent<PursuitAndAttackAction> ();
		
		pursuitAndAttackGO.transform.parent = player.transform;
		
		pursuitAndAttack.UseAction (player);
		
		return pursuitAndAttackGO;
	}

	public GameObject PursuitAndAttack(CharacterController player, CharacterController target)
	{
		GameObject pursuitAndAttackGO = ObjectManager.InstantiateObject("Actions/Combat/PursuitAndAttackAction", player.transform.position);
		PursuitAndAttackAction pursuitAndAttack = pursuitAndAttackGO.GetComponent<PursuitAndAttackAction> ();
		
		pursuitAndAttackGO.transform.parent = player.transform;
		
		pursuitAndAttack.UseAction (player, target);
		
		return pursuitAndAttackGO;
	}
}
