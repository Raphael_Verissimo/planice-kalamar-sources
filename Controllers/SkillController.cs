﻿using UnityEngine;
using System.Collections;

public class SkillController 
{
	public SkillController()
	{
	
	}

	public GameObject CastParalyse(CharacterController player)
	{
		GameObject paralyseActionGO = ObjectManager.InstantiateObject("Actions/Skills/ParalyseAction", player.transform.position);
		ParalyseAction paralyseAction = paralyseActionGO.GetComponent<ParalyseAction> ();
		
		paralyseAction.transform.parent = player.transform;
		
		paralyseAction.UseAction (player);
		return paralyseActionGO;
	}

	public GameObject CastDrainLife(CharacterController player)
	{
		GameObject drainLifeActionGO = ObjectManager.InstantiateObject("Actions/Skills/DrainLifeAction", player.transform.position);
		DrainLifeAction drainLifeAction = drainLifeActionGO.GetComponent<DrainLifeAction> ();
		
		drainLifeAction.transform.parent = player.transform;
		
		drainLifeAction.UseAction (player);
		return drainLifeActionGO;
	}

	public GameObject CastFireBall(CharacterController player)
	{
		GameObject fireBallActionGO = ObjectManager.InstantiateObject("Actions/Skills/FireballAction", player.transform.position);
		FireballAction fireBallAction = fireBallActionGO.GetComponent<FireballAction> ();
		
		fireBallAction.transform.parent = player.transform;
		
		fireBallAction.UseAction (player);
		return fireBallActionGO;
	}

	public GameObject CastPowerShot(CharacterController player)
	{
		GameObject powerShotActionGO = ObjectManager.InstantiateObject("Actions/Skills/PowerShotAction", player.transform.position);
		PowerShotAction powerShotAction = powerShotActionGO.GetComponent<PowerShotAction> ();
		
		powerShotAction.transform.parent = player.transform;
		
		powerShotAction.UseAction (player);
		return powerShotActionGO;
	}

	public GameObject CastArrowShower(CharacterController player)
	{
		GameObject arrowShowerActionGO = ObjectManager.InstantiateObject("Actions/Skills/ArrowShowerAction", player.transform.position);
		ArrowShowerAction arrowShowerAction = arrowShowerActionGO.GetComponent<ArrowShowerAction> ();
		
		arrowShowerAction.transform.parent = player.transform;
		
		arrowShowerAction.UseAction (player);
		return arrowShowerActionGO;
	}

	public GameObject CastBulletTime(CharacterController player)
	{
		GameObject bulletTimeActionGO = ObjectManager.InstantiateObject("Actions/Skills/BulletTimeAction", player.transform.position);
		BulletTimeAction bulletTimeAction = bulletTimeActionGO.GetComponent<BulletTimeAction> ();
		
		bulletTimeAction.transform.parent = player.transform;
		
		bulletTimeAction.UseAction (player);
		return bulletTimeActionGO;
	}

	public GameObject CastDefense(CharacterController player)
	{
		GameObject defenseActionGO = ObjectManager.InstantiateObject("Actions/Skills/DefenseAction", player.transform.position);
		DefenseAction defenseAction = defenseActionGO.GetComponent<DefenseAction> ();
		
		defenseAction.transform.parent = player.transform;
		
		defenseAction.UseAction (player);
		return defenseActionGO;
	}

	public GameObject CastBraver(CharacterController player)
	{
		GameObject braverActionGO = ObjectManager.InstantiateObject("Actions/Skills/BraverAction", player.transform.position);
		BraverAction braverAction = braverActionGO.GetComponent<BraverAction> ();
		
		braverAction.transform.parent = player.transform;
		
		braverAction.UseAction (player);
		return braverActionGO;
	}

	public GameObject CastThrowAxe(CharacterController player)
	{
		GameObject throwAxeActionGO = ObjectManager.InstantiateObject("Actions/Skills/ThrowAxeAction", player.transform.position);
		ThrowAxeAction throwAxeAction = throwAxeActionGO.GetComponent<ThrowAxeAction> ();
		
		throwAxeAction.transform.parent = player.transform;
		
		throwAxeAction.UseAction (player);
		return throwAxeActionGO;
	}
}
