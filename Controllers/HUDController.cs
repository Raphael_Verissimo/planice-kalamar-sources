﻿using UnityEngine;
using System.Collections;

public class HUDController : MonoBehaviour 
{
	#region Score
	// Score HUD
	private GameObject redTeamScoreCanvas;
	private GameObject blueTeamScoreCanvas;
	
	private GameObject killsCounterCanvas;
	private GameObject deathsCounterCanvas;
	private GameObject assistCounterCanvas;
	private GameObject creepsCounterCanvas;

	private GameObject hoursCanvas;
	private GameObject minutesCanvas;
	private GameObject secondsCanvas;

	private UnityEngine.UI.Text redTeamScore;
	private UnityEngine.UI.Text blueTeamScore;
	
	private UnityEngine.UI.Text killsCounter;
	private UnityEngine.UI.Text deathsCounter;
	private UnityEngine.UI.Text assistCounter;
	private UnityEngine.UI.Text creepsCounter;
	
	private UnityEngine.UI.Text hours;
	private UnityEngine.UI.Text minutes;
	private UnityEngine.UI.Text seconds;
	#endregion

	#region Player Info
	// Player Info HUD
	private GameObject attackDamageCanvas;
	private GameObject magicAttackCanvas;
	private GameObject armorCanvas;
	private GameObject magicResistCanvas;
	private GameObject attackSpeedCanvas;
	private GameObject coolDownReductionCanvas;	
	private GameObject unknownCanvas;
	private GameObject movimentSpeedCanvas;

	private UnityEngine.UI.Text attackDamage;
	private UnityEngine.UI.Text magicAttack;
	private UnityEngine.UI.Text armor;
	private UnityEngine.UI.Text magicResist;
	private UnityEngine.UI.Text attackSpeed;
	private UnityEngine.UI.Text coolDownReduction;	
	private UnityEngine.UI.Text unknown;
	private UnityEngine.UI.Text movimentSpeed;
	#endregion

	// Use this for initialization
	void Start () 
	{
		InitScore ();
		InitPlayerInfo ();
	}

	private void InitScore()
	{
		redTeamScoreCanvas = GameObject.Find ("RedTeamScore");
		blueTeamScoreCanvas = GameObject.Find ("BlueTeamScore");
		
		killsCounterCanvas = GameObject.Find ("KillsCounter");
		deathsCounterCanvas = GameObject.Find ("DeathsCounter");
		assistCounterCanvas = GameObject.Find ("AssistCounter");
		creepsCounterCanvas = GameObject.Find ("CreepsCounter");
		
		//hoursCanvas = GameObject.Find ("Hours");
		minutesCanvas = GameObject.Find ("Minutes");
		secondsCanvas = GameObject.Find ("Seconds");
		
		redTeamScore = redTeamScoreCanvas.GetComponent<UnityEngine.UI.Text> ();
		blueTeamScore = blueTeamScoreCanvas.GetComponent<UnityEngine.UI.Text> ();
		
		killsCounter = killsCounterCanvas.GetComponent<UnityEngine.UI.Text> ();
		deathsCounter = deathsCounterCanvas.GetComponent<UnityEngine.UI.Text> ();
		assistCounter = assistCounterCanvas.GetComponent<UnityEngine.UI.Text> ();
		creepsCounter = creepsCounterCanvas.GetComponent<UnityEngine.UI.Text> ();
		
		//hours = hoursCanvas.GetComponent<UnityEngine.UI.Text> ();
		minutes = minutesCanvas.GetComponent<UnityEngine.UI.Text> ();
		seconds = secondsCanvas.GetComponent<UnityEngine.UI.Text> ();
	}

	private void InitPlayerInfo()
	{
		attackDamageCanvas = GameObject.Find ("AttackDamage");
		magicAttackCanvas = GameObject.Find ("MagicDamage");
		armorCanvas = GameObject.Find ("Armor");
		magicResistCanvas = GameObject.Find ("MagicResist");
		attackSpeedCanvas = GameObject.Find ("AttackSpeed");
		coolDownReductionCanvas = GameObject.Find ("CoolDownReduction");
		//unknownCanvas = GameObject.Find ("???");
		movimentSpeedCanvas = GameObject.Find ("MovimentSpeed");

		attackDamage = attackDamageCanvas.GetComponent<UnityEngine.UI.Text> ();
		magicAttack = magicAttackCanvas.GetComponent<UnityEngine.UI.Text> ();
		armor = armorCanvas.GetComponent<UnityEngine.UI.Text> ();
		magicResist = magicResistCanvas.GetComponent<UnityEngine.UI.Text> ();
		attackSpeed = attackSpeedCanvas.GetComponent<UnityEngine.UI.Text> ();
		coolDownReduction = coolDownReductionCanvas.GetComponent<UnityEngine.UI.Text> ();
		//unknown = unknownCanvas.GetComponent<UnityEngine.UI.Text> ();
		movimentSpeed = movimentSpeedCanvas.GetComponent<UnityEngine.UI.Text> ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		UpdatePlayerInfo ();
		UpdateClock ();
	}

	private void UpdatePlayerInfo()
	{
		if (CharacterManager.MainCharacter != null)
		{
			attackDamage.text = CharacterManager.MainCharacter.AttackDamage.ToString();
			magicAttack.text = CharacterManager.MainCharacter.MagicDamage.ToString();
			armor.text = CharacterManager.MainCharacter.Armor.ToString();
			magicResist.text = CharacterManager.MainCharacter.MagicResist.ToString();
			attackSpeed.text = CharacterManager.MainCharacter.AttackSpeed.ToString();
			coolDownReduction.text = CharacterManager.MainCharacter.CoolDownReduction.ToString();
			//unknown.text = CharacterManager.MainCharacter.AttackDamage.ToString();
			movimentSpeed.text = CharacterManager.MainCharacter.MovimentSpeed.ToString();
		}
	}

	private void UpdateClock()
	{
		int clockMinutes = 0;
		int clockSeconds = 0;

		clockMinutes = GameManager.GameClock / 60;		
		clockSeconds = GameManager.GameClock - (clockMinutes * 60);

		if(clockSeconds < 10)
			seconds.text = "0"+clockSeconds.ToString();
		else
			seconds.text = clockSeconds.ToString();

		if(clockMinutes < 10)
			minutes.text = "0"+clockMinutes.ToString();
		else
			minutes.text = clockMinutes.ToString();
	}
}
