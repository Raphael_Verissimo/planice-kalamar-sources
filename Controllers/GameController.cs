﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour 
{
	private float gameTime;
	private int gameClock;

	public float GameTime {get{ return gameTime;}}
	public int GameClock {get{ return gameClock;}}

	// Use this for initialization
	void Start () 
	{
		gameTime = 0;
		gameClock = 0;
	}
	
	// Update is called once per frame
	void Update () 
	{
		gameTime = Time.fixedTime;
		gameClock = ((int)Time.fixedTime);
	}
}
