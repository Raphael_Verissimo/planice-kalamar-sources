﻿using UnityEngine;
using System.Collections;

public class ActionController 
{
	public ActionController()
	{

	}

	public void UseAction(ActionType actionType, CharacterController target, CharacterController caster)
	{
		switch(actionType)
		{
			case ActionType.CLICK_TARGET_GO:
			{
				if(target.TeamColor != caster.TeamColor)
				{
					caster.SetTarget(target);
				}
			}
			break;

			case ActionType.PURSUIT_AND_ATTACK:
			{
				if(!caster.IsLocked)
				{
					if(target.CharacterGO != caster.CharacterGO)
					{	
						caster.ClearActions();
						caster.SetTarget(target);
						caster.CurrentActionGO = CombatManager.PursuitAndAttack(caster, target);						
					}
				}
			}
			break;
		}
	}

	public void UseAction(ActionType actionType, Vector3 target, CharacterController caster)
	{
		switch(actionType)
		{
			case ActionType.CLICK_TARGET:
				caster.SetTarget(target);			
			break;
		}
		if(!caster.IsLocked)
		{
			switch(actionType)
			{
				/* ======================= DEFAULT ACTIONS ======================= */
				case ActionType.MOVE_TO:				
					caster.MoveTo(target, false);
				break;

				case ActionType.MOVE_AND_CLEAR:
					caster.ClearActions();
					caster.MoveTo(target, true);
				break;

				/* ======================= COMBAT ACTIONS ======================== */
				case ActionType.PURSUIT_AND_ATTACK:
					caster.ClearActions();				
					caster.CurrentActionGO = CombatManager.PursuitAndAttack(caster);
				break;

				/* ======================== MAGE ACTIONS ======================== */
				
				case ActionType.PARALYSE_ACTION:
					caster.ClearActions();
					caster.CurrentActionGO = SkillManager.CastParalyse(caster);
				break;

				case ActionType.DRAIN_LIFE_ACTION:
					caster.ClearActions();
					caster.CurrentActionGO = SkillManager.CastDrainLife(caster);
				break;

				case ActionType.FIREBALL_ACTION:
					caster.ClearActions();
					caster.CurrentActionGO = SkillManager.CastFireBall(caster);
				break;

				/* ====================== WARRIOR ACTIONS ======================== */

				case ActionType.DEFENSE_ACTION:
					caster.ClearActions();
					caster.CurrentActionGO = SkillManager.CastDefense(caster);
				break;
				
				case ActionType.BRAVER_ACTION:
					caster.ClearActions();
					caster.CurrentActionGO = SkillManager.CastBraver(caster);
				break;

				case ActionType.THROW_AXE_ACTION:
					caster.ClearActions();
					caster.CurrentActionGO = SkillManager.CastThrowAxe(caster);
				break;

				/* ======================= ARCHER ACTIONS ======================= */

				case ActionType.POWER_SHOT_ACTION:
					caster.ClearActions();
					caster.CurrentActionGO = SkillManager.CastPowerShot(caster);
				break;				
				case ActionType.ARROW_SHOWER_ACTION:
					caster.ClearActions();
					caster.CurrentActionGO = SkillManager.CastArrowShower(caster);
				break;
				case ActionType.BULLET_TIME_ACTION:
					caster.ClearActions();
					caster.CurrentActionGO = SkillManager.CastBulletTime(caster);
				break;
								

				/* ======================= OTHER ACTIONS ======================= *
				case ActionType.DIE:
					Death((int)target.x, (int)target.y);
				break;

				case ActionType.RESPAWN:
					ReSpawn();
				break;
				*/
			}				
		}
	}
}
