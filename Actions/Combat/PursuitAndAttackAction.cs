﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PursuitAndAttackAction : Action
{	
	#region Atributos Internos	
	private bool isMovingWithOutTarget;
	
	private IList<CharacterController> targetsInRange;
	private CharacterController currentTarget;
	private CharacterController firstTarget;
		private GameObject targetAreaGO;
	
	private bool isPursuiting;
	private bool isOutOfRange;

	private Vector3 lastTargetPosition;

	private float targetDistance;

	private string weaponPrefab;
	#endregion

	public void UseAction (CharacterController player, CharacterController target)
	{
		currentTarget = target;
		UseAction (player);
	}
	
	public void UseAction (CharacterController player)
	{
		if (player != null) 
		{
			lastTargetPosition = Vector3.zero;
			isMovingWithOutTarget = false;
			targetDistance = -1;
			
			currentCoolDown = 0;
			coolDown = player.AttackCoolDown;
			width = 80f;
			area = 80f;
			
			targetsInRange = new List<CharacterController> ();		
			lastSelectedTarget = Vector3.zero;		
			caster = player;
			isMainPlayerAction = player.IsMainPlayer;
			
			currentPosition = player.CharacterPosition;
			weaponPrefab = player.WeaponPrefab;
			
			targetAreaGO = ObjectManager.InstantiateObject ("Actions/TargetTypes/TargetArea", new Vector3 (base.currentPosition.x, -10, base.currentPosition.z));
			TargetArea targetArea = targetAreaGO.GetComponent<TargetArea> ();
			
			targetAreaGO.transform.parent = this.transform;
			targetArea.ShotAndStay (this, "", OnHitTarget);

			caster.IsIdle = false;
		}
	}
	
	void FixedUpdate()
	{
		if (caster == null)
		{
			Destroy(this.gameObject);
			return;
		}

		if (currentTarget != null)
		{
			PursuitAndAttack();
		}
		else
		{
			if(caster.CharacterModel.CharacterName.Equals("Debugger"))
			{
				Debug.Log("Targets " + targetsInRange.Count);
			}

			if (caster.GetTarget() != Vector3.zero || caster.GetTargetController() != null)
			{
				// JA CLICKOU
				if(lastSelectedTarget != caster.GetTarget())
				{
					if(caster.IsMainPlayer){GameManager.ClearTargets();}
					lastSelectedTarget = caster.GetTarget();
					ResolveTarget(lastSelectedTarget);
				}		

				// JA SELECIONOU
				if(caster.GetTargetController() != null)
				{
					if(caster.IsMainPlayer){GameManager.ClearTargets();}
					currentTarget = caster.GetTargetController();
				}	

				float distanceToTarget = (caster.Destination - caster.CharacterPosition).magnitude;
				
				if(distanceToTarget < 0.3f)
				{
					caster.IsIdle = true;
					caster.SetTarget(null);
					Destroy(this.gameObject);
					return;
				}
			}
			else
			{
				// AINDA N CLICKOU
				if(isMainPlayerAction)
					GameManager.DrawAttackTarget();
			}
		}
	}

	internal void OnHitTarget(CharacterController target, GameObject shotGO)
	{
		if (target.CharacterGO.tag == "Player" && target.IsAlive)
		{
			if(target.TeamColor != caster.TeamColor)
			{
				if(!targetsInRange.Contains(target))
				{
					if(caster.CharacterModel.CharacterName.Equals("Debugger"))
					{
						Debug.Log("Targets " + targetsInRange.Count);
					}

					if(target != caster)
					{
						//if(isMainPlayerAction)
						//{
							targetsInRange.Add(target);
							
							//if (isMovingWithOutTarget)
							//{
								if(firstTarget == null)
									firstTarget = target;

								currentTarget = firstTarget;
							//}
						//}
					}
				}
			}
		}	

		if (target.CharacterGO.tag == "Castle")
		{
			if(target.TeamColor != caster.TeamColor)
			{
				if(!targetsInRange.Contains(target))
				{
					if(target != caster)
					{
						targetsInRange.Add(target);
						
						if (isMovingWithOutTarget)
						{
							currentTarget = target;
						}
					}
				}			
			}
		}
	}

	private void ResolveTarget(Vector3 clickPos)
	{
		if (targetsInRange.Count < 1)
		{
			isMovingWithOutTarget = true;
			caster.UseAction(ActionType.MOVE_TO, clickPos);
			
			if(isMainPlayerAction)
				GameManager.DrawSelectedAttackTarget(clickPos);
		}
		else
		{
			if(caster.CharacterModel.CharacterName.Equals("Debugger"))
			{
				Debug.Log("Targets " + targetsInRange.Count);
			}

			float minDistance = float.MaxValue;

			//CharacterController firstTarget = targetsInRange[0];
			foreach (CharacterController obj in targetsInRange)		
			{
				float distance = (clickPos - obj.CharacterPosition).magnitude;
					
				if (distance <= minDistance)
				{
					minDistance = distance;		
					currentTarget = obj;
					//caster.UseAction(ActionType.PURSUIT_AND_ATTACK, obj);
				}
			}
		}
	}

	private void PursuitAndAttack()
	{
		if(caster.CharacterModel.CharacterName.Equals("Debugger"))
		{
			Debug.Log("Targets " + targetsInRange.Count);
		}

		if(!caster.IsAlive)
		{
			Destroy(this.gameObject);
			return;
		}

		if(currentTarget.IsAlive)
		{
			if(isMainPlayerAction)
				GameManager.DrawSelectedAttackTarget (currentTarget.CharacterGO);
				
			targetDistance = (currentTarget.CharacterPosition - caster.CharacterPosition).magnitude;

			if(currentTarget.CharacterGO.tag.Equals("Castle"))
				caster.AttackRange += 10; 

			if (targetDistance > 0)
			{				
				if (targetDistance > caster.AttackRange)
					Pursuit();
				else	
					Attack();
			}

			if(currentTarget.CharacterGO.tag.Equals("Castle"))
				caster.AttackRange -= 10; 
		}
		else
		{
			targetsInRange.Remove(currentTarget);
			caster.UseAction(ActionType.PURSUIT_AND_ATTACK, Vector3.zero);
			caster.SetTarget(caster.CharacterPosition);			
			if(caster.IsMainPlayer){GameManager.ClearAttackTargets();}

			Destroy(this.gameObject);
		}
	}
	
	private void Pursuit()
	{	
		if(caster.Destination != currentTarget.CharacterPosition)
		{
		//if(lastTargetPosition != currentTarget.CharacterPosition)
		//{
			//lastTargetPosition = currentTarget.CharacterPosition;

			Vector3 targetMove = currentTarget.CharacterPosition;
			Vector3 targetDirection = (currentTarget.CharacterPosition - caster.CharacterPosition).normalized;
			targetMove -= targetDirection;

			caster.UseAction(ActionType.MOVE_TO, targetMove);
		//}
		}
	}
	
	private void Attack()
	{
		caster.StopMoving();

		if (caster.CurrentCoolDown >= caster.AttackCoolDown)
		{
			caster.CurrentCoolDown = 0;
			
			DestroyObject(targetAreaGO);

			switch (caster.Weapon)
			{
				case WeaponType.BOW:
					ShotArrow();
				break;
				
				case WeaponType.SWORD:
					SwingSword();
				break;
				
				case WeaponType.SLING:
					SlingShot();
				break;
			}
		}
	}
	
	private void ShotArrow()
	{
		GameObject arrowGO = ObjectManager.InstantiateObject(caster.WeaponPrefab, new Vector3(caster.CharacterPosition.x, 1.5f, caster.CharacterPosition.z));
		MissleFollow missle = arrowGO.GetComponent<MissleFollow>();
		
		float randomShot = Random.value * 100;
		float speed = caster.AttackDuration;
		int damage = caster.AttackDamage;
		int missleLifeTime = 200;
		
		if(randomShot <= caster.CriticalChance) 
		{
			speed *= 1.5f;
			damage = ((int)(caster.CriticalDamage * damage));
		}		

		missle.Shot(caster, currentTarget, speed, missleLifeTime, ActionEffectTypes.PHYSICAL_DAMAGE, damage, 0, true, false);
	}
	
	private void SlingShot()
	{
		Vector3 targetPos = new Vector3(currentTarget.CharacterPosition.x, 2f, currentTarget.CharacterPosition.z);
		Vector3 direction = (targetPos - caster.CharacterPosition).normalized;
		Vector3 misslePosition = new Vector3 (caster.CharacterPosition.x, 1.5f, caster.CharacterPosition.z);
		misslePosition += direction;
		
		GameObject slingGO = ObjectManager.InstantiateObject(caster.WeaponPrefab, misslePosition);
		MissleFollow missle = slingGO.GetComponent<MissleFollow>();

		float randomShot = Random.value * 100;
		float speed = caster.AttackDuration;
		int damage = caster.AttackDamage;
		int missleLifeTime = 200;
		
		if(randomShot <= caster.CriticalChance) 
		{
			speed *= 1.5f;
			damage = ((int)(caster.CriticalDamage * damage));
		}		

		missle.Shot(caster, currentTarget, speed, missleLifeTime, ActionEffectTypes.PHYSICAL_DAMAGE, damage, 0, true, true);
	}
	
	private void SwingSword()
	{
		GameObject sword = ObjectManager.InstantiateObject(caster.WeaponPrefab, new Vector3(caster.CharacterPosition.x, 1, caster.CharacterPosition.z));
		sword.transform.LookAt (currentTarget.transform);

		SwingWeapon swing = sword.GetComponent<SwingWeapon> ();
		
		float randomShot = Random.value * 100;
		int damage = caster.AttackDamage;
		
		if(randomShot <= caster.CriticalChance) 
		{
			damage = ((int)(caster.CriticalDamage * damage));
		}		

		swing.Swing (caster, ActionEffectTypes.PHYSICAL_DAMAGE, damage, 0);
	}
}
