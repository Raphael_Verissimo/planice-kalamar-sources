﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SwingWeapon : MonoBehaviour 
{
	private int timer;
	private int randOrientation;

	private bool isActive;
	
	private CharacterController caster;
	
	private ActionEffectTypes effectType;
	private int amount;
	private int duration;

	private IList<CharacterController> HitedCharacters = new List<CharacterController> ();

	// Use this for initialization
	void Start () 
	{
		timer = 0;
		float randonDirection = -50 + (Random.value * 50);
		randOrientation = ((int)(Random.value*100)) % 2;

		//Debug.Log ("Random " + randOrientation);

		if(randOrientation == 0)
			transform.Rotate (new Vector3 (0f, 90f, randonDirection));
		else
			transform.Rotate (new Vector3 (0f, -90f, -randonDirection));
	}
	
	// Update is called once per frame
	void Update () 
	{		
		timer++;
		if(randOrientation == 0)
			transform.Rotate (new Vector3 (0f, -10f, 0f));
		else
			transform.Rotate (new Vector3 (0f, 10f, 0f));

		if (timer > 10)
			DestroyObject(this.gameObject);
	}

	public void Swing(CharacterController caster, ActionEffectTypes effectType, int amount, int duration)
	{
		isActive = true;		

		this.caster = caster;		
		this.amount = amount;
		this.duration = duration;
		this.effectType = effectType;
	}

	void OnTriggerEnter(Collider other) 
	{
		if (other.gameObject.tag == "Player" && isActive)
		{
			CharacterController otherController = CharacterManager.GetController(other.gameObject);
		
			if(otherController.TeamColor != caster.TeamColor)
			{
				if(otherController != caster && !HitedCharacters.Contains(otherController))
				{
					if(otherController != null)
					{
						HitedCharacters.Add(otherController);
						
						if(other.gameObject.name == "Head")
							otherController.ApplyEffect(effectType, caster, amount * 2, duration);
						else
							otherController.ApplyEffect(effectType, caster, amount, duration);
												
						isActive = false;
					}
				}	
			}					
		}

		if (other.gameObject.tag == "Shield")
		{
			isActive = false;
		}

		if (other.tag.Equals("Castle"))
		{
			CharacterController otherController = CharacterManager.GetController(other.gameObject);
			
			if(otherController.TeamColor != caster.TeamColor)
			{
				if(otherController != caster && !HitedCharacters.Contains(otherController))
				{
					if(otherController != null)
					{
						HitedCharacters.Add(otherController);						
						otherController.ApplyEffect(effectType, caster, amount, duration);						
						isActive = false;
					}
				}	
			}	
		}
	}
}
