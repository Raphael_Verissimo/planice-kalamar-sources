using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MissleFollow : MonoBehaviour 
{
	private bool isActive;

	private CharacterController target;

	private CharacterController caster;
	private Vector3 direction;
	private float speed;

	private bool isTriggerMissle;
	private int lifeTime;

	private ActionEffectTypes effectType;
	private int amount;
	private int duration;

	private bool followTarget;
	private bool bounceOnTarget;
	
	private IList<CharacterController> HitedCharacters = new List<CharacterController> ();

	// Update is called once per frame
	void FixedUpdate () 
	{ 
		lifeTime--;

		if (lifeTime <= 0)
			DestroyObject (this.gameObject);

		if (followTarget && isActive) 
		{
			this.transform.LookAt (new Vector3 (target.CharacterPosition.x, 1.5f, target.CharacterPosition.z));

			Vector3 targetPos = new Vector3(target.CharacterPosition.x, 1.5f, target.CharacterPosition.z);

			Vector3 direction = targetPos - this.transform.position;

			GetComponent<Rigidbody>().velocity = direction.normalized * speed;

			//GetComponent<Rigidbody>().useGravity = false;
		}
	}

	public void Shot(CharacterController caster, CharacterController target, float speed, int lifetime, ActionEffectTypes effectType, int amount, int duration, bool followTarget, bool bounceOnTarget)
	{		
		this.target = target;

		if (caster.CharacterModel.CharacterClass == ClassType.TOWER)
			this.transform.position += Vector3.up * 10;
		
		Shot(caster, target.CharacterPosition, speed, lifetime, effectType, amount, duration, followTarget, bounceOnTarget);
	}

	public void Shot(CharacterController caster, Vector3 target, float speed, int lifetime, ActionEffectTypes effectType, int amount, int duration, bool followTarget, bool bounceOnTarget)
	{	
		Physics.IgnoreLayerCollision (8, 8, true);
		Physics.IgnoreLayerCollision (8, 9, true);
		Physics.IgnoreLayerCollision (9, 8, true);

		this.caster = caster;
		this.speed = speed;
		this.amount = amount;

		this.lifeTime = lifetime;		

		this.followTarget = followTarget;
		this.bounceOnTarget = bounceOnTarget;
		isActive = true;

		isTriggerMissle = false;

		if (GetComponent<Collider> ())
			isTriggerMissle = GetComponent<Collider> ().isTrigger;

		Vector3 targetPos = new Vector3(target.x, target.y + 2f, target.z);
		
		this.transform.LookAt(targetPos);

		GetComponent<Rigidbody>().AddRelativeForce(Vector3.forward * speed, ForceMode.Impulse);

		//Debug.Break ();
	}

	void OnTriggerEnter(Collider other) 
	{
		if (other.gameObject.tag.Equals("Player") && isActive)// && isTriggerMissle)
		{		
			CharacterController otherController = CharacterManager.GetController(other.gameObject);
		
			if(otherController.TeamColor != caster.TeamColor)
			{
				if(otherController != caster && !HitedCharacters.Contains(otherController))
				{	
					isActive = false; 
						
					if(other.gameObject.name.Equals("Head"))
					{
						amount = amount * 2;
					}

					if(!bounceOnTarget)
					{
						this.transform.parent = other.gameObject.transform;
						GetComponent<Rigidbody>().Sleep();
						GetComponent<Rigidbody>().isKinematic = true;	
					}
					else
					{
						Rigidbody rigid = GetComponent<Rigidbody>();

						rigid.velocity = Random.insideUnitSphere.normalized * 4f;

						rigid.velocity += Vector3.up * 3;
					}

					if(otherController != null)
					{
						HitedCharacters.Add(otherController);
						otherController.ApplyEffect(effectType, caster, amount, duration);
					}
				}		
			}
		}

		if(other.gameObject.tag == "Castle")
		{
			CharacterController otherController = CharacterManager.GetController(other.gameObject);

			if(otherController.TeamColor != caster.TeamColor)
			{
				if(!bounceOnTarget)
				{
					this.transform.parent = other.gameObject.transform;
					GetComponent<Rigidbody>().Sleep();
					GetComponent<Rigidbody>().isKinematic = true;	
				}
				else
				{
					Rigidbody rigid = GetComponent<Rigidbody>();					
					rigid.velocity = Random.insideUnitSphere.normalized * 4f;					
					rigid.velocity += Vector3.up * 3;
				}
				
				if(otherController != null)
				{
					HitedCharacters.Add(otherController);
					otherController.ApplyEffect(effectType, caster, amount, duration);
				}
			}
		}

		if(other.gameObject.tag.Equals("Shield"))
		{
			isActive = false; 

			if(!bounceOnTarget)
			{
				this.transform.parent = other.gameObject.transform;
				GetComponent<Rigidbody>().Sleep();
				GetComponent<Rigidbody>().isKinematic = true;	
			}
			else
			{
				Rigidbody rigid = GetComponent<Rigidbody>();				
				rigid.velocity = Random.insideUnitSphere.normalized * 4f;				
				rigid.velocity += Vector3.up * 3;
			}
		}
	}

	void OnCollisionEnter(Collision collision) 
	{
		if (collision.gameObject.tag == "Player" && isActive)
		{		
			CharacterController collisionController = CharacterManager.GetController(collision.gameObject);
			
			if(collisionController.TeamColor != caster.TeamColor)
			{
				if(collisionController != caster && !HitedCharacters.Contains(collisionController))
				{
					//Debug.Break();
					
					TeamColor targetColor = collisionController.TeamColor;
					TeamColor casterColor = caster.TeamColor;
					
					if(targetColor != casterColor)
					{
						isActive = false; 
						
						if(collision.gameObject.name == "Head")
						{
							amount = amount * 2;
						}
						
						if(collisionController != null)
						{
							HitedCharacters.Add(collisionController);
							collisionController.ApplyEffect(effectType, caster, amount, duration);
						}
					}	
				}
			}
		}
		
		if(collision.gameObject.tag == "Castle")
		{
			CharacterController collisionController = CharacterManager.GetController(collision.gameObject);
			
			if(collisionController.TeamColor != caster.TeamColor)
			{
				isActive = false; 

				if(collisionController != null)
				{
					HitedCharacters.Add(collisionController);
					collisionController.ApplyEffect(effectType, caster, amount, duration);
				}
			}
		}

		if(collision.gameObject.tag.Equals("Shield"))
		{
			isActive = false; 
			if(!bounceOnTarget)
			{
				this.transform.parent = collision.gameObject.transform;
				GetComponent<Rigidbody>().Sleep();
				GetComponent<Rigidbody>().isKinematic = true;	
			}
			else
			{
				Rigidbody rigid = GetComponent<Rigidbody>();				
				rigid.velocity = Random.insideUnitSphere.normalized * 4f;				
				rigid.velocity += Vector3.up * 3;
			}
		}
	}
}
