﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ContinuousEffect : MonoBehaviour 
{
	private Action sourceAction;

	private CharacterController caster;
	private CharacterController target;

	private ActionEffectTypes effect;
	private string effectPrefab;
	private int amount;
	private int duration;
	private int cooldown;
	private float range;
	private int currentCooldown;

	private OnHit onEnemyHit;
	private OnHit onCasterHit;

	private IDictionary<int, GameObject> currentShots = new Dictionary<int, GameObject>();
	private GameObject targetEffect;

	private int shotIndex;
	private bool isDismissing;
	
	public int maxExpirationTime = 500;

	public void UseAction (Action sourceAction, OnHit onEnemyHit, OnHit onCasterHit) //, GameObject caster, GameObject target, ActionEffectTypes effectType, int amount, int duration, int cooldown, string effectPrefab)
	{
		shotIndex = 0;

		this.sourceAction = sourceAction;

		this.caster = sourceAction.caster;
		this.target = sourceAction.target;

		this.effect = sourceAction.effectType;
		this.amount = sourceAction.effectAmout;
		this.duration = sourceAction.effectDuration;
		this.cooldown = sourceAction.effectCooldown;
		this.range = sourceAction.range;

		this.effectPrefab = sourceAction.effectPrefab;

		this.onEnemyHit = onEnemyHit;
		this.onCasterHit = onCasterHit;

		targetEffect = ObjectManager.InstantiateObject("Actions/Skills/DrainLifeTargetEffect", sourceAction.target.CharacterPosition);		
		targetEffect.transform.parent = target.CharacterGO.transform;

		currentCooldown = cooldown;
	}

	void FixedUpdate()
	{
		maxExpirationTime--;
		if (maxExpirationTime < 0)
			DestroyObject (this.gameObject);

		//Debug.Log ("Effect CoolDown = " + currentCooldown + "  Remaining : " + sourceAction.effectDuration);

		if (isDismissing)
		{
			if(currentShots.Count == 0)
				DestroyObject (this.gameObject);
		}
		else
		{
			float distance = (sourceAction.caster.transform.position - sourceAction.target.CharacterPosition).magnitude;

			Debug.Log("Distance " + distance + " / " + (range));

			if(distance >= range)
				Dismiss();


			currentCooldown--;
			if (currentCooldown <= 0)
			{
				if (sourceAction.effectDuration > 0)
				{
					currentCooldown = cooldown;
					sourceAction.effectDuration--;
					ShotEffect();
				}
				else
				{
					Dismiss();
				}
			}
		}
	}

	private void ShotEffect()
	{
		shotIndex++;
		onEnemyHit (target, null);

		string singleShotPrefab = "Actions/TargetTypes/SingleShot";
		currentShots.Add(shotIndex, ObjectManager.InstantiateObject(singleShotPrefab, target.transform.position));		
		SingleShot singleShot = currentShots[shotIndex].GetComponentInChildren<SingleShot>();				
		singleShot.ShotFollowCaster(sourceAction, effectPrefab, onCasterHit, null);
		singleShot.maxExpirationTime = 100;
	}

	public void DismissShot(int dismissIndex)
	{
		if (currentShots.ContainsKey(dismissIndex))
		{
			DestroyObject (currentShots[dismissIndex]);
			currentShots.Remove(dismissIndex);
		}
	}

	public void Dismiss()
	{
		Debug.Log("Dismissin Continuous Effect ... ");

		if(caster.IsMainPlayer){GameManager.ClearTargets ();}

		if (targetEffect)
			DestroyObject (targetEffect);

		isDismissing = true;

		if (sourceAction)
			DestroyObject(sourceAction.gameObject);
	}
}
