using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SingleShot : ActionTarget
{		
	public int sourceActionID;
	public GameObject actionPrefabGO;
	private Vector3 target = Vector3.zero;
	private float targetRange;

	private bool followCaster;
	private bool followTarget;

	public int maxExpirationTime = 500;

	void FixedUpdate()
	{
		maxExpirationTime--;
		if (maxExpirationTime < 0)
			DestroyObject (this.gameObject);

		if(isActive)
		{
			if (followCaster)
			{
				Vector3 direction = (sourceAction.caster.transform.position - this.transform.position).normalized;

				GetComponent<Rigidbody>().velocity = (direction * 20f);
			}

			float currentDistance = (transform.position - origPosition).magnitude;

			if(currentDistance >= sourceAction.range)
				ReachRange();

			if(currentDistance >= targetRange)
				ReachTarget();

			if(actionPrefabGO != null)
			{
				actionPrefabGO.transform.position = transform.position;
			}
		}
	}

	public void ShotFollowCaster(Action action, string prefab, OnHit onHit, OnHitObject onHitObject)
	{
		followCaster = true;

		Shot (action, prefab, onHit, onHitObject, null, null);
	}

	public void ShotFollowTarget(Action action, string prefab, OnHit onHit, OnHitObject onHitObject)
	{
		followTarget = true;
		
		Shot (action, prefab, onHit, onHitObject, null, null);
	}

	public GameObject Shot(Action action, string prefab, OnHit onHit, OnHitObject onHitObject, OnReachTarget onReachTarget, OnReachRange onReachRange)
	{		
		isActive = true;
		isCollinding = true;
		sourceAction = action;

		origPosition = transform.position;
		transform.localScale *= sourceAction.width;

		target = sourceAction.caster.GetTarget ();
		target = new Vector3 (target.x, transform.position.y, target.z);
		targetRange = (origPosition - target).magnitude;

		transform.LookAt(new Vector3(target.x, transform.position.y, target.z));

		this.onHit = onHit;
		this.onHitObject = onHitObject;
		this.onReachTarget = onReachTarget;
		this.onReachRange = onReachRange;

		if (prefab != "")
		{
			Vector3 prefabPos = new Vector3 (transform.position.x, 1, transform.position.z);

			actionPrefabGO = ObjectManager.InstantiateObject(prefab, prefabPos);

			//sourceAction.currentDirection = sourceAction.casterController.direction;
			//sourceAction.currentDirection = new Vector3 (sourceAction.currentDirection.x, actionPrefabGO.transform.position.y, sourceAction.currentDirection.z);

			actionPrefabGO.transform.LookAt (sourceAction.currentDirection);
			actionPrefabGO.transform.parent = transform;

			sourceAction.actionGO = actionPrefabGO;
		}

		GetComponent<Rigidbody>().AddRelativeForce(Vector3.forward * sourceAction.speed, ForceMode.Impulse);		
	
		return actionPrefabGO;
	}

	public GameObject ShotWithTarget(Vector3 target, Action action, string prefab, OnHit onHit, OnHitObject onHitObject, OnReachTarget onReachTarget, OnReachRange onReachRange)
	{
		isActive = true;
		isCollinding = true;
		sourceAction = action;
		
		origPosition = transform.position;
		transform.localScale *= sourceAction.width;
		
		this.target = target;
		target = new Vector3 (target.x, transform.position.y, target.z);
		targetRange = (origPosition - target).magnitude;
		
		transform.LookAt(new Vector3(target.x, transform.position.y, target.z));
		
		this.onHit = onHit;
		this.onHitObject = onHitObject;
		this.onReachTarget = onReachTarget;
		this.onReachRange = onReachRange;
		
		if (prefab != "")
		{
			Vector3 prefabPos = new Vector3 (transform.position.x, 1, transform.position.z);
			
			actionPrefabGO = ObjectManager.InstantiateObject(prefab, prefabPos);
			
			//sourceAction.currentDirection = sourceAction.casterController.direction;
			//sourceAction.currentDirection = new Vector3 (sourceAction.currentDirection.x, actionPrefabGO.transform.position.y, sourceAction.currentDirection.z);
			
			actionPrefabGO.transform.LookAt (sourceAction.currentDirection);
			actionPrefabGO.transform.parent = transform;
			
			sourceAction.actionGO = actionPrefabGO;
		}
		
		GetComponent<Rigidbody>().AddRelativeForce(Vector3.forward * sourceAction.speed, ForceMode.Impulse);		
		
		return actionPrefabGO;
	}

	public void ReachTarget()
	{				
		if (onReachTarget != null)
			onReachTarget (transform.position, this.gameObject);
	}

	public void ReachRange()
	{				
		if (onReachRange != null)
			onReachRange (transform.position, this.gameObject);
	}

	void OnTriggerEnter(Collider other)
	{
		if(other.gameObject.tag.Equals("Player"))
		{
			if (onHit != null)
			{
				CharacterController controller = CharacterManager.GetController(other.gameObject);

				if (isActive && isCollinding)
				{
					onHit (controller, this.gameObject);
				}
			}
		}
		else
		{
			if (onHitObject != null)
			{
				if (isActive && other.gameObject != sourceAction.caster && isCollinding)
				{
					onHitObject (other.gameObject, this.gameObject);
				}
			}
		}
	}
}
