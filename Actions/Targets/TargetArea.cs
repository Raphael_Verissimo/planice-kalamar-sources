﻿using UnityEngine;
using System.Collections;

public class TargetArea : ActionTarget
{	
	private GameObject actionPrefabGO;
	private bool isStaying;

	void FixedUpdate()
	{
		if (transform.position.y > 0)
		{
			transform.position = new Vector3(transform.position.x, 0, transform.position.z);

			if(isStaying)
			{
				GetComponent<Rigidbody>().velocity = Vector3.zero;
			}
			else
			{
				DestroyObject(this.gameObject);
			}
		}
	}

	public void ShotAndStay(Action action, string prefab, OnHit onHit)
	{
		isStaying = true;
		Shot (action, prefab, onHit);
	}

	public void Shot(Action action, string prefab, OnHit onHit)
	{		
		isActive = true;
		sourceAction = action;
		
		transform.localScale *= sourceAction.area;

		GetComponent<Rigidbody>().velocity = Vector3.up * 100;		
		
		this.onHit = onHit;
	}

	private void RenderPrefab(string prefab)
	{
		if (prefab != "")
		{
			// Namoral i`m a fucking genius, nao vou dizer oque isso faz, te desafio a entender, ou me pergunte !!!!
			Vector3 floorPosition = new Vector3(transform.position.x, 0, transform.position.z);
			Vector3 effectOffSet = (GameManager.MainCamera.transform.position - floorPosition).normalized;
			Vector3 effectPosition = floorPosition + (effectOffSet * 10);
			
			ObjectManager.InstantiateObject (prefab, effectPosition);
		}
	}

	void OnTriggerEnter(Collider other) 
	{
		if (onHit != null && isActive && (other.gameObject.tag == "Player" || other.gameObject.tag == "Castle"))
		{ 
			CharacterController otherController = CharacterManager.GetController(other.gameObject);

			onHit (otherController, this.gameObject);
		}
	}

	void OnCollisionEnter(Collision collision) 
	{
		if(collision.gameObject.tag == "Castle")
		{
			CharacterController collisionController = CharacterManager.GetController(collision.gameObject);
			
			onHit (collisionController, this.gameObject);
		}
	}
}
