﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic; 

public delegate void OnHit(CharacterController target, GameObject shotGO);
public delegate void OnHitObject(GameObject targetGO, GameObject shotGO);
public delegate void OnReachTarget(Vector3 target, GameObject shotGO);
public delegate void OnReachRange(Vector3 target, GameObject shotGO);

public class ActionTarget : MonoBehaviour 
{
	internal bool isActive;
	internal bool isCollinding;

	internal OnHit onHit;
	internal OnHitObject onHitObject;
	internal OnReachTarget onReachTarget;
	internal OnReachRange onReachRange;

	internal Action sourceAction;
	internal Vector3 origPosition;
	internal GameObject prefab;
}

public enum TargetType
{
	TARGET_SELF,
	SINGLE_SHOT,
	TARGET_AREA
}