﻿using UnityEngine;
using System.Collections;

public class Action : MonoBehaviour 
{	
	#region Atributos Internos
	internal int actionID;
	private static int nextActionID = 0;	

	internal CharacterController caster;
	internal CharacterController target;
	internal Vector3 currentPosition;
	internal Vector3 lastSelectedTarget;
	internal Vector3 currentDirection;

	internal float range;
	internal float area;
	internal float speed;
	internal float width;

	internal bool isActive;
	internal bool isCasted;
	internal bool isCasting;
	internal bool lockCharacter;	
	internal bool isWaitingForTarget;
	internal bool isMainPlayerAction;
	
	internal int castTime;	
	internal int coolDown;
	internal int currentCoolDown;
	internal int currentCastTime;

	internal ActionEffectTypes effectType;
	internal int effectAmout;
	internal int effectDuration;
	internal int effectCooldown;

	internal float animationTime;
	internal bool animationStoped;

	internal string effectPrefab;
	internal string actionPrefab;
	internal string afterCastPrefab;
	internal string beforeCastPrefab;
	
	internal GameObject beforeCastGO;	
	internal GameObject afterCastGO;
	internal GameObject actionGO;
	internal GameObject effectGO;
	#endregion

	public static int GetNextActionID
	{
		get
		{
			return nextActionID++;
		}
	}

	internal void CastAction(CharacterController caster, bool lockCharacter)
	{		
		this.caster = caster;
		this.currentPosition = caster.CharacterPosition;
		
		if (currentCoolDown >= coolDown)
		{
			this.isCasted = false;
			this.isCasting = true;
			this.isWaitingForTarget = false;
			
			this.lockCharacter = lockCharacter;
			
			if (this.lockCharacter)
				this.caster.LockCharacter ();
			
			if (beforeCastPrefab != "" && beforeCastPrefab != null) {
				Vector3 castPosition = new Vector3 (this.currentPosition.x, 1, this.currentPosition.z);
				
				beforeCastGO = ObjectManager.InstantiateObject (beforeCastPrefab, castPosition);
				beforeCastGO.transform.LookAt (InputController.GetMousePositionOnFloor ());
				beforeCastGO.transform.parent = this.gameObject.transform;
			}
		}
		else
		{
			DestroyObject(this.gameObject);
		}
	}

	internal void ActionWaitForTarget()
	{
		StopCasting ();
		
		if (beforeCastGO != null)
			StopAnimation(beforeCastGO);
		
		if (afterCastPrefab != "" && afterCastGO == null)
		{
			Vector3 castPosition = new Vector3 (this.currentPosition.x, 2, this.currentPosition.z);
			
			afterCastGO = ObjectManager.InstantiateObject(afterCastPrefab, castPosition);				
			afterCastGO.transform.LookAt (lastSelectedTarget);		
			afterCastGO.transform.parent = this.gameObject.transform;
		}
		
		animationStoped = false;
	}

	internal void ShotActionTargetArea (TargetType type, string prefab, OnHit onHit, OnHitObject onHitObject)
	{	
		ShotActionTarget (type, prefab, onHit, onHitObject, null, null);
	}
	
	internal GameObject ShotActionTarget (TargetType type, string prefab, OnHit onHit, OnHitObject onHitObject, OnReachTarget onReachTarget, OnReachRange onReachRange)
	{	
		if (afterCastGO != null)
			StopAnimation(afterCastGO);
		
		isWaitingForTarget = false;
		
		actionGO = ShotTargetGO (type, prefab, onHit, onHitObject, onReachTarget, onReachRange);
		
		isCasted = false;
		isCasting = false;
		
		if (lockCharacter)
		{
			lockCharacter = true;
			caster.UnlockCharacter();
		}
		animationStoped = false;
		
		return actionGO;
	}

	private GameObject ShotTargetGO(TargetType type, string prefab, OnHit onHit, OnHitObject onHitObject, OnReachTarget onReachTarget, OnReachRange onReachRange)
	{
		GameObject returnedPrefab =  null;
		
		switch(type)
		{
		case TargetType.SINGLE_SHOT:			
			string singleShotPrefab = "Actions/TargetTypes/SingleShot";
			returnedPrefab = ObjectManager.InstantiateObject(singleShotPrefab, caster.transform.position);		
			SingleShot singleShot = returnedPrefab.GetComponentInChildren<SingleShot>();	
			GameObject returnedGO = singleShot.Shot(this, prefab, onHit, onHitObject, onReachTarget, onReachRange);
			if(returnedGO != null)
				returnedPrefab = returnedGO;
			break;
			
		case TargetType.TARGET_AREA:			
			string targetAreaPrefab = "Actions/TargetTypes/TargetArea";
			Vector3 targetPos = new Vector3(lastSelectedTarget.x, -(area+5f), lastSelectedTarget.z);
			returnedPrefab = ObjectManager.InstantiateObject(targetAreaPrefab, targetPos);		
			TargetArea targetArea = returnedPrefab.GetComponentInChildren<TargetArea>();				
			targetArea.Shot(this, prefab, onHit);
			break;
		}
		
		return returnedPrefab;
	}
	
	internal void StopCasting() 
	{		
		this.isCasted = true;
		this.isCasting = false;
		this.isWaitingForTarget = true;
		
		//if (beforeCastGO != null)
		//	DestroyObject(beforeCastGO);
	}

	internal void StopAnimation(GameObject prefab) 
	{
		ParticleEmitter[] particles = prefab.GetComponentsInChildren<ParticleEmitter> ();
		
		if(!animationStoped && particles != null)
		{
			Debug.Log("ANIMATION STOPED!!!");
			animationStoped = true;
			foreach (ParticleEmitter particle in particles)
			{
				particle.emit = false;
			}
		}
	}

	internal void StopColliding(GameObject prefab) 
	{
		prefab.GetComponent<SingleShot>().isCollinding = false;
	}

	internal void ChangeAcceleration(GameObject prefab, float factor) 
	{
		prefab.GetComponentInChildren<Rigidbody>().velocity *= factor;
	}

	public virtual void Dismiss()
	{	
		//Debug.Log ("Action Dismiss");
		
		DestroyObject (this.gameObject);
	}
}