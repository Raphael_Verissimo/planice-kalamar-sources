﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BraverAction : Action
{
	private bool isLeaping;
	private bool isJumping;
	
	private Vector3 middlePoint;
	private Vector3 leapDirection;
	private Vector3 downDirection;

	private float distance;
	private Vector3 direction;
	private float gravity;

	private IList<CharacterController> HitedCharacters = new List<CharacterController> ();

	public void UseAction(CharacterController caster)
	{
		isLeaping = false;

		base.currentPosition = caster.CharacterPosition;		

		effectType = ActionEffectTypes.PHYSICAL_DAMAGE;
		effectAmout = 100;
		effectDuration = 1;
		
		currentCastTime = 0;
		animationTime = 0;
		castTime = 10;  
		range = 20f;
		speed = 1f;
		width = 2f;
		area = 10f;
		coolDown = 400;
		currentCoolDown = caster.CharacterModel.SecondActionCoolDown;
		isMainPlayerAction = caster.IsMainPlayer;

		beforeCastPrefab = "";
		afterCastPrefab = "";
		actionPrefab = "";
		
		CastAction (caster, false);
	}

	void FixedUpdate()
	{		
		if (caster.GetTarget() != Vector3.zero)
		{
			if(lastSelectedTarget != caster.GetTarget())
			{
				caster.CharacterModel.SecondActionCoolDown = 0;

				lastSelectedTarget = caster.GetTarget();
								
				Leap(caster.GetTarget());

				Vector3 casterPosition = this.caster.transform.position;
				Vector3 targetDirection = (lastSelectedTarget - this.caster.transform.position).normalized;

				casterPosition += Vector3.up * 2.5f;
				casterPosition += targetDirection * 1.3f;

				GameObject braverGO = ObjectManager.InstantiateObject("Actions/Skills/BraverSword", casterPosition);
				braverGO.transform.LookAt(lastSelectedTarget);
				braverGO.transform.parent = this.transform;

				if(isMainPlayerAction)
					GameManager.DrawSelectedAreaTargetInRange(caster.transform.position, range);
			}
		}
		else
		{
			if(isMainPlayerAction)
				GameManager.DrawAreaTargetInRange(caster.transform.position, area, range);

			if(isMainPlayerAction)
				GameManager.DrawRangedAreaTarget(caster.transform.position, range);
		}

		if (isLeaping)
		{
			caster.transform.Translate (direction);
			direction -= (Vector3.up * gravity);

			if (caster.transform.position.y < 0)
			{
				Explode();
			}
		}
	}

	private void Explode()
	{
		ShotActionTargetArea (TargetType.TARGET_AREA, "", OnBraverHit, OnBraverHitObject);
		
		// Namoral i`m a fucking genius, nao vou dizer oque isso faz, te desafio a entender, ou me pergunte !!!!
		Vector3 effectOffSet = (GameManager.MainCamera.transform.position - lastSelectedTarget).normalized;
		Vector3 effectPosition = lastSelectedTarget + (effectOffSet * 3);
		
		ObjectManager.InstantiateObject ("Especial Effects/Explosion/Detonator-Wide", effectPosition);
		
		isLeaping = false;
		caster.transform.position = new Vector3(caster.transform.position.x, 1, caster.transform.position.z);
		caster.UnlockCharacter ();
		
		if(isMainPlayerAction)
			if(caster.IsMainPlayer){GameManager.ClearTargets();}
	}

	internal void OnBraverHit(CharacterController target, GameObject shotGO)
	{
		if(target != caster && !HitedCharacters.Contains(target))
		{
			if(target.TeamColor != caster.TeamColor)
			{
				HitedCharacters.Add(target);
				target.ApplyEffect(effectType, caster, effectAmout, 1);
			}
		}
	}

	internal void OnBraverHitObject(GameObject targetGO, GameObject shotGO)
	{
		if(targetGO.tag == "Castle")
		{
			CharacterController castle = CharacterManager.GetController(targetGO);

			if(castle.TeamColor != caster.TeamColor)
			{
				HitedCharacters.Add(castle);
				castle.ApplyEffect(effectType, caster, effectAmout, effectDuration);
			}
		}
	}

	private void Leap(Vector3 target)
	{
		caster.LockCharacter ();

		gravity = 0.03f;

		isLeaping = true;

		distance = (target - caster.transform.position).magnitude;
		direction = (target - caster.transform.position).normalized;

		if (distance > range)
			distance = range;

		float speedX = (direction.x * distance) * 0.021f;
		float speedY = 0.7f;
		float speedZ = (direction.z * distance) * 0.021f;

		direction = new Vector3 (speedX, speedY, speedZ);

		//Debug.Log ("Distance " + distance + " Speed " + direction);
	}
}
