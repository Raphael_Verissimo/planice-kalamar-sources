﻿using UnityEngine;
using System.Collections;

public class DrainLifeAction : Action
{
	private bool reached;
	private bool isDismissing;
	private int dismissTime; 

	private bool effectCasted;

	private ContinuousEffect drainEffect;
	private int drainsCount;

	public void UseAction (CharacterController caster)
	{
		drainsCount = 0;

		effectType = ActionEffectTypes.PHYSICAL_DAMAGE;
		effectAmout = 50;
		effectDuration = 5;
		effectCooldown = 50;

		currentCastTime = 0;
		animationTime = 0;
		castTime = 0;  
		range = 20f;
		speed = 15f;
		width = 2f;
		coolDown = 800;
		currentCoolDown = caster.CharacterModel.SecondActionCoolDown;
		isMainPlayerAction = caster.IsMainPlayer;

		reached = false;
		isDismissing = false;
		dismissTime = 50; 
		
		beforeCastPrefab = "";
		afterCastPrefab = "Actions/Skills/DrainLifeHand";
		actionPrefab = "";
		effectPrefab = "Actions/Skills/DrainLifeEffect";

		CastAction (caster, false);
	}

	internal int CalculateDamage()
	{
		int magicDamage = 0;
		
		magicDamage = caster.MagicDamage / 3;
		
		return magicDamage;
	}

	internal void OnDamage(CharacterController target, GameObject shotGO)
	{
		target.ApplyEffect(effectType, caster, CalculateDamage(), 1);
	}
	
	internal void OnDrain(CharacterController hited, GameObject shotGO)
	{
		if (hited == caster) 
		{
			drainsCount++;
			drainEffect.DismissShot (drainsCount);

			caster.ApplyEffect(ActionEffectTypes.HEALING_LIFE, caster, CalculateDamage(), 1);
		}
	}

	void FixedUpdate()
	{
		if(isCasting)
			ActionWaitForTarget ();
		
		if (caster.GetTargetController() != null)
		{			
			CharacterController selectedTarget = caster.GetTargetController();

			if(target != selectedTarget)
			{
				target = selectedTarget;
			}

			float distance = (target.CharacterPosition - caster.CharacterPosition).magnitude;

			if (isCasted && !effectCasted && distance <= range)
			{
				string drainEffectPrefab = "Actions/TargetTypes/ContinuousEffect";
				GameObject drainEffectGO = ObjectManager.InstantiateObject(drainEffectPrefab, target.CharacterPosition);
				drainEffect = drainEffectGO.GetComponent<ContinuousEffect>();
				caster.CharacterModel.SecondActionCoolDown = 0;
				drainEffect.UseAction(this, OnDamage, OnDrain);

				if(caster.IsMainPlayer){GameManager.ClearTargets();}

				effectCasted = true;
			}
		}
		else
		{
			if((isCasting || isWaitingForTarget) && isMainPlayerAction)
			{
				GameManager.DrawAttackPlayer();
			}
		}

		if(isMainPlayerAction)
			GameManager.DrawRangedAreaTarget(caster.CharacterPosition, range);

		if(isDismissing)
		{
			dismissTime--;

			if(dismissTime <= 0)
			{
				DestroyObject(actionGO);
			}
		}
	}

	public override void Dismiss()
	{
		if(drainEffect)
			drainEffect.Dismiss ();

		if(caster.IsMainPlayer){GameManager.ClearTargets();}

		DestroyObject (this.gameObject);
	}
}
