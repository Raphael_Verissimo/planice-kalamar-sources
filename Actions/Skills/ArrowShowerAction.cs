﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ArrowShowerAction : Action
{	
	private bool reached;
	private bool isDismissing;
	private int dismissTime; 

	private GameObject mainShotGO;
	private GameObject nearRightShotGO;
	private GameObject nearLeftShotGO;
	private GameObject farRightShotGO;
	private GameObject farLeftShotGO;	
	
	private Vector3 mainDirection;
	private Vector3 nearRightDirection;
	private Vector3 nearLeftDirection;
	private Vector3 farRightDirection;
	private Vector3 farLeftDirection;
	
	private IList<CharacterController> HitedCharacters = new List<CharacterController> ();

	public void UseAction (CharacterController caster)
	{		
		effectType = ActionEffectTypes.PHYSICAL_DAMAGE;
		
		effectAmout = 100;
		effectDuration = 1;
		
		currentCastTime = 0;
		animationTime = 0;
		castTime = 0;  
		range = 70f;
		speed = 70f;
		width = 3f;
		coolDown = 1000;
		currentCoolDown = caster.CharacterModel.SecondActionCoolDown;
		isMainPlayerAction = caster.IsMainPlayer;
		
		reached = false;
		isDismissing = false;
		dismissTime = 50; 
		
		beforeCastPrefab = "";
		afterCastPrefab = "Actions/Skills/ArrowShowerCast";
		actionPrefab = "Actions/Skills/ArrowShowerMissle";

		CastAction (caster, false);
	}

	internal void OnReach(Vector3 target, GameObject shotGO)
	{
		DestroyObject (shotGO);
	}
	
	internal void OnHit(CharacterController target, GameObject shotGO)
	{
		if (target != caster && !HitedCharacters.Contains(target))
		{
			if(target.TeamColor != caster.TeamColor)
			{
				HitedCharacters.Add(target);		
				DestroyObject (shotGO);
				target.ApplyEffect(effectType, caster, effectAmout, effectDuration);
			}
		}
	}

	internal void OnHitObject(GameObject targetGO, GameObject shotGO)
	{		
		if(targetGO.tag == "Castle")
		{
			CharacterController castle = CharacterManager.GetController(targetGO);

			if(castle.TeamColor != caster.TeamColor)
			{
				HitedCharacters.Add(castle);			
				DestroyObject (shotGO);
				castle.ApplyEffect(effectType, caster, effectAmout, effectDuration);
			}
		}		

		if (targetGO.tag == "Shield")
		{
			DestroyObject (shotGO);
		}
	}

	void FixedUpdate()
	{
		if(isCasting)
		{
			currentCastTime++;			
			if(currentCastTime >= castTime)
			{
				ActionWaitForTarget ();
			}
		}
		
		if (caster.GetTarget() != Vector3.zero)
		{			
			if(lastSelectedTarget != caster.GetTarget())
			{
				lastSelectedTarget = caster.GetTarget();
			}

			if (isCasted && !isDismissing)
			{
				caster.CharacterModel.SecondActionCoolDown = 0;

				DestroyObject(afterCastGO);

				FindDirection(lastSelectedTarget);

				ShotMissle(mainDirection);
				ShotMissle(nearRightDirection);
				ShotMissle(nearLeftDirection);
				ShotMissle(farRightDirection);
				ShotMissle(farLeftDirection);

				if(caster.IsMainPlayer){GameManager.ClearTargets();}

				//DestroyObject(this.gameObject);
				isDismissing = true;
			}
		}
		else
		{
			if((isCasting || isWaitingForTarget) && isMainPlayerAction)
			{
				Vector3 lookAtDirection = InputController.GetMousePositionOnFloor();
				afterCastGO.transform.LookAt(new Vector3(lookAtDirection.x, 2f, lookAtDirection.z));

				FindDirection(lookAtDirection);

				GameManager.DrawMultipleSkillShotTarget(1, caster.CharacterPosition, range/10, mainDirection);
				GameManager.DrawMultipleSkillShotTarget(2, caster.CharacterPosition, range/10, nearRightDirection);
				GameManager.DrawMultipleSkillShotTarget(3, caster.CharacterPosition, range/10, nearLeftDirection);
				GameManager.DrawMultipleSkillShotTarget(4, caster.CharacterPosition, range/10, farRightDirection);
				GameManager.DrawMultipleSkillShotTarget(5, caster.CharacterPosition, range/10, farLeftDirection);
			}
		}
		
		if(isDismissing)
		{
			dismissTime--;
			
			if(dismissTime <= 0)
			{
				DestroyObject(actionGO);
			}
		}
	}

	private void FindDirection(Vector3 currentTargetPosition)
	{
		Vector3 normalizedDirection = (currentTargetPosition - caster.CharacterPosition).normalized;
		Vector3 targetNormalizedPosition = normalizedDirection * range;
		Vector3 targetPosition = targetNormalizedPosition + caster.CharacterPosition; 
		Vector3 upTargetPosition = new Vector3 (targetPosition.x, 10, targetPosition.z); 
	
		Vector3 leftNormal = new Vector3 (normalizedDirection.z, 0, -normalizedDirection.x);
		Vector3 rightNormal = new Vector3 (-normalizedDirection.z, 0, normalizedDirection.x);

		mainDirection = targetPosition;

		nearLeftDirection = targetPosition;
		nearRightDirection = targetPosition;
		farLeftDirection = targetPosition;
		farRightDirection = targetPosition;

		nearLeftDirection += leftNormal*10;
		nearRightDirection += rightNormal*10;
		farLeftDirection += leftNormal*20;
		farRightDirection += rightNormal*20;
	}

	private GameObject ShotMissle(Vector3 targetPos)
	{
		GameObject prefabGO = ObjectManager.InstantiateObject("Actions/TargetTypes/SingleShot", caster.transform.position);		
		prefabGO.transform.position += Vector3.up;
		SingleShot singleShot = prefabGO.GetComponentInChildren<SingleShot>();				
		return singleShot.ShotWithTarget(targetPos, this, actionPrefab, OnHit, OnHitObject, null, OnReach);
	}
}
