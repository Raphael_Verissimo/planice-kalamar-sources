﻿using UnityEngine;
using System.Collections;

public class BulletTimeAction : Action
{
	private GameObject casterBody;
	private Vector3 jumpTarget;
	private Vector3 jumpDirection;

	private bool isSlow;

	private int jumpEndTime;
	private int jumpEndCurrentTime;
	
	private Vector3 rotate;

	private int maxShots;
	private int currentShots;

	private int maxCoolDown;
	//private int currentCoolDown;

	public void UseAction (CharacterController caster)
	{
		effectType = ActionEffectTypes.PHYSICAL_DAMAGE;
		this.caster = caster;
		isSlow = false;
		
		effectAmout = 0;
		effectDuration = 1;
		
		currentCastTime = 0;
		animationTime = 0;
		castTime = 10;  
		range = 3f;
		speed = 10f;
		width = 0f;
		coolDown = 1200;
		currentCoolDown = caster.CharacterModel.ThirdActionCoolDown;
		isMainPlayerAction = caster.IsMainPlayer;

		beforeCastPrefab = "";
		afterCastPrefab = "";
		actionPrefab = "";

		jumpEndTime = 20;
		maxShots = 3;
		currentShots = 0;
		maxCoolDown = 10;
		
		CastAction (caster, true);		
		StartJump ();
	}

	void FixedUpdate()
	{
		currentCoolDown++;

		if(isCasting)
		{
			UpdateRotation();
			currentCastTime++;			
			if(currentCastTime >= castTime)
			{
				ActionWaitForTarget ();
			}
			//Debug.Log("Casting Bullet Time " + currentCastTime + " / " + animationTime + " / " + castTime);
		}
		else
		{
			Jump();

			caster.CharacterModel.ThirdActionCoolDown = 0;

			if (caster.GetTarget() != Vector3.zero)
			{		
				if(currentCoolDown > maxCoolDown && currentShots < maxShots)
				{ 
					currentCoolDown = 0; 
					currentShots++;

					GameObject arrowGO = ObjectManager.InstantiateObject(caster.WeaponPrefab, new Vector3(caster.CharacterPosition.x, caster.CharacterPosition.y + 1f, caster.CharacterPosition.z));
					MissleFollow missle = arrowGO.GetComponent<MissleFollow>();

					missle.Shot(caster, caster.GetTarget(), 100f, 200, ActionEffectTypes.PHYSICAL_DAMAGE, 150, 0, false, false);

					if(isMainPlayerAction)
						GameManager.DrawAttackTarget(caster.GetTarget());

					//Time.timeScale = 0.02f;
					//Debug.Break();
					caster.ReleaseTarget();
				}
			}

			//if(currentCoolDown > maxCoolDown && currentShots < maxShots)
			if(isMainPlayerAction)
				GameManager.DrawAttackPlayer();
		}
	}

	private void StartJump()
	{
		jumpTarget = InputController.GetMousePositionOnFloor ();
		jumpDirection = (jumpTarget - caster.transform.position).normalized;
		jumpDirection += Vector3.up * 0.65f;
		//jumpDirection = new Vector3(0, 0.8f, 0.2f);

		casterBody = caster.transform.FindChild("Body").gameObject; 
		casterBody.transform.LookAt(jumpTarget);
	}

	private void UpdateRotation()
	{
		Vector3 rotate = new Vector3 (6f, 0, 0);
		casterBody.transform.Rotate (rotate);

		Debug.Log ("time " + Time.timeScale);
		if(!isSlow)
		{
			Time.timeScale -= 0.1f;
			
			if(Time.timeScale <= 0.2f)
				isSlow = true;
		}
	}

	private void Jump()
	{ 
		if(jumpDirection.y <= 0 && caster.transform.position.y <= 0)
		{
			if(isSlow)
			{
				Time.timeScale += 0.05f;
				
				if(Time.timeScale >= 1)
					isSlow = false;
			}
			//caster.GetComponent<Rigidbody>().velocity = Vector3.zero;

			jumpDirection = new Vector3 (jumpDirection.x * 0.95f, 0, jumpDirection.z* 0.95f);
			caster.transform.Translate (jumpDirection * speed/40);

			//Debug.Log("Arrasto x " + jumpDirection.x + " Z " + jumpDirection.z);

			if(jumpDirection.x <= 0.1f && jumpDirection.x  >= -0.1f && jumpDirection.z <= 0.1f && jumpDirection.z  >= -0.1f)
			{
				StandUp();
			}
		}
		else
		{
			Vector3 rotate = new Vector3 (0.7f, 0, 0);
			casterBody.transform.Rotate (rotate);
			
			jumpDirection += new Vector3 (0, -0.03f, 0);
			caster.transform.Translate (jumpDirection * speed/20);
		}
	}

	private void StandUp()
	{
		Debug.Log ("Levantando  " + casterBody.transform.rotation.x + " : " + casterBody.transform.rotation.z); 
 
		Vector3 rotate = new Vector3 (-5f, 0, 0);
		casterBody.transform.Rotate (rotate);

		if(casterBody.transform.rotation.x <= 0.02 && casterBody.transform.rotation.x >= -0.02 && 
		   casterBody.transform.rotation.z <= 0.02 && casterBody.transform.rotation.z >= -0.02)
		{
			casterBody.transform.rotation = new Quaternion(0, casterBody.transform.rotation.y, 0, casterBody.transform.rotation.w);

			caster.UnlockCharacter();
			caster.ClearActions();
		}

		casterBody.transform.position = new Vector3 (casterBody.transform.position.x, 0.5f, casterBody.transform.position.z);
	}
}
