﻿using UnityEngine;
using System.Collections;

public class ThrowAxeSpin : MonoBehaviour
{
	public bool isActive;
	public CharacterController follow; 
	public CharacterController caster; 
	public Vector3 directionOffSet;
	// Use this for initialization
	void Start () 
	{
		isActive = true;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(isActive)
		{
			transform.Rotate(new Vector3(20f, 0, 0));
		}
		else
		{
			transform.Rotate(new Vector3(0, 0, 0));
		}

		//if (follow != null && isActive)
		//	transform.position = new Vector3(transform.position.x + directionOffSet.x, transform.position.y, transform.position.z + directionOffSet.z);
	}

	void OnTriggerEnter(Collider other)
	{
		if(other.tag != "Untagged" && other.tag != "Attack-target")
		{
			if (other.tag == "Player")
			{
				CharacterController controller = CharacterManager.GetController(other.gameObject); 

				if(controller != caster && controller.TeamColor != caster.TeamColor)
				{				
					isActive = false;
				}
			}
			else
			{
				if (other.tag == "Shield")
				{
					isActive = false;
				}
			}
		}
	}
}
