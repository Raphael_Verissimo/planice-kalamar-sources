﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PowerShotAction : Action
{	
	private bool reached;
	private bool isDismissing;
	private int dismissTime; 

	private IList<CharacterController> HitedCharacters = new List<CharacterController> ();

	public void UseAction (CharacterController caster)
	{		
		effectType = ActionEffectTypes.PHYSICAL_DAMAGE;
		
		effectAmout = 150;
		effectDuration = 10000;
		
		currentCastTime = 0;
		animationTime = 0;
		castTime = 0;  
		range = 70f;
		speed = 70f;
		width = 2f;
		coolDown = 400;
		currentCoolDown = caster.CharacterModel.FirstActionCoolDown;
		isMainPlayerAction = caster.IsMainPlayer;
		
		reached = false;
		isDismissing = false;
		dismissTime = 50; 
		
		beforeCastPrefab = "";
		afterCastPrefab = "Actions/Skills/PowerShotCast";
		actionPrefab = "Actions/Skills/PowerShotMissle";

		CastAction (caster, false);
	}

	internal void OnReach(Vector3 target, GameObject shotGO)
	{
		if(shotGO)
			DestroyObject (shotGO);

		//if(this.gameObject)
		//	DestroyObject (this.gameObject);
	}
	
	internal void OnHit(CharacterController target, GameObject shotGO)
	{
		if (target != caster)
		{
			if(isMainPlayerAction && !HitedCharacters.Contains(target))
			{
				if(target.TeamColor != caster.TeamColor)
				{
					HitedCharacters.Add(target);

					target.ApplyEffect(effectType, caster, effectAmout, effectDuration);
				}
			}
		}
	}

	internal void OnHitObject(GameObject targetGO, GameObject shotGO)
	{
		if(shotGO)
		{
			if (targetGO.tag.Equals("Shield")) 
			{
				shotGO.transform.parent = targetGO.gameObject.transform;
				shotGO.GetComponent<Rigidbody>().Sleep();
				shotGO.GetComponent<Rigidbody>().isKinematic = true;	
			}

			if (targetGO.tag.Equals("Castle"))
			{
				CharacterController castle = CharacterManager.GetController(targetGO);
				
				if(caster.TeamColor != castle.TeamColor)
				{					
					HitedCharacters.Add(castle);

					shotGO.transform.parent = targetGO.gameObject.transform;
					shotGO.GetComponent<Rigidbody>().Sleep();
					shotGO.GetComponent<Rigidbody>().isKinematic = true;			
					castle.ApplyEffect(effectType, caster, effectAmout, effectDuration);
				}
			}
		}
	}

	void FixedUpdate()
	{
		effectDuration--;
		if (effectDuration <= 0)
		{
			//DestroyObject (shotGO);
			DestroyObject (this.gameObject);
			return;
		}

		if(isCasting)
		{
			currentCastTime++;			
			if(currentCastTime >= castTime)
			{
				ActionWaitForTarget ();
			}
		}
		
		if (caster.GetTarget() != Vector3.zero)
		{			
			if(lastSelectedTarget != caster.GetTarget())
			{
				lastSelectedTarget = caster.GetTarget();
			}
			
			if (isCasted)
			{
				DestroyObject(afterCastGO);

				currentDirection = lastSelectedTarget;

				GameObject shotGO = ShotActionTarget(TargetType.SINGLE_SHOT, actionPrefab, OnHit, OnHitObject, null, OnReach);
				Vector3 shotPosition = new Vector3(shotGO.transform.parent.transform.position.x, 2f, shotGO.transform.parent.transform.position.z);
				shotGO.transform.parent.transform.position = shotPosition;
				caster.CharacterModel.FirstActionCoolDown = 0;

				if(caster.IsMainPlayer){GameManager.ClearTargets();}
			}
		}
		else
		{
			if((isCasting || isWaitingForTarget) && isMainPlayerAction)
			{
				GameManager.DrawSkillShotTarget(caster.transform.position, range);
				
				Vector3 lookAtDirection = InputController.GetMousePositionOnFloor();
				afterCastGO.transform.LookAt(new Vector3(lookAtDirection.x, 2f, lookAtDirection.z));
			}
		}
		
		if(isDismissing)
		{
			dismissTime--;
			
			if(dismissTime <= 0)
			{
				//DestroyObject(actionGO);
			}
		}
	}
}
