using UnityEngine;
using System.Collections;

public class ParalyseAction : Action
{
	private bool reached;
	private bool isDismissing;
	private int dismissTime; 

	public void UseAction (CharacterController caster)
	{
		effectType = ActionEffectTypes.STUN_EFFECT;

		effectAmout = 100;
		effectDuration = caster.MagicDamage;

		currentCastTime = 0;
		animationTime = 10;
		castTime = 50;  
		range = 40f;
		speed = 25f;
		width = 2f;
		coolDown = 350;
		currentCoolDown = caster.CharacterModel.FirstActionCoolDown;
		isMainPlayerAction = caster.IsMainPlayer;

		reached = false;
		isDismissing = false;
		dismissTime = 100; 
		
		beforeCastPrefab = "Actions/Skills/BlackMagicCast";
		afterCastPrefab = "Actions/Skills/ParalyseHand";
		actionPrefab = "Actions/Skills/Paralyse";
		effectPrefab = "Actions/Skills/ParalyseEffect";

		CastAction (caster, true);
	}

	internal int CalculateDamage()
	{
		int magicDamage = 0;

		magicDamage = caster.MagicDamage;

		return magicDamage;
	}

	internal void OnReach(Vector3 target, GameObject shotGO)
	{
		isDismissing = true;
		StopAnimation (shotGO);

		shotGO.GetComponentInChildren<Rigidbody>().velocity *=  0.95f;
		shotGO.GetComponentInChildren<Light>().intensity *= 0.9f;

		if (shotGO.GetComponentInChildren<Rigidbody> ().velocity.magnitude <= 0.1f)
			DestroyObject (shotGO);
	}
	
	internal void OnHit(CharacterController target, GameObject shotGO)
	{
		if (target != caster)
		{
			if(target.TeamColor != caster.TeamColor)
			{
				DestroyActionPrefab(shotGO);
				
				GameObject effecGO = ObjectManager.InstantiateObject(effectPrefab, target.transform.position);	
				effecGO.transform.LookAt(caster.CharacterPosition);
				ParalyseEffect effect = effecGO.GetComponent<ParalyseEffect>();
				effect.explosionTime = effectDuration;

				target.ApplyEffect(ActionEffectTypes.STUN_EFFECT, caster, CalculateDamage(), effectDuration);
				target.AddEffectGO(effecGO);
			}
		}
	}

	internal void OnHitObject(GameObject targetGO, GameObject shotGO)
	{
		if(targetGO.tag.Equals("Shield"))
		{
			DestroyActionPrefab(shotGO);
		}

		if (target.tag == "Castle")
		{
			CharacterController castle = CharacterManager.GetController(targetGO);
			
			if(caster.TeamColor != castle.TeamColor)
			{
				DestroyActionPrefab(shotGO);			
				castle.ApplyEffect(effectType, caster, effectAmout, effectDuration);
			}
		}
	}

	private void DestroyActionPrefab(GameObject shotGO)
	{
		isDismissing = true;
		
		StopColliding(shotGO);
		StopAnimation(shotGO);	
		
		ChangeAcceleration(shotGO, 0.2f);
		
		shotGO.GetComponentInChildren<Rigidbody>().velocity =  Vector3.zero;
		shotGO.GetComponentInChildren<Light>().intensity = 0f;
	}

	void FixedUpdate()
	{
		if(isCasting)
		{
			currentCastTime++;			
			if(currentCastTime >= castTime)
			{
				ActionWaitForTarget ();
			}
			//Debug.Log("Casting Paralyse " + currentCastTime + " / " + animationTime + " / " + castTime);
		}
		
		if (caster.GetTarget() != Vector3.zero)
		{			
			if(lastSelectedTarget != caster.GetTarget())
			{
				lastSelectedTarget = caster.GetTarget();
			}
			
			if (isCasted)
			{
				GameObject shotGO = ShotActionTarget(TargetType.SINGLE_SHOT, actionPrefab, OnHit, OnHitObject, null, OnReach);
				shotGO.transform.position += Vector3.up;
				caster.CharacterModel.FirstActionCoolDown = 0;

				if(caster.IsMainPlayer){GameManager.ClearTargets();}
			}
		}
		else
		{
			if((isCasting || isWaitingForTarget) && isMainPlayerAction)
				GameManager.DrawSkillShotTarget(caster.transform.position, range);
		}

		if(isDismissing)
		{
			dismissTime--;

			if(dismissTime <= 0)
			{
				isDismissing = false;

				if(actionGO != null)
				{
					if(actionGO.transform.parent.gameObject != null)
					{
						DestroyObject(actionGO.transform.parent.gameObject);
						DestroyObject(this.gameObject);
					}
				}
			}
		}
	}
}
