﻿using UnityEngine;
using System.Collections;

public class DefenseAction : Action
{
	private bool reached;
	private bool isDismissing;
	private int dismissTime; 

	private GameObject shieldGO;

	public void UseAction (CharacterController caster)
	{
		effectType = ActionEffectTypes.PHYSICAL_DAMAGE;
		
		effectAmout = 200;
		effectDuration = 1;
		
		currentCastTime = 0;
		animationTime = 0;
		castTime = 0;  
		range = 30f;
		speed = 60f;
		width = 2f;
		coolDown = 150;
		currentCoolDown = caster.CharacterModel.FirstActionCoolDown;
		isMainPlayerAction = caster.IsMainPlayer;
		
		reached = false;
		isDismissing = false;
		dismissTime = 50; 
		
		beforeCastPrefab = "";
		afterCastPrefab = "";
		actionPrefab = "";

		CastAction (caster, false);
	}

	void FixedUpdate()
	{
		if(isCasting)
		{
			currentCastTime++;			
			if(currentCastTime >= castTime)
			{
				ActionWaitForTarget ();
			}
		}
		
		if (caster.GetTarget() != Vector3.zero)
		{			
			if(lastSelectedTarget != caster.GetTarget())
			{
				lastSelectedTarget = caster.GetTarget();
			}
			
			if (isCasted)
			{
				isCasted = false;

				shieldGO = ObjectManager.InstantiateObject("Actions/Skills/Shield", caster.transform.position);		
				Vector3 lookAtDirection = InputController.GetMousePositionOnFloor();
				shieldGO.transform.LookAt(new Vector3(lookAtDirection.x, 2f, lookAtDirection.z));	
				shieldGO.transform.parent = this.transform;
				caster.CharacterModel.FirstActionCoolDown = 0;

				if(caster.IsMainPlayer){GameManager.ClearTargets();}
			}
		}
		else
		{
			if((isCasting || isWaitingForTarget) && isMainPlayerAction)
			{
				GameManager.DrawDefenseTarget(caster.transform.position);
			}
		}

		if(isDismissing)
		{
			dismissTime--;

			if(dismissTime <= 0)
			{
				DestroyObject(actionGO);
			}
		}
	}
}
