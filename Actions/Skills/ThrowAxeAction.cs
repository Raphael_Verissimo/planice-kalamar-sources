﻿using UnityEngine;
using System.Collections;

public class ThrowAxeAction : Action
{
	private bool reached;
	private bool isDismissing;
	private int dismissTime; 

	private GameObject throwAxeGO;

	public void UseAction (CharacterController caster)
	{
		effectType = ActionEffectTypes.PHYSICAL_DAMAGE;
		
		effectAmout = 250;
		effectDuration = 1000;
		
		currentCastTime = 0;
		animationTime = 0;
		castTime = 0;  
		range = 30f;
		speed = 40f;
		width = 1f;
		coolDown = 120;
		currentCoolDown = caster.CharacterModel.ThirdActionCoolDown;
		isMainPlayerAction = caster.IsMainPlayer;
		
		reached = false;
		isDismissing = false;
		dismissTime = 50; 
		
		beforeCastPrefab = "";
		afterCastPrefab = "Actions/Skills/ThrowAxeHand";
		actionPrefab = "";

		CastAction (caster, false);
	}

	internal void OnReach(Vector3 target, GameObject shotGO)
	{
		DestroyObject (throwAxeGO);
	}
	
	internal void OnHit(CharacterController target, GameObject shotGO)
	{
		if (target != caster)
		{
			if(target.TeamColor != caster.TeamColor)
			{
				SingleShot singleShot = shotGO.GetComponent<SingleShot>();
				singleShot.isActive = false;
				ChangeAcceleration(shotGO, 0f);
				StopSpin();
				
				throwAxeGO.transform.parent = target.CharacterGO.transform;
				
				target.ApplyEffect(effectType, caster, effectAmout, effectDuration);
			}
		}
	}

	internal void OnHitObject(GameObject targetGO, GameObject shotGO)
	{
		if(targetGO.tag.Equals("Shield"))
		{
			SingleShot singleShot = shotGO.GetComponent<SingleShot>();
			singleShot.isActive = false;
			ChangeAcceleration(shotGO, 0f);
			StopSpin();
			
			throwAxeGO.transform.parent = targetGO.transform;
		}

		if (targetGO.tag.Equals("Castle"))
		{
			CharacterController castle = CharacterManager.GetController(targetGO);
			
			if(caster.TeamColor != castle.TeamColor)
			{									
				SingleShot singleShot = shotGO.GetComponent<SingleShot>();
				singleShot.isActive = false;
				ChangeAcceleration(shotGO, 0f);
				StopSpin();
				
				throwAxeGO.transform.parent = targetGO.transform;
				castle.ApplyEffect(effectType, caster, effectAmout, effectDuration);
			}
		}
	}

	void FixedUpdate()
	{
		if(isCasting)
		{
			currentCastTime++;			
			if(currentCastTime >= castTime)
			{
				ActionWaitForTarget ();
			}
		}
		
		if (caster.GetTarget() != Vector3.zero)
		{			
			if(lastSelectedTarget != caster.GetTarget())
			{
				lastSelectedTarget = caster.GetTarget();
			}
			
			if (isCasted)
			{
				DestroyObject(afterCastGO);

				Vector3 shotDirection = (lastSelectedTarget - caster.CharacterPosition).normalized;
				Vector3 axePosition = new Vector3(caster.CharacterPosition.x, 2f, caster.CharacterPosition.z);
				axePosition += shotDirection * 4;

				throwAxeGO = ObjectManager.InstantiateObject("Actions/Skills/ThrowAxeMissle", axePosition);		
				throwAxeGO.transform.LookAt(new Vector3(lastSelectedTarget.x, 2f, lastSelectedTarget.z));
				ThrowAxeSpin throwAxeSpin = throwAxeGO.GetComponent<ThrowAxeSpin>();
				//throwAxeSpin.directionOffSet = shotDirection;
				//throwAxeSpin.follow = target;
				throwAxeSpin.caster = caster;
				throwAxeSpin.isActive = true;

				GameObject shotGO = ShotActionTarget(TargetType.SINGLE_SHOT, actionPrefab, OnHit, OnHitObject, null, OnReach);
				shotGO.transform.position += Vector3.up;
				caster.CharacterModel.ThirdActionCoolDown = 0;


				throwAxeGO.transform.parent = shotGO.transform;

				if(caster.IsMainPlayer){GameManager.ClearTargets();}
			}
		}
		else
		{
			if((isCasting || isWaitingForTarget) && isMainPlayerAction)
			{
				GameManager.DrawSkillShotTarget(caster.transform.position, range);
				
				Vector3 lookAtDirection = InputController.GetMousePositionOnFloor();
				afterCastGO.transform.LookAt(new Vector3(lookAtDirection.x, 2f, lookAtDirection.z));
			}
		}

		if(isDismissing)
		{
			dismissTime--;

			if(dismissTime <= 0)
			{
				DestroyObject(actionGO);
			}
		}
	}

	private void StopSpin()
	{
		ThrowAxeSpin spin = throwAxeGO.GetComponent<ThrowAxeSpin>();
		spin.isActive = false;
	}
}
