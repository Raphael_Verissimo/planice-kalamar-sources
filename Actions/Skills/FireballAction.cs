﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FireballAction : Action
{	
	private IList<CharacterController> HitedCharacters = new List<CharacterController> ();

	void Start()
	{
		UseAction (CharacterManager.GetController(transform.parent.gameObject));
	}

	public void UseAction (CharacterController caster)
	{	
		effectType = ActionEffectTypes.PHYSICAL_DAMAGE;
		effectAmout = 350;
		effectDuration = 1;

		isActive = true;

		currentCastTime = 0;
		animationTime = 50;
		castTime = 250;  
		range = 100f;
		speed = 30f;
		width = 2f;
		area = 20f;
		coolDown = 150;
		currentCoolDown = caster.CharacterModel.ThirdActionCoolDown;
		isMainPlayerAction = caster.IsMainPlayer;

		beforeCastPrefab = "Actions/Skills/FireballCast";
		afterCastPrefab = "Actions/Skills/FireHand";
		actionPrefab = "Actions/Skills/Fireball";
		
		CastAction (caster, true);
	}

	internal int CalculateDamage()
	{
		int magicDamage = 0;
		
		magicDamage = caster.MagicDamage * 3;
		
		return magicDamage;
	}

	internal void OnFireballReach(Vector3 target, GameObject shotGO)
	{
		Debug.Log("FIREBALL REACH " + target + " !!!");	

		DestroyObject (shotGO);

		ShotActionTargetArea (TargetType.TARGET_AREA, "", OnExplosionHit, OnExplosionHitObjects);

		// Namoral i`m a fucking genius, nao vou dizer oque isso faz, te desafio a entender, ou me pergunte !!!!
		Vector3 effectOffSet = (GameManager.MainCamera.transform.position - lastSelectedTarget).normalized;
		Vector3 effectPosition = lastSelectedTarget + (effectOffSet * 15);
		
		ObjectManager.InstantiateObject ("Especial Effects/Explosion/Detonator-Wide2", effectPosition);

		if(caster.IsMainPlayer){GameManager.ClearTargets ();}
	}

	internal void OnHitObject(GameObject target, GameObject shotGO)
	{
		if(target.tag.Equals("Shield"))
		{
			lastSelectedTarget = target.transform.position;

			DestroyObject (shotGO);
			
			ShotActionTargetArea (TargetType.TARGET_AREA, "", OnExplosionHit, OnExplosionHitObjects);
			
			// Namoral i`m a fucking genius, nao vou dizer oque isso faz, te desafio a entender, ou me pergunte !!!!
			Vector3 effectOffSet = (GameManager.MainCamera.transform.position - lastSelectedTarget).normalized;
			Vector3 effectPosition = lastSelectedTarget + (effectOffSet * 15);
			
			ObjectManager.InstantiateObject ("Especial Effects/Explosion/Detonator-Wide2", effectPosition);
			
			if(caster.IsMainPlayer){GameManager.ClearTargets ();}
		}
		

	}
	
	internal void OnExplosionHit(CharacterController target, GameObject shotGO)
	{
		if(target.CharacterGO.tag == "Player"  && !HitedCharacters.Contains(target))
		{
			if(target.TeamColor != caster.TeamColor)
			{
				HitedCharacters.Add(target);
				target.ApplyEffect(effectType, caster, CalculateDamage(), 1);
			}
		}
	}

	internal void OnExplosionHitObjects(GameObject targetGO, GameObject shotGO)
	{
		if(targetGO.tag == "Castle")
		{
			CharacterController castle = CharacterManager.GetController(targetGO);

			if(caster.TeamColor != castle.TeamColor)
			{
				HitedCharacters.Add(castle);			
				castle.ApplyEffect(effectType, caster, effectAmout, effectDuration);
			}
		}
	}

	void FixedUpdate()
	{
		if (isActive) 
		{
			if (isCasting)
			{
				currentCastTime++;
				
				if (currentCastTime >= animationTime)
				{
					StopAnimation (beforeCastGO);

					if (currentCastTime >= (castTime * 0.7f))
					{
						ActionWaitForTarget ();
					}

					if (currentCastTime >= castTime)
					{
						StopCasting ();
					}
				}
				//Debug.Log ("Casting Fireball " + currentCastTime + " / " + animationTime + " / " + castTime);
			}
			
			if (caster.GetTarget() != Vector3.zero)
			{			
				if (lastSelectedTarget != caster.GetTarget()) 
				{
					lastSelectedTarget = caster.GetTarget();

					if(isMainPlayerAction)
						GameManager.DrawSelectedAreaTarget (area);
				}

				if (isCasted) 
				{
					ShotActionTarget (TargetType.SINGLE_SHOT, actionPrefab, null, OnHitObject, OnFireballReach, null);
					caster.ClearActions();
					caster.StopMoving();

					caster.CharacterModel.ThirdActionCoolDown = 0;
				}
			}
			else
			{
				if(isMainPlayerAction)
					GameManager.DrawAreaTarget (area);
			}
		}
	}
}
