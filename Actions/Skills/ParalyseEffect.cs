﻿using UnityEngine;
using System.Collections;

public class ParalyseEffect : Action
{
	public GameObject explosionGO;

	public int explosionTime;
	private int currentTime;

	private bool animationStoped;

	void Start()
	{
		currentTime = 0;

		animationStoped = false;
	}

	void FixedUpdate()
	{
		currentTime++;

		if (currentTime >= explosionTime * 0.4f)
			StopAnimation (explosionGO);
	}

	private void StopAnimation(GameObject prefab) 
	{
		ParticleEmitter[] particles = prefab.GetComponentsInChildren<ParticleEmitter> ();
		
		if(!animationStoped && particles != null)
		{
			animationStoped = true;
			foreach (ParticleEmitter particle in particles)
			{
				particle.emit = false;
			}
		}
	}
}
