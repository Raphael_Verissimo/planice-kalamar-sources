using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/**
 * Author: Kubaman Kalamar
 * 
 * Classe Principal de Controle de Inputs do Usu�rio, Por ela s�o executadas os Comandos de Teclado do Jogador.
 */
public class CameraManager
{
	#region Atributos Internos
	private static CameraController _controller;
	#endregion
	
	#region Atributos Publicos
	/**
	 * Controller dos Inputs Game 
	 */
	public static CameraController Controller
	{
		get 
		{
			if (_controller == null)
			{
				_controller = GameObject.Find("CameraController").GetComponent<CameraController>();
			
				if (_controller == null)
					Debug.Log("Camera Controller n�o foi incluido nesta Cena !!!");
			}

			return _controller;
		}
	}	
	
	public static GameObject MainCamera	{ get{return Controller.MainCamera;} set{Controller.MainCamera = value;} }
	//public static Vector3 CameraDirection { get{return Controller.CameraDirection;} }
	#endregion
}
