﻿using UnityEngine;
using System.Collections;

public class ActionManager 
{
	#region Atributos Internos
	private static ActionController _controller;
	#endregion
	
	#region Atributos Publicos
	/**
	 * Controller das Actions do Game 
	 */
	public static ActionController Controller
	{
		get 
		{
			if (_controller == null)
			{
				_controller = new ActionController();
			}
			
			return _controller;
		}
	}

	public static void UseAction(ActionType actionType, Vector3 target, CharacterController caster){Controller.UseAction(actionType, target, caster);}
	public static void UseAction(ActionType actionType, CharacterController target, CharacterController caster){Controller.UseAction(actionType, target, caster);}
	#endregion
}

public enum ActionType
{
	NONE,
	DIE,
	RESPAWN,
	
	MOVE_TO,
	CLICK_TARGET,	
	CLICK_TARGET_GO,
	MOVE_AND_CLEAR,
	PURSUIT_AND_ATTACK,
	
	//CLOSE COMBAT ACTIONS
	DEFENSE_ACTION,
	BRAVER_ACTION,
	THROW_AXE_ACTION,
	
	//RANGED COMBAT ACTIONS
	POWER_SHOT_ACTION,
	ARROW_SHOWER_ACTION,
	BULLET_TIME_ACTION,
	
	//RED MAGIC ACTIONS
	MAGIC_ARROW_ACTION,
	FIREBALL_ACTION,
	FIRE_WALL_ACTION,
	LIGHTING_ACTION,
	FIRE_ARROW_ACTION,
	ICE_ARROW_ACTION,
	METEOR_ACTION,
	THUNDER_BOLT_ACTION,
	ICE_BEAM_ACTION,
	
	COUNTER_SPELL_ACTION,
	DISPELL_ACTION,
	MIRROR_IMAGE_ACTION,
	REFLECT_ACTION,
	DISPELL_FIELD,
	TELEPORT_ACTION,
	HASTE_ACTION,
	SLOW_ACTION,
	INVISIBILITY_ACTION,
	
	HEALING_ACTION,
	CREATE_LIGHT_ACTION,
	BLESS_ACTION,
	BARRIER_ACTION,
	MAGIC_BARRIER_ACTION,
	REGENERATION_ACTION,
	REVEAL_ACTION,
	TURN_UNDEAD_ACTION,
	GREATER_HEALING_ACTION,
	
	BLIND_ACTION,
	DRAIN_LIFE_ACTION,
	CURSE_ACTION,
	POSSESION_ACTION,
	FEAR_ACTION,
	DARKNESS_ACTION,
	CONTROL_DEAD_ACTION,
	PARALYSE_ACTION,
	MANA_VAMPIRE_ACTION,
	
	CREATE_FOOD_ACTION,
	CONTROL_ANIMALS_ACTION,
	POISON_ACTION,
	ENTANGLE_ACTION,
	EARTH_WALL_ACTION,
	POLYMORTH_ACTION,
	CAMOUFLAGE_ACTION,
	CALL_FAMILIAR_ACTION,
	EARTH_QUAKE_ACTION,	
	
	MINING_ACTION
}

public enum ActionEffectTypes
{
	PHYSICAL_DAMAGE,
	
	DARKNESS_DAMAGE,
	LIGHT_DAMAGE,
	
	FIRE_DAMAGE,
	COLD_DAMAGE,
	
	ELETRIC_DAMAGE,
	POISON_DAMAGE,
	
	HEALING_LIFE,
	
	STUN_EFFECT
}
