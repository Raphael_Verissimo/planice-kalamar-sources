﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/**
 * Author: Kubaman Kalamar
 * 
 * Classe Principal do Jogo, Por ela são executadas as instruções, tanto de gerencia como de controle do jogo,
 * A Ideia aqui eh deixar claro, como as coisas acontecem, centralizando TUDO em mais alto nivel possivel.
 */
public class GameManager 
{	
	private static bool isOnline;
	private static GameController _controller;
	
	#region Public API
	/**
	 * API's Publicas que consistem na gerencia do Jogo, principal ponto de entrada para controles do Jogo.
	 */
	//public static bool IsOnline {get {return isOnline;} set {isOnline = value;}}
	public static GameObject MainCamera { get{return CameraManager.MainCamera;} set{CameraManager.MainCamera = value;} }
	//public static GameObject PlayerGO { get{return CharacterManager.PlayerGO;} }
	
	/**
	 * API's de Leitura de Niveis.
	 */
	public static void StartGame() { LoadLoginScene(); }
	public static void LoadLoginScene() { Application.LoadLevel("Login");}
	public static void LoadMainMenuScene() { Application.LoadLevel("MainMenu");	}
	public static void LoadMatchLobbyScene() { Application.LoadLevel("MatchLobby");	}
	public static void LoadMatchRoomScene()	{ Application.LoadLevel("MatchRoom"); }
	public static void LoadCreateAccountScene() { Application.LoadLevel("CreateAccount"); }
	public static void LoadJoinArena() { Application.LoadLevel("JoinArena"); }
	public static void LoadCharactersLoobyScene() {	Application.LoadLevel("CharactersLooby"); }
	public static void LoadCharacterCreationScene() { Application.LoadLevel("CharacterCreation"); }	
	public static void LoadPlaniceKalamar() { Application.LoadLevel("PlaniceKalamar"); }
	
	/**
	 * API's de Uso do Servidor de Dados.
	 */
	//public static void Login(string login, string password)	{ GameServerManager.Login(login, password);	}
	//public static void CreateAccount(string login, string email, string password) {	GameServerManager.CreateAccount(login, email, password); }
	//public static void SaveRanking(RankingModel userRank) {	GameServerManager.SaveRanking(userRank); }
	//public static void RequestTop10Ranking() { GameServerManager.GetTop10Ranking(); }	
	
	/**
	 * API's de Retorno do GameServer
	 */
	//public static void LoginFail() { /* TODO Tratar Login Fail */ }	
	//public static void CharacterCreationFail() { /* TODO Tratar Criaação de Conta Fail */ }	
	//public static void CharacterCreationDuplicated() { /* TODO Tratar Criaação de Conta Duplicada */ }		 
	//public static void ShowTop10Ranking(string received) { /* TODO Tratar Recebimento do Ranking */ }	
	//public static void GameServerError() { /* TODO Tratar Erros do Server */ }	
		 
	/**
	 * API's de Commandos do Jogo
	 */
	public static void Mouse1Click(RaycastHit hit) { CommandManager.Mouse1Click(hit); }
	public static void Mouse2Click(RaycastHit hit) { CommandManager.Mouse2Click(hit); }
	public static void AddCommand(KeyCode command, Vector3 target) { CommandManager.AddCommand(command, target); }

	/**
	 * API's de Spawn Points
	 */
	//public static GameObject BlueTeamSpawnPointGO { get{return SpawnManager.BlueTeamSpawnPointGO;} }
	//public static GameObject RedTeamSpawnPointGO { get{return SpawnManager.RedTeamSpawnPointGO;} }
	//public static Vector3 GetSpawnPointPosition(TeamColor teamColor) { return SpawnManager.GetSpawnPointPosition(teamColor); }

	/**
	 * API's de Gerenciamento de Personagens
	 */
	public static CharacterController CreateCharacter(string charName, ClassType classType, TeamColor teamColor, bool isMainPlayer) { return CharacterManager.CreateCharacter(charName, classType, teamColor, isMainPlayer); }
	public static CharacterController CreateCharacterAt(string charName, ClassType classType, TeamColor teamColor, bool isMainPlayer, Vector3 spawnPosition) { return CharacterManager.CreateCharacterAt(charName, classType, teamColor, isMainPlayer, spawnPosition); }
	public static CharacterController CreateCreepAt(string charName, ClassType classType, TeamColor teamColor, bool isMainPlayer, Vector3 spawnPosition) { return CharacterManager.CreateCreepAt(charName, classType, teamColor, isMainPlayer, spawnPosition); }
	public static CharacterController CreateTowerAt(TeamColor teamColor, Vector3 spawnPosition) { return CharacterManager.CreateTowerAt(teamColor, spawnPosition); }
	public static CharacterController CreateCastleAt(TeamColor teamColor, Vector3 spawnPosition) { return CharacterManager.CreateCastleAt(teamColor, spawnPosition); }
	public static void UseAction(ActionType actionType, Vector3 target){ CharacterManager.UseAction(actionType, target); }
	public static void UseAction(ActionType actionType, CharacterController target){ CharacterManager.UseAction(actionType, target); }

	/**
	 * API's de Interface e Efeitos de Jogo
	 */
	public static void ShowDamage(CharacterController character, int amount, Color numberColor) { UIManager.ShowDamage(character, amount, numberColor); }	
	public static void ShowHealing(CharacterController character, int amount, Color numberColor) { UIManager.ShowHealing(character, amount, numberColor); }	
	public static void SetUpLifeBar(CharacterController character) { UIManager.SetUpLifeBar(character); }
	public static void UpdateLifeBar(CharacterController character, int amount) { UIManager.UpdateLifeBar(character, amount); }
	public static void HideLifeBar(CharacterController character) { UIManager.HideLifeBar(character); }
	public static void ShowLifeBar(CharacterController character) { UIManager.ShowLifeBar(character); }

	/**
	 * API's de Interface de Targets
	 */
	public static void ClearTargets() { UIManager.ClearTargets(); }
	public static void ClearAttackTargets() { UIManager.ClearAttackTargets(); }
	public static void DrawMovimentTarget(Vector3 target) { UIManager.DrawMovimentTarget(target); }
	public static void DrawSkillShotTarget(Vector3 casterPos, float range) { UIManager.DrawSkillShotTarget(casterPos, range); }
	public static void DrawSkillShotTarget(Vector3 casterPos, float range, Vector3 target) { UIManager.DrawSkillShotTarget(casterPos, range, target); }
	public static void DrawMultipleSkillShotTarget(int index, Vector3 casterPos, float range, Vector3 target) { UIManager.DrawMultipleSkillShotTarget(index, casterPos, range, target); }
	public static void DrawAttackTarget() { UIManager.DrawAttackTarget(); }
	public static void DrawAttackTarget(Vector3 target) { UIManager.DrawAttackTarget(target); }
	public static void DrawAttackPlayer() { UIManager.DrawAttackPlayer(); }
	public static void DrawSelectedAttackTarget(Vector3 target) { UIManager.DrawSelectedAttackTarget(target); }
	public static void DrawSelectedAttackTarget(GameObject targetGO) { UIManager.DrawSelectedAttackTarget(targetGO); }
	public static void DrawAreaTarget(float area) { UIManager.DrawAreaTarget(area); }
	public static void DrawAreaTargetInRange(Vector3 casterPosition, float area, float range) { UIManager.DrawAreaTargetInRange(casterPosition, area, range); }
	public static void DrawRangedAreaTarget(Vector3 playerPos, float range) { UIManager.DrawRangedAreaTarget(playerPos, range); }
	public static void DrawSelectedAreaTarget(float area) { UIManager.DrawSelectedAreaTarget(area); }
	public static void DrawSelectedAreaTargetInRange(Vector3 casterPosition, float range) { UIManager.DrawSelectedAreaTargetInRange(casterPosition, range); }
	public static void DrawDefenseTarget(Vector3 casterPos) { UIManager.DrawDefenseTarget(casterPos); }


	/**
	 * API's de Gerenciamento de Partida
	 */
	private static GameController Controller
	{
		get
		{
			if(_controller == null)
				_controller = GameObject.Find("GameController").GetComponent<GameController>();

			return _controller;
		}
	}

	public static float GameTime { get { return Controller.GameTime; }}
	public static int GameClock { get { return Controller.GameClock; }}

	#endregion	
}
