﻿using UnityEngine;
using System.Collections;

public class CombatManager  
{
	#region Atributos Internos
	private static CombatController _controller;
	#endregion
	
	#region Atributos Publicos
	/**
	 * Controller dos Inputs Game 
	 */
	public static CombatController Controller
	{
		get 
		{
			if (_controller == null)
			{
				_controller = new CombatController();
			}
			
			return _controller;
		}
	}	

	/**
	 * API's de atack basico
	 */
	public static GameObject PursuitAndAttack(CharacterController player) {return Controller.PursuitAndAttack (player);}
	public static GameObject PursuitAndAttack(CharacterController player, CharacterController target) {return Controller.PursuitAndAttack (player, target);}
	#endregion
}
