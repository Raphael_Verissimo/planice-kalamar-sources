﻿using UnityEngine;
using System.Collections;

public class SkillManager
{
	#region Atributos Internos
	private static SkillController _controller;
	#endregion
	
	#region Atributos Publicos
	/**
	 * Controller dos Inputs Game 
	 */
	public static SkillController Controller
	{
		get 
		{
			if (_controller == null)
			{
				_controller = new SkillController();
			}
			
			return _controller;
		}
	}	

	public static GameObject CastParalyse(CharacterController player) {return Controller.CastParalyse (player);}
	public static GameObject CastDrainLife(CharacterController player) {return Controller.CastDrainLife (player);}
	public static GameObject CastFireBall(CharacterController player) {return Controller.CastFireBall (player);}

	public static GameObject CastPowerShot(CharacterController player) {return Controller.CastPowerShot (player);}
	public static GameObject CastArrowShower(CharacterController player) {return Controller.CastArrowShower (player);}
	public static GameObject CastBulletTime(CharacterController player) {return Controller.CastBulletTime (player);}

	public static GameObject CastDefense(CharacterController player) {return Controller.CastDefense (player);}
	public static GameObject CastBraver(CharacterController player) {return Controller.CastBraver (player);}
	public static GameObject CastThrowAxe(CharacterController player) {return Controller.CastThrowAxe (player);}
	#endregion
}
