﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/**
 * Author: Kubaman Kalamar
 * 
 * Classe Principal de Controle de Inputs do Usuário, Por ela são executadas os Comandos de Teclado do Jogador.
 */
public class CommandManager
{
	#region Atributos Internos
	private static CommandController _controller;
	#endregion
	
	#region Atributos Publicos
	/**
	 * Controller dos Inputs Game 
	 */
	public static CommandController Controller
	{
		get 
		{
			if (_controller == null)
			{
				_controller = new CommandController();
			}

			return _controller;
		}
	}	

	/**
	 * API's de Cadastro de Keys e Allias.
	 */
	public static void SetClassCommands(CharacterController character, ClassType characterClass) { Controller.SetClassCommands(character, characterClass); }
	//private static void UpdateCommandAllias(KeyCode keyCode, ActionType actionType) { Controller.UpdateCommandAllias(keyCode, actionType); }

	/**
	 * API's de Uso dos Commandos.
	 */
	public static void Mouse1Click(RaycastHit hit) { Controller.Mouse1Click(hit); }
	public static void Mouse2Click(RaycastHit hit) { Controller.Mouse2Click(hit); }
	public static void AddCommand(KeyCode command, Vector3 target) { Controller.AddCommand(command, target); }
	#endregion
}
