﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CharacterManager 
{
	public static IList<CharacterController> Characters = new List<CharacterController>();

	public static IList<CharacterController> BlueCharacters = new List<CharacterController>();
	public static IList<CharacterController> BlueCreeps = new List<CharacterController>();
	public static IList<CharacterController> BlueStructures = new List<CharacterController>();

	public static IList<CharacterController> RedCharacters = new List<CharacterController>();
	public static IList<CharacterController> RedCreeps = new List<CharacterController>();
	public static IList<CharacterController> RedStructures = new List<CharacterController>();

	public static void ClearCharacters() { Characters.Clear(); }
	public static void AddCharacter(CharacterController character)
	{ 
		if(!Characters.Contains(character))
		{
			Characters.Add(character); 

			if(character.TeamColor == TeamColor.BLUE_TEAM)
			{
				BlueCharacters.Add(character);
			}
			else
			{
				RedCharacters.Add(character);
			}
		}
	}

	public static void RemoveCharacter(CharacterController character)
	{ 
		if(!Characters.Contains(character))
		{
			Characters.Remove(character);

			if(character.TeamColor == TeamColor.BLUE_TEAM)
			{
				BlueCharacters.Remove(character);
			}
			else
			{
				RedCharacters.Remove(character);
			}
		}
	}

	public static void AddCreep(CharacterController character)
	{ 
		if(!Characters.Contains(character))
		{
			Characters.Add(character); 
			
			if(character.TeamColor == TeamColor.BLUE_TEAM)
			{
				BlueCreeps.Add(character);
			}
			else
			{
				RedCreeps.Add(character);
			}
		}
	}
	
	public static void RemoveCreeps(CharacterController character)
	{ 
		if(!Characters.Contains(character))
		{
			Characters.Remove(character);
			
			if(character.TeamColor == TeamColor.BLUE_TEAM)
			{
				BlueCreeps.Remove(character);
			}
			else
			{
				RedCreeps.Remove(character);
			}
		}
	}

	public static void AddStructures(CharacterController character)
	{ 
		if(!Characters.Contains(character))
		{
			Characters.Add(character); 
			
			if(character.TeamColor == TeamColor.BLUE_TEAM)
			{
				BlueStructures.Add(character);
			}
			else
			{
				RedStructures.Add(character);
			}
		}
	}
	
	public static void RemoveStructures(CharacterController character)
	{ 
		if(!Characters.Contains(character))
		{
			Characters.Remove(character);
			
			if(character.TeamColor == TeamColor.BLUE_TEAM)
			{
				BlueStructures.Remove(character);
			}
			else
			{
				RedStructures.Remove(character);
			}
		}
	}
	
	public static int CharactersCount 
	{ 
		get
		{
			if(Characters == null) 
				Characters = new List<CharacterController>();
				
			return Characters.Count;
		} 
	}
	
	public static CharacterController MainCharacter 
	{ 
		get
		{
			if(Characters == null) 
				return null;
				
			foreach (CharacterController character in Characters)
			{
				if (character.IsMainPlayer)
					return character;
			}
			
			return null;
		} 

		set 
		{		

		}
	}
	
	public int MainCharacterIndex
	{
		get
		{
			if(MainCharacter == null)
				return 0;
		
			return Characters.IndexOf(MainCharacter);
		}
	}
	
	public GameObject PlayerGO
	{
		get
		{
			if(MainCharacter == null)
				return null;
		
			return MainCharacter.CharacterGO;
		}
	}
	
	public GameObject GetCharacterGO (CharacterController character)
	{
		if (Characters.Contains(character))				
			return Characters[Characters.IndexOf(character)].CharacterGO;

		return null;
	}
	
	public CharacterModel GetCharacterModel (CharacterController character)
	{
		if (Characters.Contains(character))				
			return Characters[Characters.IndexOf(character)].CharacterModel;

		return null;
	}
	
	public int GetCharacterIndex (CharacterController character)
	{
		//if (Characters.Contains(character))				
		//	return Characters[Characters.IndexOf(character)].Index;

		return 0;
	}

	public static CharacterController GetController(GameObject characterGO)
	{
		CharacterController characterController = null;

		if (characterGO.GetComponent<CharacterController> () != null)
			characterController = characterGO.GetComponent<CharacterController> ();
	
		if (characterGO.GetComponentInChildren<CharacterController> () != null)
			characterController = characterGO.GetComponentInChildren<CharacterController> ();

		if (characterGO.GetComponentInParent<CharacterController> () != null)
			characterController = characterGO.GetComponentInParent<CharacterController> ();

		return characterController;
	}

	public static void ClearMainCharacter()
	{
		int mainCharacter = int.MinValue;
		
		foreach (CharacterController character in Characters)
		{
			if (character.IsMainPlayer)
				mainCharacter = Characters.IndexOf(character);
		}

		if(mainCharacter > int.MinValue)
			Characters.RemoveAt(mainCharacter);	
	}

	public static CharacterController CreateCharacter(string charName, ClassType classType, TeamColor teamColor, bool isMainPlayer)
	{
		CharacterController controller = null;
	
		Vector3 spawnPoint = Vector3.zero;//GameManager.GetSpawnPointPosition(teamColor);
		spawnPoint = new Vector3 (spawnPoint.x, 1, spawnPoint.z);
		
		if (spawnPoint != Vector3.zero)
		{
			controller = ObjectManager.CharacterController ("Characters/Character", spawnPoint);

			CharacterModel model = new CharacterModel(charName, classType);

			controller.IsAlive = true;
			controller.CharacterModel = model;
			controller.TeamColor = teamColor;
			controller.IsMainPlayer = isMainPlayer;
			
			if (isMainPlayer)			
			{
				CommandManager.SetClassCommands(controller, classType);
				ClearMainCharacter();
			}

			SetCharacterColor(controller, classType, teamColor);

			LifeBar lifeBar = controller.CharacterGO.GetComponentInChildren<LifeBar> ();			
			lifeBar.SetUpLifeBar(controller);
		}

		AddCharacter(controller);

		return controller;
	}

	public static CharacterController CreateCharacterAt(string charName, ClassType classType, TeamColor teamColor, bool isMainPlayer, Vector3 spawnPosition)
	{
		CharacterController controller = null;
		
		Vector3 spawnPoint = Vector3.zero;//GameManager.GetSpawnPointPosition(teamColor);
		spawnPoint = new Vector3 (spawnPosition.x, 1, spawnPosition.z);
		
		if (spawnPoint != Vector3.zero)
		{
			controller = ObjectManager.CharacterController ("Characters/Character", spawnPoint);
			
			CharacterModel model = new CharacterModel(charName, classType);
			
			controller.IsAlive = true;
			controller.CharacterModel = model;
			controller.TeamColor = teamColor;
			controller.IsMainPlayer = isMainPlayer;
			
			if (isMainPlayer)			
			{
				CommandManager.SetClassCommands(controller, classType);
				ClearMainCharacter();
			}
			
			SetCharacterColor(controller, classType, teamColor);
			
			LifeBar lifeBar = controller.CharacterGO.GetComponentInChildren<LifeBar> ();			
			lifeBar.SetUpLifeBar(controller);
		}
		
		AddCharacter(controller);
		
		return controller;
	}

	public static CharacterController CreateCreepAt(string charName, ClassType classType, TeamColor teamColor, bool isMainPlayer, Vector3 spawnPosition)
	{
		CharacterController controller = null;
		
		Vector3 spawnPoint = Vector3.zero;//GameManager.GetSpawnPointPosition(teamColor);
		spawnPoint = new Vector3 (spawnPosition.x, 1, spawnPosition.z);
		
		if (spawnPoint != Vector3.zero)
		{
			controller = ObjectManager.CharacterController ("Characters/Creep", spawnPoint);
			
			CharacterModel model = new CharacterModel(charName, classType);
			
			controller.IsAlive = true;
			controller.IsIdle = true;
			controller.CharacterModel = model;
			controller.TeamColor = teamColor;
			controller.IsMainPlayer = isMainPlayer;
			
			if (isMainPlayer)			
			{
				CommandManager.SetClassCommands(controller, classType);
				ClearMainCharacter();
			}
			
			SetCharacterColor(controller, classType, teamColor);
			
			LifeBar lifeBar = controller.CharacterGO.GetComponentInChildren<LifeBar> ();			
			lifeBar.SetUpLifeBar(controller);
		}
		
		AddCreep(controller);
		
		return controller;
	}

	public static CharacterController CreateTowerAt(TeamColor teamColor, Vector3 spawnPosition)
	{
		CharacterController controller = null;
		
		Vector3 spawnPoint = Vector3.zero;//GameManager.GetSpawnPointPosition(teamColor);
		spawnPoint = new Vector3 (spawnPosition.x, 0, spawnPosition.z);
		
		if (spawnPoint != Vector3.zero)
		{
			controller = ObjectManager.CharacterController ("Structures/Tower", spawnPoint);
			
			CharacterModel model = new CharacterModel("", ClassType.TOWER);
			
			controller.IsAlive = true;
			controller.CharacterModel = model;
			controller.TeamColor = teamColor;
			controller.IsMainPlayer = false;
			
			LifeBar lifeBar = controller.CharacterGO.GetComponentInChildren<LifeBar> ();			
			lifeBar.SetUpLifeBar(controller);
		}
		
		AddStructures(controller);
		
		return controller;
	}

	public static void CreateBrokenTowerAt(TeamColor teamColor, Vector3 spawnPosition)
	{
		Vector3 spawnPoint = Vector3.zero;//GameManager.GetSpawnPointPosition(teamColor);
		spawnPoint = new Vector3 (spawnPosition.x, 0, spawnPosition.z);
		
		if (spawnPoint != Vector3.zero)
		{
			ObjectManager.InstantiateObject ("Structures/BrokenTower", spawnPoint);
		}
	}

	public static CharacterController CreateCastleAt(TeamColor teamColor, Vector3 spawnPosition)
	{
		CharacterController controller = null;
		
		Vector3 spawnPoint = Vector3.zero;//GameManager.GetSpawnPointPosition(teamColor);
		spawnPoint = new Vector3 (spawnPosition.x, 6.3f, spawnPosition.z);

		string baseName = "";

		if (teamColor == TeamColor.BLUE_TEAM)
			baseName = "Structures/HeroBase";
		else			
			baseName = "Structures/VilanBase";
		
		if (spawnPoint != Vector3.zero)
		{
			controller = ObjectManager.CharacterController (baseName, spawnPoint);
			
			CharacterModel model = new CharacterModel("", ClassType.CASTLE);
			
			controller.IsAlive = true;
			controller.CharacterModel = model;
			controller.TeamColor = teamColor;
			controller.IsMainPlayer = false;
			
			LifeBar lifeBar = controller.CharacterGO.GetComponentInChildren<LifeBar> ();			
			lifeBar.SetUpLifeBar(controller);
		}
		
		AddStructures(controller);
		
		return controller;
	}

	public static void UseAction(ActionType actionType, Vector3 target) 
	{
		if (MainCharacter != null)
			MainCharacter.UseAction(actionType, target);
	}

	public static void UseAction(ActionType actionType, CharacterController target) 
	{
		if (MainCharacter != null)
			MainCharacter.UseAction(actionType, target);
	}

	private static void SetCharacterColor(CharacterController controller, ClassType classType, TeamColor teamColor)
	{
		switch(classType)
		{
			case ClassType.WARRIOR:
			case ClassType.MERCENARY:
				if(teamColor == TeamColor.BLUE_TEAM)					
					controller.SetCharacterMaterial(Resources.Load("Materials/BlueWarrior", typeof(Material)) as Material);
				else
					controller.SetCharacterMaterial((Material)Resources.Load("Materials/RedWarrior", typeof(Material)) as Material);
			break;

			case ClassType.ARCHER:
			case ClassType.HUNTER:
				if(teamColor == TeamColor.BLUE_TEAM)
					controller.SetCharacterMaterial((Material)Resources.Load("Materials/BlueArcher", typeof(Material)) as Material);
				else
					controller.SetCharacterMaterial((Material)Resources.Load("Materials/RedArcher", typeof(Material)) as Material);
			break;

			case ClassType.MAGE:
			case ClassType.DISCIPLE:
				if(teamColor == TeamColor.BLUE_TEAM)
					controller.SetCharacterMaterial((Material)Resources.Load("Materials/BlueMage", typeof(Material)) as Material);
				else
					controller.SetCharacterMaterial((Material)Resources.Load("Materials/RedMage", typeof(Material)) as Material);
			break;
		}

	}
}

public enum TeamColor
{
	BLUE_TEAM,
	RED_TEAM
}