using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/**
 * Author: Kubaman Kalamar
 * 
 * Classe Principal de Controle das Interfaces de Jogo nos Personagens.
 */
public class UIManager
{
	#region Atributos Internos
	private static UIController _controller;
	#endregion
	
	#region Atributos Publicos
	/**
	 * Controller das Interfaces do Game 
	 */
	public static UIController Controller
	{
		get 
		{
			if (_controller == null)
			{
				_controller = GameObject.Find("UIController").GetComponent<UIController>();
				
				if (_controller == null)
					Debug.Log("UI Controller n�o foi incluido nesta Cena !!!");
			}

			return _controller;
		}
	}	
	
	/**
	 * Metodos de Controle de Display de Dano e Vida
	 */
	public static void ShowDamage(CharacterController character, int amount, Color numberColor) { Controller.ShowDamage(character, amount, numberColor); }
	public static void ShowHealing(CharacterController character, int amount, Color numberColor) { Controller.ShowHealing(character, amount, numberColor); }
	public static void SetUpLifeBar(CharacterController character) { Controller.SetUpLifeBar(character); }
	public static void UpdateLifeBar(CharacterController character, int amount) { Controller.UpdateLifeBar(character, amount); }
	public static void HideLifeBar(CharacterController character) { Controller.HideLifeBar(character); }
	public static void ShowLifeBar(CharacterController character) { Controller.ShowLifeBar(character); }

	/**
	 * Metodos de Controle de exibi��o de Targets
	 */
	public static void ClearTargets() { Controller.ClearTargets(); }
	public static void ClearAttackTargets() { Controller.ClearAttackTargets(); }
	public static void DrawMovimentTarget(Vector3 target) { Controller.DrawMovimentTarget(target); }
	public static void DrawSkillShotTarget(Vector3 casterPos, float range) { Controller.DrawSkillShotTarget(casterPos, range); }
	public static void DrawSkillShotTarget(Vector3 casterPos, float range, Vector3 target) { Controller.DrawSkillShotTarget(casterPos, range, target); }
	public static void DrawMultipleSkillShotTarget(int index, Vector3 casterPos, float range, Vector3 target) { Controller.DrawMultipleSkillShotTarget(index, casterPos, range, target); }
	public static void DrawAttackTarget() { Controller.DrawAttackTarget(); }
	public static void DrawAttackTarget(Vector3 target) { Controller.DrawAttackTarget(target); }
	public static void DrawAttackPlayer() { Controller.DrawAttackPlayer(); }
	public static void DrawSelectedAttackTarget(Vector3 target) { Controller.DrawSelectedAttackTarget(target); }
	public static void DrawSelectedAttackTarget(GameObject targetGO) { Controller.DrawSelectedAttackTarget(targetGO); }
	public static void DrawAreaTarget(float area) { Controller.DrawAreaTarget(area); }
	public static void DrawAreaTargetInRange(Vector3 casterPosition, float area, float range) { Controller.DrawAreaTargetInRange(casterPosition, area, range); }
	public static void DrawRangedAreaTarget(Vector3 playerPos, float range) { Controller.DrawRangedAreaTarget(playerPos, range); }
	public static void DrawSelectedAreaTarget(float area) { Controller.DrawSelectedAreaTarget(area); }
	public static void DrawSelectedAreaTargetInRange(Vector3 casterPosition, float range) { Controller.DrawSelectedAreaTargetInRange(casterPosition, range); }
	public static void DrawDefenseTarget(Vector3 casterPos) { Controller.DrawDefenseTarget(casterPos); }
	#endregion
}