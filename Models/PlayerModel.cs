﻿using UnityEngine;
using System.Collections;

public class PlayerModel
{
	private string login;
	private bool isMainPlayer;

	private ClassType currentCharacterClass;
	private TeamColor currentTeamColor;

	public string Login { get{return login;} set{login = value;} }
	public bool IsMainPlayer { get{return isMainPlayer;} set{isMainPlayer = value;} }

	public ClassType CurrentCharacterClass { get{return currentCharacterClass;} set{currentCharacterClass = value;} }
	public TeamColor CurrentTeamColor { get{return currentTeamColor;} set{currentTeamColor = value;} }

	public PlayerModel (string login, bool isMainPlayer, ClassType currentCharacterClass, TeamColor currentTeamColor)
	{
		this.login = login;
		this.isMainPlayer = isMainPlayer;
		this.currentCharacterClass = currentCharacterClass;
		this.currentTeamColor = currentTeamColor;
	}
}
