using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

public class CharacterModel  
{
	#region Atributos Internos	
	[SerializeField] private string characterName;
	[SerializeField] private int actorNumber;
	[SerializeField] private ClassType characterClass;

	[SerializeField] private bool isMainPlayer;
	[SerializeField] private int modelIndex;

	[SerializeField] private int strength;
    [SerializeField] private int vitality;
	[SerializeField] private int inteligence;
    [SerializeField] private int wisdom;
    [SerializeField] private int dextery;
    [SerializeField] private int agility;
	
	[SerializeField] private float strengthExperience;
    [SerializeField] private float vitalityExperience;
    [SerializeField] private float inteligenceExperience;
    [SerializeField] private float wisdomExperience;
    [SerializeField] private float dexteryExperience;
    [SerializeField] private float agilityExperience;

	[SerializeField] private int hitPoints;
	[SerializeField] private int magicPoints;	
	
	[SerializeField] private int maxHitPoints;
	[SerializeField] private int maxMagicPoints;
	
	[SerializeField] private int currentCoolDown;
	[SerializeField] private int firstActionCoolDown;
	[SerializeField] private int secondActionCoolDown;
	[SerializeField] private int thirdActionCoolDown;
	[SerializeField] private int specialActionCoolDown;	

	
	[SerializeField] private float attackRange;
	[SerializeField] private int attackCoolDown;	
	[SerializeField] private int attackDuration;
	[SerializeField] private string weaponPrefab;
	[SerializeField] private WeaponType weapon;
	
	[SerializeField] private int attackDamage;
	[SerializeField] private int magicDamage;
	[SerializeField] private int armor;
	[SerializeField] private int magicResist;
	[SerializeField] private float attackSpeed;
	[SerializeField] private int coolDownReduction;
	[SerializeField] private float movimentSpeed;

	[SerializeField] private float criticalChance;
	[SerializeField] private float criticalDamage;
	#endregion	
	
	#region Atributos de Interface			
	//[SerializeField] private DisplayController display;
	[SerializeField] private GameObject lifeBar;	
	#endregion	

	#region Properties
	public string CharacterName { get{return characterName;} set{characterName = value;}} 
	public ClassType CharacterClass { get{return characterClass;} set{characterClass = value;}} 
	public int ActorNumber { get{return actorNumber;} set{actorNumber = value;}} 
	public bool IsMainPlayer { get{return isMainPlayer;} set{isMainPlayer = value;}} 
	public int ModelIndex { get{return modelIndex;} set{modelIndex = value;}}

	public int Strength { get{return strength;} set{strength = value;}}
    public int Vitality { get { return vitality; } set { vitality = value; } }
    public int Inteligence { get { return inteligence; } set { inteligence = value; }}
    public int Wisdom { get { return wisdom; } set { wisdom = value; }}
    public int Dextery { get { return dextery; } set { dextery = value; }}
    public int Agility { get { return agility; } set { agility = value; }} 

	public float StrengthExperience { get{return strengthExperience;} set{strengthExperience = value;}}
    public float VitalityExperience { get { return vitalityExperience; } set { vitalityExperience = value; }}
    public float InteligenceExperience { get { return inteligenceExperience; } set { inteligenceExperience = value; }}
    public float WisdomExperience { get { return wisdomExperience; } set { wisdomExperience = value; }}
    public float DexteryExperience { get { return dexteryExperience; } set { dexteryExperience = value; }}
    public float AgilityExperience { get { return agilityExperience; } set { agilityExperience = value; }}

    public int HitPoints { get { return hitPoints; } set { hitPoints = value; }}
    public int MagicPoints { get { return magicPoints; } set { magicPoints = value; }}
    public int MaxHitPoints { get { return maxHitPoints; } set { maxHitPoints = value; }}
    public int MaxMagicPoints { get { return maxMagicPoints; } set { maxMagicPoints = value; }}

	public float LifePercent { get { return (float)((float)hitPoints / (float)maxHitPoints); } }
	public float ManaPercent { get { return (float)((float)magicPoints / (float)maxMagicPoints); } }
	
	public int CurrentCoolDown { get { return currentCoolDown; } set { currentCoolDown = value; }}
    public int FirstActionCoolDown { get { return firstActionCoolDown; } set { firstActionCoolDown = value; }}
    public int SecondActionCoolDown { get { return secondActionCoolDown; } set { secondActionCoolDown = value; }}
    public int ThirdActionCoolDown { get { return thirdActionCoolDown; } set { thirdActionCoolDown = value; }}
    public int SpecialActionCoolDown { get { return specialActionCoolDown; } set { specialActionCoolDown = value; }}	
	    
    public float AttackRange { get { return attackRange; } set { attackRange = value; }}
	public int AttackCoolDown { get { return attackCoolDown; } set { attackCoolDown = value; }}
	public int AttackDuration { get { return attackDuration; } set { attackDuration = value; }}
	public string WeaponPrefab { get { return weaponPrefab; } set { weaponPrefab = value; }}	
	public WeaponType Weapon { get { return weapon; } set { weapon = value; }}	
	
	public int AttackDamage { get { return attackDamage; } set { attackDamage = value; }}
	public int MagicDamage { get { return magicDamage; } set { magicDamage = value; }}
	public int Armor { get { return armor; } set { armor = value; }}
	public int MagicResist { get { return magicResist; } set { magicResist = value; }}
	public float AttackSpeed { get { return attackSpeed; } set { attackSpeed = value; }}
	public int CoolDownReduction { get { return coolDownReduction; } set { coolDownReduction = value; }}
	public float MovimentSpeed { get { return movimentSpeed; } set { movimentSpeed = value; }}

	public float CriticalChance { get { return criticalChance; } set { criticalChance = value; }}
	public float CriticalDamage { get { return criticalDamage; } set { criticalDamage = value; }}
	#endregion
	
	public CharacterModel(string charName, ClassType classType)
	{
		characterName = charName;
		characterClass = classType;

		currentCoolDown = 2000;
		firstActionCoolDown = 2000;
		secondActionCoolDown = 2000;
		thirdActionCoolDown = 2000;
		specialActionCoolDown = 2000;

		SetClass (classType);
   }
	
	public void SetClass(ClassType characterClass)
	{	
		switch (characterClass)
		{
			case ClassType.MAGE:
				SetWeapon(WeaponType.SLING);	

				hitPoints = 400;
				maxHitPoints = 400;
				
				attackDamage = 65;
				magicDamage = 150;
				armor = 25;
				magicResist = 65;
				attackSpeed = 0.35f;
				coolDownReduction = 0;
				movimentSpeed = 600f;

				attackRange = 30f;
				attackCoolDown = 120;			
				attackDuration = 40;
				criticalChance = 5;
				criticalDamage = 1.5f;
			break;

			case ClassType.ARCHER:
				SetWeapon(WeaponType.BOW);	
				
				hitPoints = 500;
				maxHitPoints = 500;

				attackDamage = 85;
				magicDamage = 20;
				armor = 50;
				magicResist = 35;
				attackSpeed = 0.5f;
				coolDownReduction = 0;
				movimentSpeed = 650f;	
				
				attackRange = 35f;
				attackCoolDown = 100;
				attackDuration = 100;
				criticalChance = 12;
				criticalDamage = 2.5f;
			break;

			case ClassType.WARRIOR:
				SetWeapon(WeaponType.SWORD);	
					
				hitPoints = 700;
				maxHitPoints = 700;	

				attackDamage = 125;
				magicDamage = 0;
				armor = 75;
				magicResist = 0;
				attackSpeed = 0.75f;
				coolDownReduction = 0;
				movimentSpeed = 550f;

				attackRange = 2.5f;
				attackCoolDown = 50;
				attackDuration = 100;
				criticalChance = 25;
				criticalDamage = 2f;
			break;

			case ClassType.DISCIPLE:
				SetWeapon(WeaponType.SLING);	
				
				hitPoints = 200;
				maxHitPoints = 150;	
				movimentSpeed = 350f;	

				attackDamage = 25;
				attackRange = 20f;
				attackCoolDown = 250;
				attackDuration = 30;
				criticalChance = 1;
				criticalDamage = 1.5f;
			break;

			case ClassType.HUNTER:
				SetWeapon(WeaponType.BOW);	
				
				hitPoints = 250;
				maxHitPoints = 200;	
				movimentSpeed = 400f;	

				attackDamage = 35;
				attackRange = 25f;
				attackCoolDown = 300;
				attackDuration = 80;
				criticalChance = 5;
				criticalDamage = 1.75f;
			break;

			case ClassType.MERCENARY:
				SetWeapon(WeaponType.SWORD);	
				
				hitPoints = 350;
				maxHitPoints = 350;	
				movimentSpeed = 450f;

				attackDamage = 50;
				attackRange = 2.5f;
				attackCoolDown = 200;
				attackDuration = 100;
				criticalChance = 10;
				criticalDamage = 2.0f;
			break;

			case ClassType.TOWER:
				SetWeapon(WeaponType.BOW);	
				
				hitPoints = 5000;
				maxHitPoints = 5000;	
				movimentSpeed = 0f;
				
				attackDamage = 175;
				attackRange = 40f;
				attackCoolDown = 300;
				attackDuration = 80;
				criticalChance = 0;
				criticalDamage = 1.0f;
			break;

			case ClassType.CASTLE:
				SetWeapon(WeaponType.CANNON);	
				
				hitPoints = 10000;
				maxHitPoints = 10000;	
				movimentSpeed = 0f;
				
				attackDamage = 350;
				attackRange = 100f;
				attackCoolDown = 1000;
				attackDuration = 80;
				criticalChance = 0;
				criticalDamage = 1.0f;
			break;
		}
	}
	
	private void SetWeapon(WeaponType weapon)
	{
		switch (weapon)
		{
			case WeaponType.BOW:
				weaponPrefab = "Actions/Combat/Arrow";				
			break;

			case WeaponType.SWORD:
				weaponPrefab = "Actions/Combat/Sword";				
			break;

			case WeaponType.SLING:
				weaponPrefab = "Actions/Combat/Stone";
				
			break;
		}
		this.weapon = weapon;
	}
	
	public void UpdateCoolDown()
	{
		//if(IsMainPlayer)
		//		Debug.Log ("CoolDown " + firstActionCoolDown + " | " + secondActionCoolDown + " | " + thirdActionCoolDown);
	
		currentCoolDown++;
		firstActionCoolDown++;
		secondActionCoolDown++;
		thirdActionCoolDown++;
		specialActionCoolDown++;
	}
	
	public int CalculateDamage(ActionEffectTypes effect, int amount)
	{
		/* TODO calc damage */
		return amount;
	}
	
	public int CalculateEffectDuration(ActionEffectTypes effect, int duration)
	{
		/* TODO calc duration */
		return duration;
	}
	
	public void ApplyDamage(int damage, CharacterController character)
	{
		/*
		if(damage > 0)
		{
			hitPoints += damage;
			
			if(hitPoints > maxHitPoints)
			{
				hitPoints = maxHitPoints;
				
				GameManager.SetFullLife(character);				
			}
			else
			{
				GameManager.SetLifeDamage(character, damage, maxHitPoints);		
			}
		}
		else
		{
			characterModel.HitPoints -= amount;
			
			float damagePercent = amount / (float)characterModel.MaxHitPoints;
			
			float lifeScale = damagePercent * 10f;
			
			lifeBar.transform.localScale -= new Vector3(lifeScale, 0, 0);
			lifeBar.transform.localPosition -= new Vector3(lifeScale/2, 0, 0); 
			
			Debug.Log (characterModel.HitPoints + " / " + characterModel.MaxHitPoints);
			
			if (characterModel.HitPoints < 1)
			{
				Death(senderIndex, amount);
			}
		}
		*/
	}

	public static CharacterModel FromJson(string json)
	{
		/*
		CharacterModel returnCharacter = new CharacterModel();
		
		string[] parsedJson = json.Split(';');
		
		foreach(string elem in parsedJson)
		{
			string key = elem.Split(':')[0];
			string value = elem.Split(':')[1];
			
			if (key.Equals("Name"))	returnCharacter.CharacterName = value;
			
			if (key.Equals("ModelIndex")) returnCharacter.ModelIndex = int.Parse(value);
			
			if (key.Equals("Strength"))	returnCharacter.Strength =  int.Parse(value);
			if (key.Equals("Vitality"))	returnCharacter.Vitality = int.Parse(value);
			if (key.Equals("Inteligence")) returnCharacter.Inteligence = int.Parse(value);
			if (key.Equals("Wisdom")) returnCharacter.Wisdom = int.Parse(value);
			if (key.Equals("Dextery")) returnCharacter.Dextery = int.Parse(value);
			if (key.Equals("Agility")) returnCharacter.Agility = int.Parse(value);
			
			if (key.Equals("StrengthExperience")) returnCharacter.StrengthExperience = float.Parse(value);
			if (key.Equals("VitalityExperience")) returnCharacter.VitalityExperience = float.Parse(value);
			if (key.Equals("InteligenceExperience")) returnCharacter.InteligenceExperience = float.Parse(value);
			if (key.Equals("InteligenceExperience")) returnCharacter.WisdomExperience = float.Parse(value);
			if (key.Equals("DexteryExperience")) returnCharacter.DexteryExperience = float.Parse(value);
			if (key.Equals("AgilityExperience")) returnCharacter.AgilityExperience = float.Parse(value);

			if (key.Equals("HitPoints")) returnCharacter.HitPoints = int.Parse(value);
			if (key.Equals("MagicPoints"))	returnCharacter.MagicPoints = int.Parse(value);
			if (key.Equals("MaxHitPoints")) returnCharacter.MaxHitPoints = int.Parse(value);
			if (key.Equals("MaxMagicPoints")) returnCharacter.MaxMagicPoints = int.Parse(value);

			/*
			if (key.Equals("LeftHand")) returnCharacter.Inteligence = new Item;
			if (key.Equals("RightHand")) returnCharacter.Wisdom = value;

			if (key.Equals("Head"))	returnCharacter.Strength = value;
			if (key.Equals("Neck"))	returnCharacter.Vitality = value;
			if (key.Equals("Body")) returnCharacter.Inteligence = value;
			if (key.Equals("Arms")) returnCharacter.Wisdom = value;
			if (key.Equals("Cape")) returnCharacter.Dextery = value;
			if (key.Equals("Belt")) returnCharacter.Agility = value;
			if (key.Equals("Legs"))	returnCharacter.Strength = value;
			if (key.Equals("LeftWrist"))	returnCharacter.Vitality = value;
			if (key.Equals("RightWrist")) returnCharacter.Inteligence = value;
			if (key.Equals("LeftFingers")) returnCharacter.Wisdom = value;
			if (key.Equals("RightFingers")) returnCharacter.Dextery = value;
			if (key.Equals("Foot")) returnCharacter.Agility = value;
		}
		
		return returnCharacter;
		*/
		return null;
	}

    public string ToJson()
    {
		/*
        string json = "";

        json += JsonUtils.StartJson("CharacterModel");

		json += JsonUtils.AddElement("Name", CharacterName);

		json += JsonUtils.AddElement("ModelIndex", ModelIndex.ToString());

        json += JsonUtils.AddElement("Strength", Strength.ToString());
		json += JsonUtils.AddElement("Vitality", Vitality.ToString());
		json += JsonUtils.AddElement("Inteligence", Inteligence.ToString());
		json += JsonUtils.AddElement("Wisdom", Wisdom.ToString());
		json += JsonUtils.AddElement("Dextery", Dextery.ToString());
		json += JsonUtils.AddElement("Agility", Agility.ToString());

		json += JsonUtils.AddElement("StrengthExperience", StrengthExperience.ToString());
		json += JsonUtils.AddElement("VitalityExperience", VitalityExperience.ToString());
		json += JsonUtils.AddElement("InteligenceExperience", InteligenceExperience.ToString());
		json += JsonUtils.AddElement("WisdomExperience", WisdomExperience.ToString());
		json += JsonUtils.AddElement("DexteryExperience", DexteryExperience.ToString());
		json += JsonUtils.AddElement("AgilityExperience", AgilityExperience.ToString());

		json += JsonUtils.AddElement("HitPoints", HitPoints.ToString());
		json += JsonUtils.AddElement("MagicPoints", MagicPoints.ToString());
		json += JsonUtils.AddElement("MaxHitPoints", MaxHitPoints.ToString());
		json += JsonUtils.AddElement("MaxMagicPoints", MaxMagicPoints.ToString());

        json += JsonUtils.EndJson;
        return json;
        */

		return "";
    }

	//public void Collect\ 

    //TODO O model deveria apenas conter as Informa��es e Serilia��es, Mover a��es para outra classe.
	
	/**
	 * Trata o dano recebido, atualizando a vida, a barra de vida e exibindo o dano em numeros vermelhos.
	 */
	public void DoApplyDamage()//ActionEffectTypes effect, Vector3 damage)
	{
		/*
		Color damageColor = Color.white;

		switch(effect)
		{
			case ActionEffectTypes.PHYSICAL_DAMAGE: damageColor = Color.red; break;			
			case ActionEffectTypes.FIRE_DAMAGE: damageColor = Color.magenta; break;
		}
		
		display.ShowNumber((int)damage.y, damageColor, this.gameObject);
		
		hitPoints -= (int)damage.y;
		
		float damagePercent = damage.y / (float)maxHitPoints;
		
		float lifeScale = damagePercent * 10f;
		
		lifeBar.transform.localScale -= new Vector3(lifeScale, 0, 0);
		lifeBar.transform.localPosition -= new Vector3(lifeScale/2, 0, 0); 

		Debug.Log (hitPoints + " / " + maxHitPoints);

		if (hitPoints < 1)
			Death(damage);
		*/
	}	

	public void Death(Vector3 damage)
	{
		/*
		ReleaseCamera ();

		Debug.Log ("Explode char " + this.transform.position + " = " + damage);

		this.GetComponent<Rigidbody>().isKinematic = false;
		this.GetComponent<Rigidbody>().useGravity = true;
		this.GetComponent<Rigidbody>().AddExplosionForce (400, new Vector3(damage.x, 0.1f, damage.z) , 10);
		*/
	}

	public void ReleaseCamera()
	{
		/*
		this.GetComponent<BoxCollider> ().isTrigger = false; 
		this.transform.Find("UI").gameObject.SetActive (false);

		if (IsMainPlayer) 
		{
			GameObject sceneCam = GameObject.Find ("Scene Camera");
			sceneCam.GetComponent<Camera> ().enabled = true;

			Destroy(this.transform.Find ("MainCharacterController").gameObject);
		}
		*/ 
	}
}	
